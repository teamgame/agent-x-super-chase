============
Read Me
============
**Agent-X Super Chase**

************
Installation
************

**How to get the game executable .jar:**

Download the jar from this `link <https://bitbucket.org/teamgame/agent-x-super-chase/downloads>`_.

    
**How to run the game executable .jar:**

If you downloaded the game executable .jar from the link above, make sure you have JRE 1.7 or above installed.

Double click the .jar, or enter ``java -jar [filename]`` if you prefer using the terminal.

    
**How to get the game source-code:**

Clone our BitBucket `repository <https://bitbucket.org/teamgame/agent-x-super-chase>`_ or
download it from `here <https://bitbucket.org/teamgame/agent-x-super-chase/downloads>`_.

*Note you'll need git to be able to clone the repo*

**How to build the game from source-code:**

*Pre Requirements*

* JDK 1.6 or higher.


Open a terminal and navigate to the root of the project directory.

*Linux & Mac*

#. Make sure the file gradlew has permission to execute.
#. Enter the command '``./gradlew dist``'.

*Windows* 

#. Enter the command '``gradlew dist``'.


* Navigate to '``project_root/desktop/build/libs``' to locate the jar file, it should be named ``desktop-[version].jar``


*********
Controlls
*********

*Menu* 

* skip splash screen - Esc
* exit back to menu from sub pages - Esc
* exit game - Esc


*In Game* 

* move the ship - W,A,S,D
* shoot - Ctrl
* enter coin - Enter
* quit to menu - ESC
* god mode - F12
* debug-mode - B


*********
Changelog
*********

v1.0

* Project structure revamp
* UI finished
* Added sounds to the game
* Added java-doc
* Added JUnit tests
* Bug fixes
* Balancing and polish
* Updated build instructions
* Added god-mode for debug purposes

v0.7

* Added general-enemies
* Main-ships spawn from portals
* Added HUD on screen
* HUD shows score
* HUD shows player and main-enemy health
* HUD shows distance to enemy and distance left
* Added combo-mechanic
* Added arcade-ish resume mechanic
* Added game-over screen that shows the score
* Tweaks to building generation
* Various minor bugfixes

v0.2

* Added main enemy
* Added Bullets
* Added fire controller
* Added death
* Added main enemy AI
* Added game state class
* Added highscore to menu
* Added about to menu
* Added Junit tests for menu
* Minor bugfixes
    
v0.1b

* Fixed graphic-bug

v0.1

* Added splash-screen
* Added main-menu
* Added auto-generated level
* Added parallax-background
* Added player-ship
* Added player-controls
* Added collision-detection
* Added debug-mode

v0.0.1 

* Project setup