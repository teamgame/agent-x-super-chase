package se.teamgame.axsc.screens.game;

import org.apache.log4j.Logger;

import se.teamgame.axsc.levels.Level;
import se.teamgame.axsc.levels.LevelRenderer;
import se.teamgame.axsc.managers.AssetManager;
import se.teamgame.axsc.managers.InputManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;

/**
 * GameScreen class that is handling the game screen
 * 
 * @author marcus
 * 
 */
public class GameScreen implements Screen {

	static Logger log = Logger.getLogger(GameScreen.class);

	private Level level;
	private LevelRenderer renderer;

	/**
	 * Constructor
	 */
	public GameScreen() {

		log.debug("Creating game-screen");

		level = new Level();
		renderer = new LevelRenderer(level);

		Gdx.input.setInputProcessor(new InputManager(this));
	}

	@Override
	public void render(float delta) {
		level.update(delta);
		renderer.render();
	}

	@Override
	public void resize(int width, int height) {
		renderer.resize(width, height);
	}

	@Override
	public void show() {
		AssetManager.getInstance().backgroundMusic.setLooping(true);
		AssetManager.getInstance().backgroundMusic.setVolume(1f);
		AssetManager.getInstance().backgroundMusic.play();
		level.show();
		renderer.show();
	}

	@Override
	public void hide() {
		AssetManager.getInstance().backgroundMusic.stop();
		renderer.hide();
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		renderer.dispose();

	}

	/**
	 * 
	 * @return active level
	 */
	public Level getLevel() {
		return level;
	}

	/**
	 * 
	 * @return level renderer
	 */
	public LevelRenderer getRenderer() {
		return renderer;
	}

}
