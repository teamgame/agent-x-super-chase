package se.teamgame.axsc.screens.menu.accessors;

import org.apache.log4j.Logger;

import aurelienribon.tweenengine.TweenAccessor;

import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * Sprite accessor class for the tween manager functions. Sets or gets the
 * sprites alpha value for example fade animation.
 * 
 * @author Mike
 * 
 */
public class SpriteAccessor implements TweenAccessor<Sprite> {

	static Logger log = Logger.getLogger(SpriteAccessor.class);

	public static final int ALPHA = 0;

	@Override
	public int getValues(Sprite target, int tweenType, float[] returnValues) {

		switch (tweenType) {
			case ALPHA:
				returnValues[0] = target.getColor().a;
				return 1;
			default:
				log.error("invalid tweenType");
				return -1;
		}
	}

	@Override
	public void setValues(Sprite target, int tweenType, float[] newValues) {

		switch (tweenType) {
			case ALPHA:
				target.setColor(target.getColor().r, target.getColor().g, target.getColor().b, newValues[0]);
				break;
			default:
				log.error("invalid tweenType");
				break;
		}

	}

}