package se.teamgame.axsc.screens.menu;

import org.apache.log4j.Logger;

import se.teamgame.axsc.managers.AssetManager;
import se.teamgame.axsc.screens.game.GameScreen;
import se.teamgame.axsc.screens.menu.accessors.ActorAccessor;
import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class MenuScreen implements Screen {

	static Logger log = Logger.getLogger(MenuScreen.class);

	AssetManager manager;

	private Stage stage;
	private Label label;
	private Table table;
	private BitmapFont font_red;
	private LabelStyle style;
	private TextButtonStyle buttonStyle;
	private TextButton button_highscore, button_newgame, button_about, button_exit;
	private TweenManager tweenManager;

	@Override
	public void render(float delta) {

		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act(delta);
		stage.draw();

		tweenManager.update(delta);
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void show() {

		log.debug("Entering setup of assets");

		manager = AssetManager.getInstance();

		stage = new Stage();

		Gdx.input.setInputProcessor(new InputMultiplexer(stage, new InputAdapter() {

			@Override
			public boolean keyTyped(char character) {

				if (character == 27) {
					Gdx.app.exit();
				}

				return true;
			}

		}));

		setupFontAndLabel();
		setupButtons();
		setupTable();

		stage.addActor(table);

		animate();
	}

	@Override
	public void hide() {
		dispose();
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {

		log.debug("Disposing assets");

		stage.dispose();
	}

	private void setupFontAndLabel() {

		log.debug("Entering setup of font and label");

		font_red = manager.font_red;
		font_red.setScale(Gdx.graphics.getWidth() / 1920f);
		style = new LabelStyle(font_red, null);
		style.background = manager.menu_skin.getDrawable("logo");
		label = new Label("", style);
		label.setColor(1, 1, 1, 0);
	}

	private void setupButtons() {

		log.debug("Entering setup of buttons");

		buttonStyle = new TextButtonStyle(manager.menu_skin.getDrawable("button.up"),
				manager.menu_skin.getDrawable("button.down"), manager.menu_skin.getDrawable("button.up"), font_red);

		button_newgame = new TextButton("NEW GAME", buttonStyle);
		button_newgame.setColor(1, 1, 1, 0);
		button_newgame.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				((Game) Gdx.app.getApplicationListener()).setScreen(new GameScreen());
			}
		});
		button_newgame.pad(Gdx.graphics.getHeight() / 10);

		button_highscore = new TextButton("HIGHSCORE", buttonStyle);
		button_highscore.setColor(1, 1, 1, 0);
		button_highscore.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				((Game) Gdx.app.getApplicationListener()).setScreen(new HighscoreScreen());
			}
		});
		button_highscore.pad(Gdx.graphics.getHeight() / 10);

		button_about = new TextButton("ABOUT", buttonStyle);
		button_about.setColor(1, 1, 1, 0);
		button_about.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				((Game) Gdx.app.getApplicationListener()).setScreen(new AboutScreen());
			}
		});
		button_about.pad(Gdx.graphics.getHeight() / 10);

		button_exit = new TextButton("EXIT", buttonStyle);
		button_exit.setColor(1, 1, 1, 0);
		button_exit.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.exit();
			}
		});
		button_exit.pad(Gdx.graphics.getHeight() / 10);
	}

	private void setupTable() {

		log.debug("Entering table setup");

		table = new Table(manager.menu_skin);
		table.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		table.add(label);
		table.getCell(label).size(Gdx.graphics.getWidth() * 0.95f, Gdx.graphics.getHeight() / 6)
				.spaceBottom(Gdx.graphics.getHeight() / 20);
		table.row();
		table.add(button_newgame);
		table.getCell(button_newgame).size(Gdx.graphics.getWidth() / 4, Gdx.graphics.getHeight() / 15);
		table.getCell(button_newgame).spaceBottom(Gdx.graphics.getHeight() / 30);
		table.row();
		table.add(button_highscore);
		table.getCell(button_highscore).size(Gdx.graphics.getWidth() / 4, Gdx.graphics.getHeight() / 15);
		table.getCell(button_highscore).spaceBottom(Gdx.graphics.getHeight() / 30);
		table.row();
		table.add(button_about);
		table.getCell(button_about).size(Gdx.graphics.getWidth() / 4, Gdx.graphics.getHeight() / 15);
		table.getCell(button_about).spaceBottom(Gdx.graphics.getHeight() / 30);
		table.row();
		table.add(button_exit);
		table.getCell(button_exit).size(Gdx.graphics.getWidth() / 4, Gdx.graphics.getHeight() / 15);
	}

	private void animate() {

		log.debug("Entering label and button animation");

		tweenManager = new TweenManager();

		Tween.registerAccessor(Actor.class, new ActorAccessor());

		Timeline.createSequence().beginSequence()

		.push(Tween.set(label, ActorAccessor.ALPHA).target(0))
				.push(Tween.set(button_newgame, ActorAccessor.ALPHA).target(0))
				.push(Tween.set(button_highscore, ActorAccessor.ALPHA).target(0))
				.push(Tween.set(button_about, ActorAccessor.ALPHA).target(0))
				.push(Tween.set(button_exit, ActorAccessor.ALPHA).target(0))
				.push(Tween.to(label, ActorAccessor.ALPHA, .05f).target(1))
				.push(Tween.to(button_newgame, ActorAccessor.ALPHA, .05f).target(1))
				.push(Tween.to(button_highscore, ActorAccessor.ALPHA, .05f).target(1))
				.push(Tween.to(button_about, ActorAccessor.ALPHA, .05f).target(1))
				.push(Tween.to(button_exit, ActorAccessor.ALPHA, .05f).target(1)).end().start(tweenManager);
	}
}
