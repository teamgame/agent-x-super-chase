package se.teamgame.axsc.screens.menu;

import org.apache.log4j.Logger;

import se.teamgame.axsc.io.IoHighscore;
import se.teamgame.axsc.managers.AssetManager;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * Menu class that shows the over all highscore of the game.
 * 
 * @author Mike
 * 
 */
public class HighscoreScreen implements Screen {

	static Logger log = Logger.getLogger(HighscoreScreen.class);

	AssetManager manager;

	private Stage stage;
	private Table table;
	private Label label_highscore, label_name1, label_score1, label_name2, label_score2, label_name3, label_score3,
			label_name4, label_score4, label_name5, label_score5, label_name6, label_score6, label_name7, label_score7,
			label_name8, label_score8, label_name9, label_score9, label_name10, label_score10;
	private LabelStyle style_white, style_red;
	private BitmapFont font_highscore_white, font_highscore_red;
	private TextButton button_reset, button_menu;
	private TextButtonStyle buttonStyle;
	private String[] highscoreList;
	private IoHighscore io;
	private final String filename = "score.sav";

	@Override
	public void render(float delta) {

		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void show() {

		log.debug("Entering setup of assets");

		manager = AssetManager.getInstance();

		stage = new Stage();

		Gdx.input.setInputProcessor(new InputMultiplexer(stage, new InputAdapter() {

			@Override
			public boolean keyTyped(char character) {

				if (character == 27) {
					((Game) Gdx.app.getApplicationListener()).setScreen(new MenuScreen());
				}
				return true;
			}
		}));

		io = new IoHighscore();
		highscoreList = io.loadHighscore(filename);
		setupFontsAndLabels();
		setupButtons();
		setupTable();

		stage.addActor(table);
	}

	@Override
	public void hide() {

		dispose();
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {

		log.debug("Disposing assets");

		stage.dispose();
	}

	/**
	 * Sets up fonts and prepares the text to be rendered to the screen.
	 */
	private void setupFontsAndLabels() {

		log.debug("Entering setup of fonts and labels");

		font_highscore_white = manager.arkitech_menu;
		font_highscore_red = manager.font_red;
		font_highscore_white.setFixedWidthGlyphs("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");
		font_highscore_white.setScale(Gdx.graphics.getWidth() / 1920f);
		font_highscore_red.setScale(Gdx.graphics.getWidth() / 1920f);
		style_white = new LabelStyle(font_highscore_white, null);
		style_red = new LabelStyle(font_highscore_red, null);
		label_highscore = new Label("HIGHSCORE", style_red);

		label_name1 = new Label(highscoreList[1], style_white);
		label_score1 = new Label(highscoreList[0], style_white);

		label_name2 = new Label(highscoreList[3], style_white);
		label_score2 = new Label(highscoreList[2], style_white);

		label_name3 = new Label(highscoreList[5], style_white);
		label_score3 = new Label(highscoreList[4], style_white);

		label_name4 = new Label(highscoreList[7], style_white);
		label_score4 = new Label(highscoreList[6], style_white);

		label_name5 = new Label(highscoreList[9], style_white);
		label_score5 = new Label(highscoreList[8], style_white);

		label_name6 = new Label(highscoreList[11], style_white);
		label_score6 = new Label(highscoreList[10], style_white);

		label_name7 = new Label(highscoreList[13], style_white);
		label_score7 = new Label(highscoreList[12], style_white);

		label_name8 = new Label(highscoreList[15], style_white);
		label_score8 = new Label(highscoreList[14], style_white);

		label_name9 = new Label(highscoreList[17], style_white);
		label_score9 = new Label(highscoreList[16], style_white);

		label_name10 = new Label(highscoreList[19], style_white);
		label_score10 = new Label(highscoreList[18], style_white);
	}

	/**
	 * Sets up the buttons of this class. reset button for resetting the
	 * highscore list ( deletes it from disk ).
	 */
	private void setupButtons() {

		log.debug("Entering setup of buttons");

		buttonStyle = new TextButtonStyle(manager.menu_skin.getDrawable("button.up"),
				manager.menu_skin.getDrawable("button.down"), manager.menu_skin.getDrawable("button.up"),
				font_highscore_red);

		button_reset = new TextButton("RESET", buttonStyle);

		button_reset.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.files.local("score.sav").delete();
				((Game) Gdx.app.getApplicationListener()).setScreen(new HighscoreScreen());
			}
		});
		button_reset.pad(Gdx.graphics.getHeight() / 10);

		button_menu = new TextButton("MAIN MENU", buttonStyle);

		button_menu.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				((Game) Gdx.app.getApplicationListener()).setScreen(new MenuScreen());
			}
		});
		button_menu.pad(Gdx.graphics.getHeight() / 10);
	}

	/**
	 * Adds labels and buttons to a table so the stage can handle it in the
	 * render method.
	 */
	private void setupTable() {

		log.debug("Entering table setup");

		table = new Table(manager.menu_skin);
		table.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		table = new Table(manager.menu_skin);
		table.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		table.add(label_highscore);
		table.getCell(label_highscore).spaceBottom(Gdx.graphics.getHeight() / 20);
		table.getCell(label_highscore).padLeft(Gdx.graphics.getWidth() / 6);
		table.row();
		table.add(label_name1).left();
		table.add(label_score1).right();
		table.row();
		table.add(label_name2).left();
		table.add(label_score2).right();
		table.row();
		table.add(label_name3).left();
		table.add(label_score3).right();
		table.row();
		table.add(label_name4).left();
		table.add(label_score4).right();
		table.row();
		table.add(label_name5).left();
		table.add(label_score5).right();
		table.row();
		table.add(label_name6).left();
		table.add(label_score6).right();
		table.row();
		table.add(label_name7).left();
		table.add(label_score7).right();
		table.row();
		table.add(label_name8).left();
		table.add(label_score8).right();
		table.row();
		table.add(label_name9).left();
		table.add(label_score9).right();
		table.row();
		table.add(label_name10).left();
		table.add(label_score10).right();
		table.getCell(label_name10).spaceBottom(Gdx.graphics.getHeight() / 30);
		table.getCell(label_score10).spaceBottom(Gdx.graphics.getHeight() / 30);
		table.row();
		table.add(button_reset);
		table.getCell(button_reset).size(Gdx.graphics.getWidth() / 4, Gdx.graphics.getHeight() / 15);
		table.getCell(button_reset).spaceBottom(Gdx.graphics.getHeight() / 30);
		table.getCell(button_reset).padLeft(Gdx.graphics.getWidth() / 6);
		table.row();
		table.add(button_menu);
		table.getCell(button_menu).size(Gdx.graphics.getWidth() / 4, Gdx.graphics.getHeight() / 15);
		table.getCell(button_menu).padLeft(Gdx.graphics.getWidth() / 6);
	}
}
