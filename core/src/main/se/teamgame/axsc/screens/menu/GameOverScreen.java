package se.teamgame.axsc.screens.menu;

import java.util.ArrayList;
import java.util.Arrays;

import org.apache.log4j.Logger;

import se.teamgame.axsc.gamestate.GameState;
import se.teamgame.axsc.io.IoHighscore;
import se.teamgame.axsc.managers.AssetManager;
import se.teamgame.axsc.screens.game.GameScreen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * The game over screen you come to after playing the game.
 * 
 * @author Mike
 * 
 */
public class GameOverScreen implements Screen {

	static Logger log = Logger.getLogger(GameOverScreen.class);

	AssetManager manager;

	private GameState state;
	private Stage stage;
	private Table main_table, name_table, button_table;
	private Label label_logo, label_highscore, label_1letter, label_2letter, label_3letter, label_player_score,
			label_name1, label_score1, label_name2, label_score2, label_name3, label_score3, label_dots;
	private LabelStyle style_white, style_red;
	private BitmapFont font_white, font_red;
	private TextButton button_1plus, button_2plus, button_3plus, button_1minus, button_2minus, button_3minus,
			button_menu, button_replay;
	private TextButtonStyle buttonStyle;
	private final String[] letters = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P",
			"Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
	private ArrayList<String> list;
	private String[] highscoreList;
	private boolean scoreSet;
	private IoHighscore io;
	private final String filename = "score.sav";

	/**
	 * Constructor
	 * 
	 * @param state
	 *            takes in data such as score and health data.
	 */
	public GameOverScreen(GameState state) {

		log.debug("Creating game over screen");

		if (!(state == null)) {
			this.state = state;
		}
	}

	@Override
	public void render(float delta) {

		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void show() {

		log.debug("Entering setup of assets");

		manager = AssetManager.getInstance();

		stage = new Stage();

		scoreSet = false;

		io = new IoHighscore();
		highscoreList = io.loadHighscore(filename);
		setupFontsAndLabels();
		setupButton();
		setupTables();

		Gdx.input.setInputProcessor(new InputMultiplexer(stage, new InputAdapter() {

			@Override
			public boolean keyTyped(char character) {

				if (character == 27) {
					log.debug("Exiting and saving game score with esc button.");
					io.saveHighscore(state, label_1letter, label_2letter, label_3letter, filename);
					((Game) Gdx.app.getApplicationListener()).setScreen(new MenuScreen());
				}
				return true;
			}
		}));

		stage.addActor(main_table);
	}

	@Override
	public void hide() {
		dispose();
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		log.debug("Disposing assets");

		stage.dispose();
	}

	/**
	 * Sets up fonts and prepares the text to be rendered to the screen.
	 * Different setup if you win or looses the game.
	 */
	private void setupFontsAndLabels() {

		log.debug("Entering setup of fonts and labels");

		list = new ArrayList<String>(Arrays.asList(letters));
		font_white = manager.arkitech_menu;
		font_red = manager.font_red;
		font_white.setFixedWidthGlyphs(".ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");
		font_white.setScale(Gdx.graphics.getWidth() / 1920f);
		font_red.setScale(Gdx.graphics.getWidth() / 1920f);
		style_white = new LabelStyle(font_white, null);
		style_red = new LabelStyle(font_red, null);
		if (state.getScore() == 0) {
			label_logo = new Label("DEFEAT", style_red);

			addHighscoreLabels();

		} else {
			label_logo = new Label("VICTORY", style_red);

			addHighscoreLabels();

			label_dots = new Label("..." + "\n" + "..." + "\n" + "..." + "\n", style_white);
		}

		label_1letter = new Label(list.get(24), style_white);
		label_2letter = new Label(list.get(14), style_white);
		label_3letter = new Label(list.get(20), style_white);

		label_player_score = new Label("" + state.getScore(), style_white);
		label_player_score.setPosition(0, 40);
	}

	/**
	 * Creates the highscore labels.
	 */
	private void addHighscoreLabels() {

		label_highscore = new Label("Highscore", style_white);
		label_name1 = new Label(highscoreList[1], style_white);
		label_score1 = new Label(highscoreList[0], style_white);
		label_name2 = new Label(highscoreList[3], style_white);
		label_score2 = new Label(highscoreList[2], style_white);
		label_name3 = new Label(highscoreList[5], style_white);
		label_score3 = new Label(highscoreList[4], style_white);
	}

	/**
	 * Sets up the buttons of this class.
	 */
	private void setupButton() {

		log.debug("Entering setup of buttons");

		buttonStyle = new TextButtonStyle(manager.menu_skin.getDrawable("button.up"),
				manager.menu_skin.getDrawable("button.down"), manager.menu_skin.getDrawable("button.up"), font_red);

		button_1plus = new TextButton("+", buttonStyle);

		button_1plus.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				String s = label_1letter.getText().toString();
				int index = list.indexOf(s);
				if (index <= 24) {
					label_1letter.setText(list.get(index + 1));
				} else {
					label_1letter.setText(list.get(0));
				}
			}
		});

		button_2plus = new TextButton("+", buttonStyle);

		button_2plus.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				String s = label_2letter.getText().toString();
				int index = list.indexOf(s);
				if (index <= 24) {
					label_2letter.setText(list.get(index + 1));
				} else {
					label_2letter.setText(list.get(0));
				}
			}
		});

		button_3plus = new TextButton("+", buttonStyle);

		button_3plus.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				String s = label_3letter.getText().toString();
				int index = list.indexOf(s);
				if (index <= 24) {
					label_3letter.setText(list.get(index + 1));
				} else {
					label_3letter.setText(list.get(0));
				}
			}
		});

		button_1minus = new TextButton("-", buttonStyle);

		button_1minus.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				String s = label_1letter.getText().toString();
				int index = list.indexOf(s);
				if (index >= 1) {
					label_1letter.setText(list.get(index - 1));
				} else {
					label_1letter.setText(list.get(25));
				}
			}
		});

		button_2minus = new TextButton("-", buttonStyle);

		button_2minus.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				String s = label_2letter.getText().toString();
				int index = list.indexOf(s);
				if (index >= 1) {
					label_2letter.setText(list.get(index - 1));
				} else {
					label_2letter.setText(list.get(25));
				}
			}
		});

		button_3minus = new TextButton("-", buttonStyle);

		button_3minus.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				String s = label_3letter.getText().toString();
				int index = list.indexOf(s);
				if (index >= 1) {
					label_3letter.setText(list.get(index - 1));
				} else {
					label_3letter.setText(list.get(25));
				}
			}
		});

		button_menu = new TextButton("MAIN MENU", buttonStyle);

		button_menu.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				log.debug("Exiting and saving game score with menu button.");
				io.saveHighscore(state, label_1letter, label_2letter, label_3letter, filename);
				((Game) Gdx.app.getApplicationListener()).setScreen(new MenuScreen());
			}
		});
		button_menu.pad(Gdx.graphics.getHeight() / 10);

		button_replay = new TextButton("REPLAY", buttonStyle);

		button_replay.isChecked();

		button_replay.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				log.debug("Replaying and saving game score with replay button.");
				io.saveHighscore(state, label_1letter, label_2letter, label_3letter, filename);
				((Game) Gdx.app.getApplicationListener()).setScreen(new GameScreen());
			}
		});
		button_replay.pad(Gdx.graphics.getHeight() / 10);

	}

	/**
	 * Adds labels and buttons to a table so the stage can handle it in the
	 * render method. Different setup if you win or looses the game.
	 */
	private void setupTables() {

		log.debug("Entering tables setup");

		main_table = new Table(manager.menu_skin);
		name_table = new Table(manager.menu_skin);
		button_table = new Table(manager.menu_skin);

		main_table.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		main_table.add(label_logo);
		main_table.getCell(label_logo).spaceBottom(Gdx.graphics.getHeight() / 20);
		main_table.getCell(label_logo).padLeft(Gdx.graphics.getWidth() / 6);
		main_table.row();
		main_table.add(label_highscore);
		main_table.getCell(label_highscore).spaceBottom(Gdx.graphics.getHeight() / 30);
		main_table.getCell(label_highscore).padLeft(Gdx.graphics.getWidth() / 6);
		main_table.row();

		if (state.getScore() != 0) {
			log.debug("Adding victory graphics to tables");
			name_table.add(button_1plus).size(Gdx.graphics.getWidth() / 40, Gdx.graphics.getHeight() / 30);
			name_table.add(button_2plus).size(Gdx.graphics.getWidth() / 40, Gdx.graphics.getHeight() / 30);
			name_table.add(button_3plus).size(Gdx.graphics.getWidth() / 40, Gdx.graphics.getHeight() / 30);
			name_table.row();
			name_table.add(label_1letter).size(Gdx.graphics.getWidth() / 40, Gdx.graphics.getHeight() / 20);
			name_table.add(label_2letter).size(Gdx.graphics.getWidth() / 40, Gdx.graphics.getHeight() / 20);
			name_table.add(label_3letter).size(Gdx.graphics.getWidth() / 40, Gdx.graphics.getHeight() / 20);
			name_table.row();
			name_table.add(button_1minus).size(Gdx.graphics.getWidth() / 40, Gdx.graphics.getHeight() / 30);
			name_table.add(button_2minus).size(Gdx.graphics.getWidth() / 40, Gdx.graphics.getHeight() / 30);
			name_table.add(button_3minus).size(Gdx.graphics.getWidth() / 40, Gdx.graphics.getHeight() / 30);

			if (state.getScore() > Integer.parseInt(highscoreList[0]) && scoreSet == false) {
				addNameInputToTable();
				scoreSet = true;
			}

			main_table.add(label_name1).left();
			main_table.add(label_score1).right();
			main_table.row();

			if (state.getScore() > Integer.parseInt(highscoreList[2]) && scoreSet == false) {
				addNameInputToTable();
				scoreSet = true;
			}

			main_table.add(label_name2).left();
			main_table.add(label_score2).right();
			main_table.row();

			if (state.getScore() > Integer.parseInt(highscoreList[4]) && scoreSet == false) {
				addNameInputToTable();
				scoreSet = true;
			}

			if (scoreSet == false) {
				main_table.add(label_name3).left();
				main_table.add(label_score3).right();
				main_table.row();
			}

			if (scoreSet == false) {
				main_table.add(label_dots).left();
				main_table.getCell(label_dots).spaceBottom(Gdx.graphics.getHeight() / 30);
				main_table.row();
			}

			if (state.getScore() < Integer.parseInt(highscoreList[4])) {
				addNameInputToTable();
			}

		} else {
			main_table.add(label_name1).left();
			main_table.add(label_score1).right();
			main_table.row();
			main_table.add(label_name2).left();
			main_table.add(label_score2).right();
			main_table.row();
			main_table.add(label_name3).left();
			main_table.add(label_score3).right();
			main_table.row();
		}

		button_table.add(button_replay);
		button_table.getCell(button_replay).size(Gdx.graphics.getWidth() / 4, Gdx.graphics.getHeight() / 15);
		button_table.row();
		button_table.add(button_menu);
		button_table.getCell(button_menu).size(Gdx.graphics.getWidth() / 4, Gdx.graphics.getHeight() / 15);
		button_table.getCell(button_replay).spaceBottom(Gdx.graphics.getHeight() / 30);
		main_table.add(button_table).spaceTop(Gdx.graphics.getHeight() / 30);
		main_table.getCell(button_table).padLeft(Gdx.graphics.getWidth() / 6);
	}

	/**
	 * Gets this method when score are sorted.
	 */
	private void addNameInputToTable() {

		main_table.add(name_table).left();
		main_table.add(label_player_score).right();
		main_table.row();
	}
}
