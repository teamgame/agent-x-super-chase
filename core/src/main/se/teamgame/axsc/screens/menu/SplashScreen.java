package se.teamgame.axsc.screens.menu;

import org.apache.log4j.Logger;

import se.teamgame.axsc.managers.AssetManager;
import se.teamgame.axsc.screens.menu.accessors.SpriteAccessor;
import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Splash screen before the menu class.
 * 
 * @author Mike
 * 
 */
public class SplashScreen implements Screen {

	static Logger log = Logger.getLogger(SplashScreen.class);

	AssetManager manager;

	private SpriteBatch batch;
	private Sprite t, e, a, m, g, a2, m2, e2, p, r, e3, s, e4, n, t2, s2;
	private TweenManager tweenManager;
	private float counter = 1;

	private final int centerX = Gdx.graphics.getWidth() / 2;
	private final int centerY = Gdx.graphics.getHeight() / 2;

	@Override
	public void render(float delta) {

		batch.begin();
		t.draw(batch);
		e.draw(batch);
		a.draw(batch);
		m.draw(batch);
		g.draw(batch);
		a2.draw(batch);
		m2.draw(batch);
		e2.draw(batch);
		p.draw(batch);
		r.draw(batch);
		e3.draw(batch);
		s.draw(batch);
		e4.draw(batch);
		n.draw(batch);
		t2.draw(batch);
		s2.draw(batch);
		batch.end();

		if (Gdx.input.isKeyPressed(Keys.ANY_KEY) || Gdx.input.isTouched()) {
			log.debug("Exit splash with keyboard");
			((Game) Gdx.app.getApplicationListener()).setScreen(new MenuScreen());
		}

		tweenManager.update(delta);
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void show() {

		log.debug("Entering setup of assets");

		manager = AssetManager.getInstance();

		batch = new SpriteBatch();
		tweenManager = new TweenManager();
		Tween.registerAccessor(Sprite.class, new SpriteAccessor());

		loadTexturesToSprites();

		t.setPosition(centerX - 107, centerY);
		e.setPosition(centerX - 82, centerY);
		a.setPosition(centerX - 63, centerY);
		m.setPosition(centerX - 35, centerY);
		g.setPosition(centerX + 17, centerY);
		a2.setPosition(centerX + 45, centerY);
		m2.setPosition(centerX + 72, centerY);
		e2.setPosition(centerX + 106, centerY);

		p.setPosition(centerX - 47, centerY - 30);
		r.setPosition(centerX - 31, centerY - 25);
		e3.setPosition(centerX - 21, centerY - 25);
		s.setPosition(centerX - 6, centerY - 25);
		e4.setPosition(centerX + 6, centerY - 25);
		n.setPosition(centerX + 21, centerY - 25);
		t2.setPosition(centerX + 35, centerY - 25);
		s2.setPosition(centerX + 46, centerY - 25);

		tweenManager.update(Float.MIN_VALUE);
	}

	@Override
	public void hide() {
		dispose();
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {

		log.debug("Disposing assets");

		batch.dispose();
	}

	/**
	 * Loads the textures from the assert manager.
	 */
	private void loadTexturesToSprites() {

		log.debug("Entering sprite texture loading");

		t = animateSprite(manager.t_letter_big);
		e = animateSprite(manager.e_letter_big);
		a = animateSprite(manager.a_letter_big);
		m = animateSprite(manager.m_letter_big);

		g = animateSprite(manager.g_letter_big);
		a2 = animateSprite(manager.a_letter_big);
		m2 = animateSprite(manager.m_letter_big);
		e2 = animateSprite(manager.e_letter_big);

		p = animateSprite(manager.p_letter_small);
		r = animateSprite(manager.r_letter_small);
		e3 = animateSprite(manager.e_letter_small);
		s = animateSprite(manager.s_letter_small);
		e4 = animateSprite(manager.e_letter_small);
		n = animateSprite(manager.n_letter_small);
		t2 = animateSprite(manager.t_letter_small);
		s2 = animateSprite(manager.s_letter_small);
	}

	/**
	 * Animates the sprites in the splash screen with the tween engine.
	 * 
	 * @param texture
	 *            to be animated.
	 * @return animated sprite.
	 */
	private Sprite animateSprite(Texture texture) {

		log.debug("Entering sprite animation");

		Sprite sprite = new Sprite(texture);

		Tween.set(sprite, SpriteAccessor.ALPHA).target(0).start(tweenManager);
		Tween.to(sprite, SpriteAccessor.ALPHA, 2).target(1).start(tweenManager).delay(counter);
		Tween.to(sprite, SpriteAccessor.ALPHA, 1).target(0).start(tweenManager).delay(5)
				.setCallback(new TweenCallback() {

					@Override
					public void onEvent(int type, BaseTween<?> source) {
						((Game) Gdx.app.getApplicationListener()).setScreen(new MenuScreen());
					}
				}).start(tweenManager);

		counter += 0.15;
		return sprite;
	}
}
