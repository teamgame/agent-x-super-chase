package se.teamgame.axsc.screens.menu;

import org.apache.log4j.Logger;

import se.teamgame.axsc.AgentXSuperChase;
import se.teamgame.axsc.managers.AssetManager;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * Menu class, shows information about the project.
 * 
 * @author Mike
 * 
 */
public class AboutScreen implements Screen {

	static Logger log = Logger.getLogger(AboutScreen.class);

	AssetManager manager;

	private Stage stage;
	private Table table;
	private Label label_logo, label_by, label_version;
	private LabelStyle style_white, style_red;
	private BitmapFont font_about_white, font_about_red;
	private TextButton button_menu;
	private TextButtonStyle buttonStyle;

	@Override
	public void render(float delta) {

		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void show() {

		log.debug("Entering setup of assets");

		manager = AssetManager.getInstance();

		stage = new Stage();

		Gdx.input.setInputProcessor(new InputMultiplexer(stage, new InputAdapter() {

			@Override
			public boolean keyTyped(char character) {

				if (character == 27) {
					((Game) Gdx.app.getApplicationListener()).setScreen(new MenuScreen());
				}
				return true;
			}
		}));

		setupFontsAndLabels();
		setupButton();
		setupTable();

		stage.addActor(table);
	}

	@Override
	public void hide() {
		dispose();
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {

		log.debug("Disposing assets");
		stage.dispose();
	}

	/**
	 * Sets up fonts and prepares the text to be rendered to the screen.
	 */
	private void setupFontsAndLabels() {

		log.debug("Entering setup of fonts and labels");

		font_about_white = manager.font_white;
		font_about_red = manager.font_red;
		font_about_white.setScale(Gdx.graphics.getWidth() / 1920f);
		font_about_red.setScale(Gdx.graphics.getWidth() / 1920f);
		style_white = new LabelStyle(font_about_white, null);
		style_red = new LabelStyle(font_about_red, null);
		label_logo = new Label("ABOUT", style_red);
		label_by = new Label("By: Team Game - 2014", style_white);
		label_version = new Label("Version: " + AgentXSuperChase.VERSION, style_white);
	}

	/**
	 * Sets up the buttons of this class.
	 */
	private void setupButton() {

		log.debug("Entering setup of button");

		buttonStyle = new TextButtonStyle(manager.menu_skin.getDrawable("button.up"),
				manager.menu_skin.getDrawable("button.down"), manager.menu_skin.getDrawable("button.up"),
				font_about_red);

		button_menu = new TextButton("MAIN MENU", buttonStyle);

		button_menu.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				((Game) Gdx.app.getApplicationListener()).setScreen(new MenuScreen());
			}
		});
		button_menu.pad(Gdx.graphics.getHeight() / 10);
	}

	/**
	 * Adds labels and buttons to a table so the stage can handle it in the
	 * render method.
	 */
	private void setupTable() {

		log.debug("Entering table setup");

		table = new Table(manager.menu_skin);
		table.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		table = new Table(manager.menu_skin);
		table.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		table.add(label_logo);
		table.getCell(label_logo).spaceBottom(Gdx.graphics.getHeight() / 20);
		table.row();
		table.add(label_by);
		table.row();
		table.add(label_version);
		table.getCell(label_version).spaceBottom(Gdx.graphics.getHeight() / 30);
		table.row();
		table.add(button_menu);
		table.getCell(button_menu).size(Gdx.graphics.getWidth() / 4, Gdx.graphics.getHeight() / 15);
		table.getCell(button_menu).spaceBottom(Gdx.graphics.getHeight() / 30);
	}
}
