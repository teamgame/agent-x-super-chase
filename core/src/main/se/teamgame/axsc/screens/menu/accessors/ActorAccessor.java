package se.teamgame.axsc.screens.menu.accessors;

import org.apache.log4j.Logger;

import aurelienribon.tweenengine.TweenAccessor;

import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Actor accessor class for the tween manager functions. Sets or gets the Actors
 * y-pos, alpha or rgb values for animating the actor.
 * 
 * @author Mike
 * 
 */
public class ActorAccessor implements TweenAccessor<Actor> {

	static Logger log = Logger.getLogger(ActorAccessor.class);

	public static final int Y = 0, RGB = 1, ALPHA = 2;

	@Override
	public int getValues(Actor target, int tweenType, float[] returnValues) {

		switch (tweenType) {
			case Y:
				returnValues[0] = target.getY();
				return 1;
			case RGB:
				returnValues[0] = target.getColor().r;
				returnValues[1] = target.getColor().g;
				returnValues[2] = target.getColor().b;
				return 3;
			case ALPHA:
				returnValues[0] = target.getColor().a;
				return 1;
			default:
				log.error("invalid tweenType");
				return -1;
		}

	}

	@Override
	public void setValues(Actor target, int tweenType, float[] newValues) {

		switch (tweenType) {
			case Y:
				target.setY(newValues[0]);
				break;
			case RGB:
				target.setColor(newValues[0], newValues[1], newValues[2], target.getColor().a);
				break;
			case ALPHA:
				target.setColor(target.getColor().r, target.getColor().g, target.getColor().b, newValues[0]);
				break;
			default:
				log.error("invalid tweenType");
				break;
		}

	}

}
