package se.teamgame.axsc.hud;

import org.apache.log4j.Logger;

import se.teamgame.axsc.gamestate.GameState;
import se.teamgame.axsc.hud.elements.ChaseMeterHud;
import se.teamgame.axsc.hud.elements.EnemyHud;
import se.teamgame.axsc.hud.elements.PlayerHud;
import se.teamgame.axsc.hud.elements.ScoreHud;
import se.teamgame.axsc.levels.Level.LevelState;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Main HUD class for in game graphics.
 * 
 * @author Mike
 * 
 */
public class Hud {

	static Logger log = Logger.getLogger(Hud.class);

	private GameState state;
	private PlayerHud player;
	private ScoreHud score;
	private EnemyHud enemy;
	private ChaseMeterHud chaseMeter;

	/**
	 * Constructor
	 * 
	 * @param state
	 *            holds all the data from the game for updating the meters on
	 *            the HUD.
	 */
	public Hud(GameState state) {

		log.debug("Creating HUD");

		this.state = state;
	}

	public void show() {

		log.debug("Entering setup of assets");

		player = new PlayerHud();
		score = new ScoreHud();
		enemy = new EnemyHud();
		chaseMeter = new ChaseMeterHud();
	}

	public void update(float delta) {

		player.updateHealth(state);
		score.updateScore(state);
		enemy.updateHealth(state);
		chaseMeter.updateChaseMeter(state);
	}

	public void render(SpriteBatch batch) {

		batch.begin();
		player.getBackgroundSprite().draw(batch);
		player.getForgroundSprite().draw(batch);
		player.getLabel().draw(batch, .7f);

		score.getScore().draw(batch, .7f);

		if (state.getLevelState() == LevelState.BATTLE) {
			enemy.getBackgroundSprite().draw(batch);
			enemy.getForgroundSprite().draw(batch);
			enemy.getLabel().draw(batch, .7f);
		}

		chaseMeter.getBackgroundSprite().draw(batch);
		chaseMeter.getPlayerSprite().draw(batch);
		chaseMeter.getEnemySprite().draw(batch);
		batch.end();

	}

}
