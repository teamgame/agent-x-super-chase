package se.teamgame.axsc.hud.elements;

import org.apache.log4j.Logger;

import se.teamgame.axsc.gamestate.GameState;
import se.teamgame.axsc.managers.AssetManager;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

/**
 * The player HUD class. Holds data and graphics for the player.
 * 
 * @author Mike
 * 
 */
public class PlayerHud {

	static Logger log = Logger.getLogger(PlayerHud.class);

	AssetManager manager;

	private Sprite back, front;
	private BitmapFont font_white;
	private Label label;
	private LabelStyle labelStyle;

	/**
	 * Constructor
	 */
	public PlayerHud() {

		log.debug("Creating Player Hud");

		manager = AssetManager.getInstance();

		back = new Sprite(manager.bar_bkg);
		front = new Sprite(manager.player_bar);

		back.setAlpha(.7f);
		front.setAlpha(.7f);

		back.setBounds(20, 1040, 350, 12);
		front.setBounds(back.getX() + 3, back.getY() + 3, 0, 5);

		font_white = manager.arkitech20;

		labelStyle = new LabelStyle(font_white, null);

		label = new Label("Player", labelStyle);
		label.setPosition(back.getX(), back.getY() + 15);
	}

	/**
	 * 
	 * @return the background sprite for the player health bar
	 */
	public Sprite getBackgroundSprite() {
		return back;
	}

	/**
	 * 
	 * @return the foreground sprite for the player health bar
	 */
	public Sprite getForgroundSprite() {
		return front;
	}

	/**
	 * 
	 * @return the player text above the player health bar.
	 */
	public Label getLabel() {
		return label;
	}

	/**
	 * 
	 * @param state
	 *            updates the player health bar.
	 */
	public void updateHealth(GameState state) {

		float updateHealth = state.getPlayerHealth() * 3.44f;

		if (!(state.getPlayerHealth() < 0) && !(state.getPlayerHealth() > 100)) {
			front.setBounds(back.getX() + 3, back.getY() + 3, updateHealth, 5);
		}

	}

}
