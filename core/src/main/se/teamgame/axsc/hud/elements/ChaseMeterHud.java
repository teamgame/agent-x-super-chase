package se.teamgame.axsc.hud.elements;

import org.apache.log4j.Logger;

import se.teamgame.axsc.gamestate.GameState;
import se.teamgame.axsc.managers.AssetManager;

import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * This class holds the graphics and data for updating the meter that shows
 * distance between player, enemy and the end of the course.
 * 
 * @author Mike
 * 
 */
public class ChaseMeterHud {

	static Logger log = Logger.getLogger(ChaseMeterHud.class);

	AssetManager manager;

	private Sprite back, player, enemy;

	/**
	 * Constructor
	 */
	public ChaseMeterHud() {

		log.debug("Creating ChaseMeter");

		manager = AssetManager.getInstance();

		back = new Sprite(manager.bar_bkg);
		player = new Sprite(manager.player_bar);
		enemy = new Sprite(manager.enemy_bar);

		back.setAlpha(.9f);
		player.setAlpha(.9f);
		enemy.setAlpha(.9f);

		back.setBounds(20, 20, 1880, 12);
		player.setBounds(20, 20, 12, 12);
		enemy.setBounds(20, 20, 12, 12);
	}

	/**
	 * 
	 * @return the meter background sprite.
	 */
	public Sprite getBackgroundSprite() {
		return back;
	}

	/**
	 * 
	 * @return the player sprite on the meter.
	 */
	public Sprite getPlayerSprite() {
		return player;
	}

	/**
	 * 
	 * @return the enemy sprite on the meter.
	 */
	public Sprite getEnemySprite() {
		return enemy;
	}

	/**
	 * 
	 * @param state
	 *            updates the distance between player, enemy and end of the
	 *            course.
	 */
	public void updateChaseMeter(GameState state) {

		float updateDistance = 1880f / state.getEndpoint();

		if (!((20 + state.getDistanceTravelled() * updateDistance) > 1880f)) {
			player.setBounds(20 + (state.getDistanceTravelled() * updateDistance), 20, 12, 12);
		}
		if (!(((20 + state.getDistanceTravelled() * updateDistance) + (state.getDistanceToEnemy() * updateDistance)) > 1880f)) {
			enemy.setBounds(20 + (state.getDistanceTravelled() * updateDistance)
					+ (state.getDistanceToEnemy() * updateDistance), 20, 12, 12);
		}
	}
}
