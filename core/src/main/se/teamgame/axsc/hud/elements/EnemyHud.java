package se.teamgame.axsc.hud.elements;

import org.apache.log4j.Logger;

import se.teamgame.axsc.gamestate.GameState;
import se.teamgame.axsc.managers.AssetManager;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

/**
 * The enemy HUD class. Holds data and graphics for the main enemy.
 * 
 * @author Mike
 * 
 */
public class EnemyHud {

	static Logger log = Logger.getLogger(EnemyHud.class);

	AssetManager manager;

	private Sprite back, front;
	private BitmapFont font_white;
	private Label label;
	private LabelStyle labelStyle;

	/**
	 * Constructor
	 */
	public EnemyHud() {

		log.debug("Creating Enemy Hud");

		manager = AssetManager.getInstance();

		back = new Sprite(manager.bar_bkg);
		front = new Sprite(manager.enemy_bar);

		back.setAlpha(.7f);
		front.setAlpha(.7f);

		back.setBounds(1550, 1040, 350, 12);
		front.setBounds(1550 + 3, 1040 + 3, 0, 5);

		font_white = manager.arkitech20;

		labelStyle = new LabelStyle(font_white, null);

		label = new Label("Enemy", labelStyle);
		label.setPosition(back.getX(), back.getY() + 15);

	}

	/**
	 * 
	 * @return the background sprite for the main enemy health bar
	 */
	public Sprite getBackgroundSprite() {
		return back;
	}

	/**
	 * 
	 * @return the foreground sprite for the main enemy health bar
	 */
	public Sprite getForgroundSprite() {
		return front;
	}

	/**
	 * 
	 * @return the enemy text above the main enemy health bar.
	 */
	public Label getLabel() {
		return label;
	}

	/**
	 * 
	 * @param state
	 *            updates the main enemy health bar.
	 */
	public void updateHealth(GameState state) {

		float updateHealth = state.getEnemyHealth() * 3.44f;

		if (!(state.getEnemyHealth() < 0) && !(state.getEnemyHealth() > 100)) {
			front.setBounds(back.getX() + 3, back.getY() + 3, updateHealth, 5);
		}
	}

}
