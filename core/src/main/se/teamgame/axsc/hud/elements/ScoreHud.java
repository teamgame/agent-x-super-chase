package se.teamgame.axsc.hud.elements;

import org.apache.log4j.Logger;

import se.teamgame.axsc.gamestate.GameState;
import se.teamgame.axsc.managers.AssetManager;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

/**
 * The score class for the HUD.
 * 
 * @author Mike
 * 
 */
public class ScoreHud {

	static Logger log = Logger.getLogger(PlayerHud.class);

	AssetManager manager;

	private BitmapFont font_white;
	private Label label;
	private LabelStyle labelStyle;

	/**
	 * Constructor
	 */
	public ScoreHud() {

		log.debug("Creating Score Hud");

		manager = AssetManager.getInstance();

		font_white = manager.arkitech20;
		font_white.setFixedWidthGlyphs("0123456789");

		labelStyle = new LabelStyle(font_white, null);

		label = new Label("Score: ", labelStyle);
		label.setPosition(1920 / 2 - label.getWidth(), 1040);

	}

	/**
	 * 
	 * @return the score text.
	 */
	public Label getScore() {
		return label;
	}

	/**
	 * 
	 * @param state
	 *            updates the score value.
	 */
	public void updateScore(GameState state) {
		label.setText("Score: " + state.getScore());
	}
}
