package se.teamgame.axsc;

import org.apache.log4j.Logger;

import se.teamgame.axsc.managers.AssetManager;
import se.teamgame.axsc.screens.menu.SplashScreen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;

/**
 * Main game class
 * 
 * @author marcus
 * 
 */
public class AgentXSuperChase extends Game {

	static Logger log = Logger.getLogger(AgentXSuperChase.class);

	public static final String TITLE = "Agent-X Super Chase";
	public static final String VERSION = "1.0";

	public static final int WIDTH = 1920;
	public static final int HEIGHT = 1080;

	AssetManager manager;

	@Override
	public void create() {

		log.debug("Creating game");

		Gdx.graphics.setDisplayMode(Gdx.graphics.getDesktopDisplayMode().width,
				Gdx.graphics.getDesktopDisplayMode().height, true);

		manager = AssetManager.getInstance();
		manager.loadGraphics();
		manager.loadSounds();
		manager.loadFonts();

		setScreen(new SplashScreen());
	}

	@Override
	public void dispose() {
		super.dispose();
		manager.dispose();
	}
}
