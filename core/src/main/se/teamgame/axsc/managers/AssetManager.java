package se.teamgame.axsc.managers;

import org.apache.log4j.Logger;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

/**
 * AssetManager class, singleton used for managing game assets
 * 
 * @author marcus
 * 
 */
public class AssetManager {

	static Logger log = Logger.getLogger(AssetManager.class);

	private static AssetManager INSTANCE;

	private TextureAtlas atlas;

	public TextureRegion topCenter;

	public TextureRegion topLeft1;
	public TextureRegion topRight1;
	public TextureRegion leftLit1;
	public TextureRegion rightLit1;
	public TextureRegion leftDark1;
	public TextureRegion rightDark1;
	public TextureRegion centerLit1;
	public TextureRegion centerDark1;

	public TextureRegion topLeft2;
	public TextureRegion topRight2;
	public TextureRegion leftLit2;
	public TextureRegion rightLit2;
	public TextureRegion leftDark2;
	public TextureRegion rightDark2;
	public TextureRegion centerLit2;
	public TextureRegion centerDark2;

	public TextureRegion topLeft3;
	public TextureRegion topRight3;
	public TextureRegion leftLit3;
	public TextureRegion rightLit3;
	public TextureRegion leftDark3;
	public TextureRegion rightDark3;
	public TextureRegion centerLit3;
	public TextureRegion centerDark3;

	public TextureRegion roofOrnament0;
	public TextureRegion roofOrnament1;
	public TextureRegion roofOrnament2;
	public TextureRegion roofOrnament3;
	public TextureRegion roofOrnament4;
	public TextureRegion roofOrnament5;
	public TextureRegion roofOrnament6;
	public TextureRegion roofOrnament7;

	public TextureRegion playerShip;
	public TextureRegion mainEnemy;

	public TextureRegion thrustLeft;
	public TextureRegion sentryDrone;
	public TextureRegion sentryDroneThrust;
	public TextureRegion rotatorBody;
	public TextureRegion rotatorTurret;
	public TextureRegion rotatorThrust;
	public TextureRegion meteorit;
	public TextureRegion bigOne;
	public TextureRegion bigOneLaser;

	public TextureRegion playerBullet;
	public TextureRegion enemyBullet;
	public TextureRegion mine;

	public Texture skyBackground;
	public Texture backgroundSkyline;
	public Texture mntLayerBack;
	public Texture mntLayerFront;
	public Texture water;

	public Texture a_letter_big;
	public Texture e_letter_big;
	public Texture g_letter_big;
	public Texture m_letter_big;
	public Texture t_letter_big;
	public Texture e_letter_small;
	public Texture n_letter_small;
	public Texture p_letter_small;
	public Texture r_letter_small;
	public Texture s_letter_small;
	public Texture t_letter_small;

	public Texture menu_logo;

	public Texture meteorWarning;
	public Texture insertCoin;

	public Texture bar_bkg;
	public Texture player_bar;
	public Texture enemy_bar;

	public Animation explosion;
	public Animation portal;

	public BitmapFont font_white;
	public BitmapFont font_red;
	public BitmapFont arkitech20;
	public BitmapFont arkitech30;
	public BitmapFont arkitech60;
	public BitmapFont arkitech100;
	public BitmapFont arkitech_menu;

	public Skin menu_skin;

	public Sound alertSound;
	public Sound laserSound;
	public Sound explosionSound1;
	public Sound explosionSound2;
	public Sound explosionSound3;
	public Sound portalSound;
	public Sound ricochetSound;
	public Sound rotatorShoot1;
	public Sound rotatorShoot2;
	public Sound rotatorShoot3;
	public Sound sentryShoot1;
	public Sound sentryShoot2;
	public Sound sentryShoot3;
	public Sound shoot1;
	public Sound shoot2;
	public Sound shoot3;

	public final String names = "350000,THR,300000,REX,250000,BOB,200000,TIM,150000,TOM,100000,LEA,75000,ZEB,50000,ZAK,25000,BIP,10000,BOT,";

	public Music backgroundMusic;

	private AssetManager() {

	}

	/**
	 * 
	 * @return instance of the class
	 */
	public static AssetManager getInstance() {
		if (INSTANCE == null) {
			log.debug("Creating Asset-manager instance");
			INSTANCE = new AssetManager();
		}

		return INSTANCE;
	}

	/**
	 * Loads graphic assets
	 */
	public void loadGraphics() {

		log.debug("Loading graphics");

		loadSplashGraphics();
		loadMenuGraphics();
		loadBuildingBlocks();
		loadBackgroundGraphics();
		loadShipsGraphics();
		loadWeaponsGraphics();
		loadTrashEnemyGraphics();
		loadAnimations();
		loadHUDGraphics();
	}

	/**
	 * Loads sound assets
	 */
	public void loadSounds() {

		log.debug("Loading sounds");

		alertSound = Gdx.audio.newSound(Gdx.files.internal("snd/alert.ogg"));
		laserSound = Gdx.audio.newSound(Gdx.files.internal("snd/laser.ogg"));
		explosionSound1 = Gdx.audio.newSound(Gdx.files.internal("snd/explosion1.ogg"));
		explosionSound2 = Gdx.audio.newSound(Gdx.files.internal("snd/explosion2.ogg"));
		explosionSound3 = Gdx.audio.newSound(Gdx.files.internal("snd/explosion3.ogg"));
		ricochetSound = Gdx.audio.newSound(Gdx.files.internal("snd/ricochet.ogg"));
		rotatorShoot1 = Gdx.audio.newSound(Gdx.files.internal("snd/rotator_shoot1.ogg"));
		rotatorShoot2 = Gdx.audio.newSound(Gdx.files.internal("snd/rotator_shoot2.ogg"));
		rotatorShoot3 = Gdx.audio.newSound(Gdx.files.internal("snd/rotator_shoot3.ogg"));
		sentryShoot1 = Gdx.audio.newSound(Gdx.files.internal("snd/sentry_shoot1.ogg"));
		sentryShoot2 = Gdx.audio.newSound(Gdx.files.internal("snd/sentry_shoot2.ogg"));
		sentryShoot3 = Gdx.audio.newSound(Gdx.files.internal("snd/sentry_shoot3.ogg"));
		shoot1 = Gdx.audio.newSound(Gdx.files.internal("snd/shoot1.ogg"));
		shoot2 = Gdx.audio.newSound(Gdx.files.internal("snd/shoot2.ogg"));
		shoot3 = Gdx.audio.newSound(Gdx.files.internal("snd/shoot3.ogg"));
		portalSound = Gdx.audio.newSound(Gdx.files.internal("snd/portal.ogg"));

		backgroundMusic = Gdx.audio.newMusic(Gdx.files.internal("snd/background.ogg"));

	}

	/**
	 * Loads font assets
	 */
	public void loadFonts() {
		log.debug("Loading fonts");

		font_white = new BitmapFont(Gdx.files.internal("fonts/white.fnt"));
		font_red = new BitmapFont(Gdx.files.internal("fonts/enemyred.fnt"));
		arkitech20 = new BitmapFont(Gdx.files.internal("fonts/arkitech20.fnt"));
		arkitech30 = new BitmapFont(Gdx.files.internal("fonts/arkitech30.fnt"));
		arkitech60 = new BitmapFont(Gdx.files.internal("fonts/arkitech60.fnt"));
		arkitech100 = new BitmapFont(Gdx.files.internal("fonts/arkitech100.fnt"));
		arkitech_menu = new BitmapFont(Gdx.files.internal("fonts/white.fnt"));
	}

	/**
	 * Dispose assets
	 */
	public void dispose() {
		log.debug("Disposing assets");

		atlas.dispose();

		a_letter_big.dispose();
		e_letter_big.dispose();
		g_letter_big.dispose();
		m_letter_big.dispose();
		t_letter_big.dispose();
		e_letter_small.dispose();
		n_letter_small.dispose();
		p_letter_small.dispose();
		r_letter_small.dispose();
		s_letter_small.dispose();
		t_letter_small.dispose();

		backgroundSkyline.dispose();
		mntLayerBack.dispose();
		mntLayerFront.dispose();
		water.dispose();

		font_red.dispose();
		font_white.dispose();
		arkitech20.dispose();
		arkitech30.dispose();
		arkitech60.dispose();
		arkitech100.dispose();
		arkitech_menu.dispose();

		bar_bkg.dispose();
		player_bar.dispose();
		enemy_bar.dispose();
		meteorWarning.dispose();
		insertCoin.dispose();

		menu_logo.dispose();
		menu_skin.dispose();

		alertSound.dispose();
		laserSound.dispose();
		explosionSound1.dispose();
		explosionSound2.dispose();
		explosionSound3.dispose();
		portalSound.dispose();
		ricochetSound.dispose();
		rotatorShoot1.dispose();
		rotatorShoot2.dispose();
		rotatorShoot3.dispose();
		sentryShoot1.dispose();
		sentryShoot2.dispose();
		sentryShoot3.dispose();
		shoot1.dispose();
		shoot2.dispose();
		shoot3.dispose();

		backgroundMusic.dispose();
	}

	/**
	 * Loads building block grapics
	 */
	private void loadBuildingBlocks() {
		log.debug("Loading building-blocks");

		atlas = new TextureAtlas(Gdx.files.internal("gfx/buildings/buildingblocks.pack"));

		topCenter = atlas.findRegion("topCenter");

		topLeft1 = atlas.findRegion("topLeft-1");
		topRight1 = new TextureRegion(topLeft1);
		topRight1.flip(true, false);
		leftLit1 = atlas.findRegion("leftLit-1");
		rightLit1 = new TextureRegion(leftLit1);
		rightLit1.flip(true, false);
		leftDark1 = atlas.findRegion("leftDark-1");
		rightDark1 = new TextureRegion(leftDark1);
		rightDark1.flip(true, false);
		centerLit1 = atlas.findRegion("centerLit-1");
		centerDark1 = atlas.findRegion("centerDark-1");

		topLeft2 = atlas.findRegion("topLeft-2");
		topRight2 = new TextureRegion(topLeft2);
		topRight2.flip(true, false);
		leftLit2 = atlas.findRegion("leftLit-2");
		rightLit2 = new TextureRegion(leftLit2);
		rightLit2.flip(true, false);
		leftDark2 = atlas.findRegion("leftDark-2");
		rightDark2 = new TextureRegion(leftDark2);
		rightDark2.flip(true, false);
		centerLit2 = atlas.findRegion("centerLit-2");
		centerDark2 = atlas.findRegion("centerDark-2");

		topLeft3 = atlas.findRegion("topLeft-3");
		topRight3 = new TextureRegion(topLeft3);
		topRight3.flip(true, false);
		leftLit3 = atlas.findRegion("leftLit-3");
		rightLit3 = new TextureRegion(leftLit3);
		rightLit3.flip(true, false);
		leftDark3 = atlas.findRegion("leftDark-3");
		rightDark3 = new TextureRegion(leftDark3);
		rightDark3.flip(true, false);
		centerLit3 = atlas.findRegion("centerLit-3");
		centerDark3 = atlas.findRegion("centerDark-3");

		roofOrnament0 = atlas.findRegion("roofOrnament-0");
		roofOrnament1 = atlas.findRegion("roofOrnament-1");
		roofOrnament2 = atlas.findRegion("roofOrnament-2");
		roofOrnament3 = atlas.findRegion("roofOrnament-3");
		roofOrnament4 = atlas.findRegion("roofOrnament-4");
		roofOrnament5 = atlas.findRegion("roofOrnament-5");
		roofOrnament6 = atlas.findRegion("roofOrnament-6");
		roofOrnament7 = atlas.findRegion("roofOrnament-7");
	}

	/**
	 * Loads graphics for splash screen
	 */
	private void loadSplashGraphics() {
		log.debug("Loading splash-graphics");

		a_letter_big = new Texture("gfx/splashscreen/a_big.png");
		e_letter_big = new Texture("gfx/splashscreen/e_big.png");
		g_letter_big = new Texture("gfx/splashscreen/g_big.png");
		m_letter_big = new Texture("gfx/splashscreen/m_big.png");
		t_letter_big = new Texture("gfx/splashscreen/t_big.png");
		e_letter_small = new Texture("gfx/splashscreen/e_small.png");
		n_letter_small = new Texture("gfx/splashscreen/n_small.png");
		p_letter_small = new Texture("gfx/splashscreen/p_small.png");
		r_letter_small = new Texture("gfx/splashscreen/r_small.png");
		s_letter_small = new Texture("gfx/splashscreen/s_small.png");
		t_letter_small = new Texture("gfx/splashscreen/t_small.png");
	}

	/**
	 * Loads graphics for menu
	 */
	private void loadMenuGraphics() {
		log.debug("Loading menu-graphics");

		atlas = new TextureAtlas("gfx/menuscreen/buttons.pack");
		menu_logo = new Texture("gfx/menuscreen/axsc_logo.png");
		menu_skin = new Skin(atlas);
		menu_skin.add("logo", menu_logo);
	}

	/**
	 * Loads background graphics
	 */
	private void loadBackgroundGraphics() {
		log.debug("Loading background-graphics");

		skyBackground = new Texture(Gdx.files.internal("gfx/backgrounds/bkg_sky_image_test.png"));
		backgroundSkyline = new Texture(Gdx.files.internal("gfx/backgrounds/bkg_skyline1_blur.png"));
		mntLayerBack = new Texture(Gdx.files.internal("gfx/backgrounds/bkg_mountain_back_blur.png"));
		mntLayerFront = new Texture(Gdx.files.internal("gfx/backgrounds/bkg_mountain_front_blur.png"));
		water = new Texture(Gdx.files.internal("gfx/backgrounds/water.png"));
	}

	/**
	 * Loads graphics for main ships
	 */
	private void loadShipsGraphics() {
		log.debug("Loading ship-graphics");

		atlas = new TextureAtlas(Gdx.files.internal("gfx/characters/entities.pack"));
		playerShip = atlas.findRegion("player_ship");
		mainEnemy = atlas.findRegion("enemy_ship");
	}

	/**
	 * Loads graphics for weapons
	 */
	private void loadWeaponsGraphics() {
		log.debug("Loading weapons-graphics");
		atlas = new TextureAtlas(Gdx.files.internal("gfx/characters/entities.pack"));

		playerBullet = atlas.findRegion("player_bullet");
		enemyBullet = atlas.findRegion("enemy_bullet");
		mine = atlas.findRegion("enemy_mine");
	}

	/**
	 * Loads graphics for trash enemies
	 */
	private void loadTrashEnemyGraphics() {
		log.debug("Loading trashenemy-graphics");

		atlas = new TextureAtlas(Gdx.files.internal("gfx/characters/entities.pack"));

		thrustLeft = atlas.findRegion("thrust_left");

		sentryDrone = atlas.findRegion("sentry_drone");
		sentryDroneThrust = atlas.findRegion("thrust_down_x2");

		rotatorBody = atlas.findRegion("rotator_body");
		rotatorTurret = atlas.findRegion("rotator_turret");
		rotatorThrust = atlas.findRegion("thrust_left_s");

		meteorit = atlas.findRegion("meteorit");

		bigOne = atlas.findRegion("big");
		bigOneLaser = atlas.findRegion("laser");

	}

	/**
	 * Loads animations
	 */
	private void loadAnimations() {
		log.debug("Loading animations");

		atlas = new TextureAtlas(Gdx.files.internal("gfx/characters/explosion.pack"));
		TextureRegion[] explosionFrames = new TextureRegion[47];
		for (int i = 0; i < explosionFrames.length; i++) {
			explosionFrames[i] = atlas.findRegion("explosion (" + (i + 1) + ")");
		}
		explosion = new Animation(1 / 60f, explosionFrames);

		atlas = new TextureAtlas(Gdx.files.internal("gfx/characters/portal.pack"));
		TextureRegion[] portalFrames = new TextureRegion[10];
		for (int i = 0; i < portalFrames.length; i++) {
			portalFrames[i] = atlas.findRegion("portal-" + (i + 1));
		}
		portal = new Animation(1 / 30f, portalFrames);

	}

	/**
	 * Loads hud graphics
	 */
	private void loadHUDGraphics() {
		log.debug("Loading HUD graphics");

		bar_bkg = new Texture("gfx/hud/bar_bkg.png");
		player_bar = new Texture("gfx/hud/player_bar.png");
		enemy_bar = new Texture("gfx/hud/enemy_bar.png");
		meteorWarning = new Texture("gfx/hud/warmet.png");
		insertCoin = new Texture("gfx/hud/insert.png");
	}

}
