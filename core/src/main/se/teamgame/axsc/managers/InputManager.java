package se.teamgame.axsc.managers;

import org.apache.log4j.Logger;

import se.teamgame.axsc.screens.game.GameScreen;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;

/**
 * Input manager class that handles player input in game
 * 
 * @author marcus
 *
 */
public class InputManager extends InputAdapter {

	static Logger log = Logger.getLogger(InputManager.class);

	GameScreen game;

	private boolean wDown;
	private boolean aDown;
	private boolean sDown;
	private boolean dDown;

	/**
	 * Constructor
	 * 
	 * @param game
	 *            active game
	 */
	public InputManager(GameScreen game) {

		log.debug("Creating Input-manager");

		this.game = game;
	}

	@Override
	public boolean keyDown(int keycode) {

		switch (keycode) {
			case Keys.W:
				game.getLevel().getPlayerShip().setVelocityY(game.getLevel().getPlayerShip().SPEED);
				wDown = true;
				break;
			case Keys.A:
				game.getLevel().getPlayerShip().setVelocityX(-game.getLevel().getPlayerShip().SPEED);
				aDown = true;
				break;
			case Keys.S:
				game.getLevel().getPlayerShip().setVelocityY(-game.getLevel().getPlayerShip().SPEED);
				sDown = true;
				break;
			case Keys.D:
				game.getLevel().getPlayerShip().setVelocityX(game.getLevel().getPlayerShip().SPEED);
				dDown = true;
				break;
			case Keys.CONTROL_RIGHT:
				game.getLevel().getPlayerShip().setFiring(true);
				break;
			default:
				return false;

		}

		return true;
	}

	@Override
	public boolean keyUp(int keycode) {

		switch (keycode) {

			case Keys.ESCAPE:
				game.getLevel().exitToMenu();
				break;
			case Keys.B:
				game.getRenderer().toggleDebug();
				break;
			case Keys.F12:
				game.getLevel().toggleGodMode();
				break;
			case Keys.W:
				wDown = false;
				if (!sDown) {
					game.getLevel().getPlayerShip().setVelocityY(0);
				} else {
					game.getLevel().getPlayerShip().setVelocityY(-game.getLevel().getPlayerShip().SPEED);
				}
				break;
			case Keys.S:
				sDown = false;
				if (!wDown) {
					game.getLevel().getPlayerShip().setVelocityY(0);
				} else {
					game.getLevel().getPlayerShip().setVelocityY(game.getLevel().getPlayerShip().SPEED);
				}
				break;
			case Keys.A:
				aDown = false;
				if (!dDown) {
					game.getLevel().getPlayerShip().setVelocityX(0);
				} else {
					game.getLevel().getPlayerShip().setVelocityX(game.getLevel().getPlayerShip().SPEED);
				}
				break;
			case Keys.D:
				dDown = false;
				if (!aDown) {
					game.getLevel().getPlayerShip().setVelocityX(0);
				} else {
					game.getLevel().getPlayerShip().setVelocityX(-game.getLevel().getPlayerShip().SPEED);
				}
				break;
			case Keys.CONTROL_RIGHT:
				game.getLevel().getPlayerShip().setFiring(false);
				break;
			default:
				return false;

		}

		return true;
	}
}