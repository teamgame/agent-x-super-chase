package se.teamgame.axsc.pools;

import org.apache.log4j.Logger;

import se.teamgame.axsc.entities.weapons.Bullet;
import se.teamgame.axsc.managers.AssetManager;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

/**
 * Bullet pool class used to generate and recycle bullets
 * 
 * @author marcus
 *
 */
public class BulletPool {

	static Logger log = Logger.getLogger(BulletPool.class);

	public enum BulletType {
		PLAYER_BULLET, ENEMY_BULLET, MINE
	}

	private AssetManager manager;

	private Array<Bullet> pool;

	public BulletPool() {

		log.debug("Creating bullet-pool");

		manager = AssetManager.getInstance();
		pool = new Array<Bullet>();
	}

	/**
	 * 
	 * @param x
	 *            pos
	 * @param y
	 *            pos
	 * @param direction
	 *            in radians
	 * @param velocity
	 * @param type
	 *            BulletType
	 * @return a new bullet if no one exists in pool otherwise it will return a
	 *         recycled bullet
	 */
	public Bullet getBullet(float x, float y, double direction, float velocity, BulletType type) {

		if (pool.size == 0) {
			return new Bullet(x, y, direction, velocity, getTexture(type), type);
		} else {
			Bullet bullet = pool.pop();
			bullet.reset(x, y, direction, velocity, getTexture(type), type);
			return bullet;
		}
	}

	/**
	 * Returns a TextureRegion based on BulletType
	 * 
	 * @param type
	 *            BulletType
	 * @return TextureRegion
	 */
	private TextureRegion getTexture(BulletType type) {
		switch (type) {
			case PLAYER_BULLET:
				return manager.playerBullet;
			case ENEMY_BULLET:
				return manager.enemyBullet;
			case MINE:
				return manager.mine;
			default:
				return null;
		}
	}

	/**
	 * Adds a bullet to the pool
	 * 
	 * @param bullet
	 */
	public void recycle(Bullet bullet) {
		pool.add(bullet);
	}

	/**
	 * 
	 * @return pool size
	 */
	public int getPoolSize() {
		return pool.size;
	}

}
