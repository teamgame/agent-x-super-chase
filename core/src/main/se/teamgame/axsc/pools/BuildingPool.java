package se.teamgame.axsc.pools;

import org.apache.log4j.Logger;

import se.teamgame.axsc.entities.buildings.Building;
import se.teamgame.axsc.entities.buildings.BuildingBuilder;
import se.teamgame.axsc.entities.buildings.BuildingBuilder.BuildingHeight;
import se.teamgame.axsc.entities.buildings.BuildingBuilder.BuildingType;

import com.badlogic.gdx.utils.Array;

/**
 * Building pool class used to generate and recycle buildings
 * 
 * @author marcus
 *
 */
public class BuildingPool {

	static Logger log = Logger.getLogger(BuildingPool.class);

	private BuildingBuilder builder;

	private Array<Building> pool;

	public BuildingPool() {

		log.debug("Creating building-pool");

		builder = new BuildingBuilder();
		pool = new Array<Building>();
	}

	/**
	 * *
	 * 
	 * @param x
	 *            pos
	 * @param y
	 *            ypos
	 * @param buildingHeight
	 *            BuildingHeight
	 * @param type
	 *            BuildingTYpe
	 * @param scrollSpeed
	 * @return a new building if no one exists in pool otherwise it will return
	 *         a recycled building
	 */
	public Building getBuilding(float x, float y, BuildingHeight buildingHeight, BuildingType type, float scrollSpeed) {

		builder.prepareBuilder(type, buildingHeight);

		if (pool.size == 0) {
			return new Building(builder.getBuilding(), x, y, builder.getBlocksX(), builder.getBlocksY(), scrollSpeed);
		} else {
			Building building = pool.pop();
			building.reset(builder.getBuilding(), x, y, builder.getBlocksX(), builder.getBlocksY(), scrollSpeed);
			return building;
		}
	}

	/**
	 * Adds a building to the pool
	 * 
	 * @param building
	 */
	public void recycle(Building building) {
		pool.add(building);
	}

	/**
	 * 
	 * @return pool size
	 */
	public int getPoolSize() {
		return pool.size;
	}

}
