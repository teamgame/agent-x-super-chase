package se.teamgame.axsc.io;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import se.teamgame.axsc.gamestate.GameState;
import se.teamgame.axsc.managers.AssetManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

/**
 * IO class for the menu highscore system.
 * 
 * @author Mike
 * 
 */
public class IoHighscore {

	static Logger log = Logger.getLogger(IoHighscore.class);

	AssetManager manager;

	private FileHandle file;
	private String fileText;
	private String saveString;
	private String scoreString;

	/**
	 * Constructor
	 */
	public IoHighscore() {
		log.debug("Creating IO highscore class");
		manager = AssetManager.getInstance();
	}

	/**
	 * Saves the highscore if you keep yourself from getting killed.
	 * 
	 * @param state
	 *            checks if player lost ( 0 points ) or player win ( above 0
	 *            points ).
	 * @param letter1
	 *            First letter in player name.
	 * @param letter2
	 *            Second letter in player name.
	 * @param letter3
	 *            Third letter in player name.
	 * @param filename
	 *            name of save file to disk.
	 */
	public void saveHighscore(GameState state, Label letter1, Label letter2, Label letter3, String filename) {

		log.debug("Entering IO save");

		file = Gdx.files.local(filename);

		fileText = null;
		saveString = "";
		scoreString = "";

		if (state.getScore() != 0) {

			log.debug("Score is not zero");

			try {
				fileText = file.readString();
			} catch (Exception e) {
				log.debug("File not found!");
			}

			if (fileText == null) {
				log.debug("Creating new file from mocked data");
				saveString = manager.names;
				scoreString = state.getScore() + "," + letter1.getText().toString() + letter2.getText().toString()
						+ letter3.getText().toString() + ",";

			} else {
				log.debug("File found! Reading in data.");
				saveString = fileText;
				scoreString = state.getScore() + "," + letter1.getText().toString() + letter2.getText().toString()
						+ letter3.getText().toString() + ",";

			}

			saveString = sortingString(saveString, scoreString);

			file.writeString(saveString, false);
		}
	}

	/**
	 * A sorting method for the highscore list, sorts on how high your score is.
	 * 
	 * @param saveString
	 *            is the list from disk or mocked data from assets manager if
	 *            file not exists.
	 * @param scoreString
	 *            is the name and score that the player inputs.
	 * @return combines savestring and scorestring and returns a sorted list for
	 *         saving to disk.
	 */
	private String sortingString(String saveString, String scoreString) {

		log.debug("Entering IO sorting");

		String[] sortString1 = saveString.split(",");
		String[] sortString2 = scoreString.split(",");
		ArrayList<String> sortString3 = new ArrayList<String>();
		String returnString = "";
		boolean check = false;

		for (int i = 18; i >= 0; i -= 2) {

			if (Integer.parseInt(sortString2[0]) <= Integer.parseInt(sortString1[i]) && check == false) {
				sortString3.add(0, sortString2[1]);
				sortString3.add(0, sortString2[0]);
				check = true;
			}
			sortString3.add(0, sortString1[i + 1]);
			sortString3.add(0, sortString1[i]);

		}

		if (check == false) {
			sortString3.add(0, sortString2[1]);
			sortString3.add(0, sortString2[0]);
		}

		for (int j = 0; j < sortString3.size(); j++) {
			returnString = returnString + sortString3.get(j) + ",";
		}

		return returnString;
	}

	/**
	 * Loads the highscore file from disk or the mocked data from assets manager
	 * if file not exists.
	 * 
	 * @param filename
	 *            the filename to load.
	 * @return the loaded file as a string.
	 */
	public String[] loadHighscore(String filename) {

		log.debug("Entering IO load");

		file = Gdx.files.local(filename);

		fileText = null;

		try {
			fileText = file.readString();
		} catch (Exception e) {
			log.debug("File not found, reading from mocked file!");
			fileText = manager.names;
		}

		return fileText.split(",");
	}

}
