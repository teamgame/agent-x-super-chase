package se.teamgame.axsc.gamestate;

import org.apache.log4j.Logger;

import se.teamgame.axsc.entities.ships.mainenemy.MainEnemyShip;
import se.teamgame.axsc.entities.ships.player.PlayerShip;
import se.teamgame.axsc.levels.Level;
import se.teamgame.axsc.levels.Level.LevelState;

public class GameState {

	static Logger log = Logger.getLogger(GameState.class);

	private Level level;
	private PlayerShip player;
	private MainEnemyShip enemy;
	private int score;

	/**
	 * GameState class used to get the current state of the game
	 * 
	 * @param level
	 *            active level
	 */
	public GameState(Level level) {
		log.debug("Creating game-state class");

		this.level = level;
		player = level.getPlayerShip();
		enemy = level.getMainEnemy();

		score = level.getEndPoint();

	}

	/**
	 * 
	 * @return current score
	 */
	public int getScore() {
		return score;
	}

	/**
	 * 
	 * @return current player health
	 */
	public int getPlayerHealth() {
		return level.getPlayerShip().getHealth();
	}

	/**
	 * 
	 * @return current main enemy health
	 */
	public int getEnemyHealth() {
		return level.getMainEnemy().getHealth();
	}

	/**
	 * 
	 * @return current distance travelled
	 */
	public int getDistanceTravelled() {
		return level.getDistanceTravelled();
	}

	/**
	 * 
	 * @return current LevelState
	 */
	public LevelState getLevelState() {
		return level.getState();
	}

	/**
	 * 
	 * @return current distance to main enemy
	 */
	public int getDistanceToEnemy() {
		return (int) (enemy.getX() - player.getX());
	}

	/**
	 * 
	 * @return current combo count
	 */
	public int getCombo() {
		return level.getComboCount();
	}

	/**
	 * 
	 * @return current frames per second
	 */
	public int getFps() {
		return level.getFps();
	}

	/**
	 * 
	 * @return current endpoint of the lavel
	 */
	public int getEndpoint() {
		return level.getEndPoint();
	}

	/**
	 * Updates the score
	 */
	public void updateScore() {
		score = getEndpoint() - getDistanceTravelled() - getDistanceToEnemy();
	}

	/**
	 * 
	 * @param score
	 */
	public void setScore(int score) {
		this.score = score;
	}

}
