package se.teamgame.axsc.levels;

import java.nio.IntBuffer;

import org.apache.log4j.Logger;
import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Cursor;
import org.lwjgl.input.Mouse;

import se.teamgame.axsc.AgentXSuperChase;
import se.teamgame.axsc.entities.buildings.Building;
import se.teamgame.axsc.entities.ships.Ship.ShipState;
import se.teamgame.axsc.entities.trashenemies.TrashEnemy;
import se.teamgame.axsc.entities.weapons.Bullet;
import se.teamgame.axsc.levels.Level.Wave;
import se.teamgame.axsc.managers.AssetManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Scaling;

/**
 * LevelRenderer class used to render the level
 * 
 * @author marcus
 *
 */
public class LevelRenderer {

	static Logger log = Logger.getLogger(LevelRenderer.class);

	private boolean debug = false;

	private OrthographicCamera camera;
	private ShapeRenderer shapeRenderer;
	private SpriteBatch batch;

	private Cursor emptyCursor;

	private BitmapFont debugFont;
	private BitmapFont arkitech30;
	private BitmapFont arkitech60;
	private BitmapFont arkitech100;

	private Level level;

	/**
	 * Constructor
	 * 
	 * @param level
	 *            to render
	 */
	public LevelRenderer(Level level) {

		log.debug("Creating level-renderer");

		this.level = level;

		camera = new OrthographicCamera();
		camera.setToOrtho(false, AgentXSuperChase.WIDTH, AgentXSuperChase.HEIGHT);
		batch = new SpriteBatch();
		batch.setProjectionMatrix(camera.combined);

		shapeRenderer = new ShapeRenderer();
		shapeRenderer.setProjectionMatrix(camera.combined);
		debugFont = new BitmapFont();
		debugFont.scale(1f);
		debugFont.setColor(1, 1, 1, 0.7f);
		arkitech30 = AssetManager.getInstance().arkitech30;
		arkitech60 = AssetManager.getInstance().arkitech60;
		arkitech100 = AssetManager.getInstance().arkitech100;

	}

	/**
	 * Renders the level
	 */
	public void render() {
		Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		batch.begin();

		level.getBackground().render(batch);

		for (Building building : level.getBuildings()) {
			building.render(batch);
		}
		for (Bullet bullet : level.getBullets()) {
			bullet.render(batch);
		}
		for (TrashEnemy enemy : level.getEnemies()) {
			enemy.render(batch);
		}

		level.getWater().render(batch);

		level.getMainEnemy().render(batch);
		level.getPlayerShip().render(batch);

		batch.end();

		shapeRenderer.begin(ShapeType.Filled);
		shapeRenderer.setColor(.02f, .02f, .02f, 1);
		shapeRenderer.rect(0, 140, camera.viewportWidth, 20);
		shapeRenderer.end();

		batch.begin();
		arkitech30.draw(batch, "Combo: " + level.getComboCount(), 10, 100);

		if (level.getWave() == Wave.METEORITES && level.getWaveTime() < 3) {
			batch.draw(AssetManager.getInstance().meteorWarning,
					(1920 - AssetManager.getInstance().meteorWarning.getWidth()) / 2, 550);
		}

		if (level.getPlayerShip().getState() == ShipState.DEAD) {
			batch.draw(AssetManager.getInstance().insertCoin,
					(1920 - AssetManager.getInstance().insertCoin.getWidth()) / 2, 700);

			arkitech100.setFixedWidthGlyphs("0123456789");
			arkitech100.draw(batch, String.format("%02d", level.getInsertCoinTimer()), 840, 630);
			arkitech60.draw(batch, "Credits: " + level.getCoins(), 720, 230);
			arkitech60.draw(batch, "press ENTER to insert coin", 280, 150);
		}
		batch.end();

		if (debug) {
			debugRender();
		}

		level.getHud().render(batch);
	}

	/**
	 * Renders debug information
	 */
	private void debugRender() {
		boolean collision = false;

		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		shapeRenderer.begin(ShapeType.Filled);

		level.getPlayerShip().debugRender(shapeRenderer);
		level.getMainEnemy().debugRender(shapeRenderer);
		level.getMainEnemyAi().debugRender(shapeRenderer);

		for (Building building : level.getBuildings()) {
			building.debugRender(shapeRenderer);
		}

		for (Bullet bullet : level.getBullets()) {
			bullet.debugRender(shapeRenderer);
		}
		for (TrashEnemy enemy : level.getEnemies()) {
			enemy.debugRender(shapeRenderer);
		}

		shapeRenderer.setColor(0, 0, 0, 0.3f);
		shapeRenderer.rect(0, camera.viewportHeight - 335, 400, 335);

		shapeRenderer.end();
		Gdx.gl.glDisable(GL20.GL_BLEND);

		batch.begin();
		debugFont.draw(batch, "FPS: " + level.getFps(), 10, camera.viewportHeight - 10);
		debugFont.draw(batch, "Scroll speed: " + (int) -level.getScrollSpeed(), 10, camera.viewportHeight - 35);
		debugFont.draw(batch, "Building-height: " + level.getBuildingHeight(), 10, camera.viewportHeight - 60);
		debugFont.draw(batch, "Buildings active: " + level.getBuildingsOnScreen(), 10, camera.viewportHeight - 85);
		debugFont.draw(batch, "Buildings in pool: " + level.getBuildingPoolSize(), 10, camera.viewportHeight - 110);
		debugFont.draw(batch, "Player pos (x,y): " + (int) level.getPlayerShip().getX() + ","
				+ (int) level.getPlayerShip().getY(), 10, camera.viewportHeight - 135);
		debugFont.draw(batch, "Collision: " + collision, 10, camera.viewportHeight - 160);
		debugFont.draw(batch, "Distance to enemy: "
				+ (int) (level.getMainEnemy().getX() - level.getPlayerShip().getX()), 10, camera.viewportHeight - 185);
		debugFont.draw(batch, "Bullets active: " + level.getBulletsOnScreen(), 10, camera.viewportHeight - 210);
		debugFont.draw(batch, "Bullets in pool: " + level.getBulletPoolSize(), 10, camera.viewportHeight - 235);
		debugFont.draw(batch, "Distance traveled: " + level.getDistanceTravelled(), 10, camera.viewportHeight - 260);
		debugFont.draw(batch, "Game-state: " + level.getState(), 10, camera.viewportHeight - 285);
		debugFont.draw(batch, "Wave-type: " + level.getWave(), 10, camera.viewportHeight - 310);

		batch.end();

	}

	/**
	 * Run every time the the game screen is shown
	 */
	public void show() {
		hideCursor(true);
	}

	/**
	 * Run every time the game screen id hidden
	 */
	public void hide() {

		hideCursor(false);

	}

	/**
	 * Called when the screen resizes
	 * 
	 * @param width
	 * @param height
	 */
	public void resize(int width, int height) {
		Vector2 size = Scaling.fit.apply(1920, 1080, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		Gdx.gl.glViewport((int) (width - size.x) / 2, (int) (height - size.y) / 2, (int) size.x, (int) size.y);
	}

	/**
	 * Disposes shape renderer
	 */
	public void dispose() {
		log.debug("Diposing shapeRenderer");
		shapeRenderer.dispose();
	}

	/**
	 * Toggles rendering of debug info
	 */
	public void toggleDebug() {
		debug = !debug;
	}

	/**
	 * Creates an invisible cursor and change cursor to the invisible one if
	 * passed true
	 * 
	 * @param hidden
	 *            hide the cursor or not
	 */
	private void hideCursor(boolean hidden) {
		if (emptyCursor == null) {
			int min = Cursor.getMinCursorSize();
			IntBuffer tmp = BufferUtils.createIntBuffer(min * min);
			try {
				emptyCursor = new Cursor(min, min, min / 2, min / 2, 1, tmp, null);
			} catch (LWJGLException e) {
				log.error("Couldn't create empty cursor");
			}
		}
		if (Mouse.isInsideWindow())
			try {
				Mouse.setNativeCursor(hidden ? emptyCursor : null);
			} catch (LWJGLException e) {
				log.error("Could set empty cursor");
			}
	}

}
