package se.teamgame.axsc.levels;

import java.util.Random;

import org.apache.log4j.Logger;

import se.teamgame.axsc.AgentXSuperChase;
import se.teamgame.axsc.entities.ai.MainEnemyAi;
import se.teamgame.axsc.entities.background.ParalaxBackground;
import se.teamgame.axsc.entities.background.ScrollingLayer;
import se.teamgame.axsc.entities.buildings.Building;
import se.teamgame.axsc.entities.buildings.BuildingBuilder.BuildingHeight;
import se.teamgame.axsc.entities.buildings.BuildingBuilder.BuildingType;
import se.teamgame.axsc.entities.ships.Ship.ShipState;
import se.teamgame.axsc.entities.ships.mainenemy.MainEnemyShip;
import se.teamgame.axsc.entities.ships.player.PlayerShip;
import se.teamgame.axsc.entities.trashenemies.BigOne;
import se.teamgame.axsc.entities.trashenemies.Meteorit;
import se.teamgame.axsc.entities.trashenemies.Rotator;
import se.teamgame.axsc.entities.trashenemies.SentryDrone;
import se.teamgame.axsc.entities.trashenemies.TrashEnemy;
import se.teamgame.axsc.entities.trashenemies.TrashEnemy.TrashState;
import se.teamgame.axsc.entities.weapons.Bullet;
import se.teamgame.axsc.gamestate.GameState;
import se.teamgame.axsc.hud.Hud;
import se.teamgame.axsc.managers.AssetManager;
import se.teamgame.axsc.pools.BuildingPool;
import se.teamgame.axsc.pools.BulletPool;
import se.teamgame.axsc.pools.BulletPool.BulletType;
import se.teamgame.axsc.screens.menu.GameOverScreen;
import se.teamgame.axsc.screens.menu.MenuScreen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.utils.Array;

public class Level {

	static Logger log = Logger.getLogger(Level.class);

	public enum LevelState {
		START, ENEMY_PORTAL, PLAYER_PORTAL, CHASE, BATTLE, GAME_OVER
	}

	public enum Wave {
		NONE, SENTRY_DRONES, ROTATORS, METEORITES, BLOCKADE
	}

	public final float BASE_SPEED = -550;

	private PlayerShip playerShip;
	private MainEnemyShip mainEnemy;
	private MainEnemyAi mainEnemyAi;
	private BigOne bigOne;
	private Array<Bullet> bullets;
	private Array<Building> buildings;
	private Array<TrashEnemy> enemies;
	private ParalaxBackground background;
	private ScrollingLayer water;
	private Hud hud;
	private Random random;

	private boolean godMode;

	private int fps;
	private float longDelta;

	private LevelState state;
	private float stateTime;

	private BulletPool bulletPool;
	private BuildingPool buildingPool;
	private float buildingsWidth;
	private float lastTail;
	private BuildingHeight buildingHeight;
	private int buildingsInState;
	private GameState gameState;
	private int distanceTravelled;
	private int endPoint;

	private boolean bigOnePresent = false;

	private int comboCount;
	private float comboTimer;

	private Wave wave;
	private float waveTime;
	private int waveDuration;

	private float regenTimer;

	private float blockadeTimer;

	private int buildingSwitchProbability;
	private int buildingsTogether;

	private int coins;
	private int insertCoinTimer;

	/**
	 * Constructor
	 */
	public Level() {

		log.debug("Creating level");

		godMode = false;

		state = LevelState.START;
		stateTime = 0;

		wave = Wave.NONE;
		waveTime = 0;
		waveDuration = 2;

		bulletPool = new BulletPool();
		buildingPool = new BuildingPool();
		random = new Random();

		playerShip = new PlayerShip(400, 850, this);

		mainEnemy = new MainEnemyShip(400, 850, this);
		mainEnemyAi = new MainEnemyAi(this, bulletPool);

		background = new ParalaxBackground(BASE_SPEED);
		water = new ScrollingLayer(AssetManager.getInstance().water, BASE_SPEED * 1.5f);

		bullets = new Array<Bullet>();
		buildings = new Array<Building>();
		enemies = new Array<TrashEnemy>();
		buildingsWidth = 0;
		lastTail = 0;
		buildingHeight = BuildingHeight.LOW;
		buildingsInState = 0;
		distanceTravelled = 0;
		endPoint = 500000;

		buildingSwitchProbability = 1000;
		buildingsTogether = 0;

		comboCount = 0;
		comboTimer = 0;

		regenTimer = 0;

		while (buildingsWidth <= AgentXSuperChase.WIDTH) {
			addBuilding();
		}

		gameState = new GameState(this);

		hud = new Hud(gameState);

		coins = 3;
		insertCoinTimer = 0;

	}

	/**
	 * executes Hud.show()
	 */
	public void show() {
		hud.show();
	}

	/**
	 * Updates the level
	 * 
	 * @param delta
	 *            time since last time the method was executed
	 */
	public void update(float delta) {

		stateTime += delta;

		switch (state) {
			case START:
				start(delta);
				break;
			case ENEMY_PORTAL:
				enemyPortal(delta);
				break;
			case PLAYER_PORTAL:
				playerPortal(delta);
				break;
			case CHASE:
				chase(delta);
				break;
			case BATTLE:
				battle(delta);
				break;
			case GAME_OVER:
				gameOver(delta);
				break;
			default:
				log.error("Invalid level-state");
				break;
		}

		updateFPS(delta);
		hud.update(delta);
	}

	/**
	 * Adds a building to the level
	 */
	private void addBuilding() {

		if (random.nextBoolean() || buildingsTogether > 4) {
			lastTail += random.nextInt(50 - 10) + 10;
			buildingsTogether = 0;
		}

		int type = random.nextInt(3);

		if (type == 0) {
			buildings.add(buildingPool.getBuilding(lastTail, 150, buildingHeight, BuildingType.A, BASE_SPEED));
		} else if (type == 1) {
			buildings.add(buildingPool.getBuilding(lastTail, 150, buildingHeight, BuildingType.B, BASE_SPEED));
		} else {
			buildings.add(buildingPool.getBuilding(lastTail, 150, buildingHeight, BuildingType.C, BASE_SPEED));
		}

		lastTail = buildings.peek().getTailX();
		buildingsWidth += buildings.peek().getWidth();
		buildingsInState++;
		buildingsTogether++;
		if (state != LevelState.START) {
			changeBuildingHeight();
		}

	}

	/**
	 * Randomly change building height based on some simple rules
	 */
	private void changeBuildingHeight() {
		if (buildingsInState > random.nextInt(buildingSwitchProbability)) {
			switch (buildingHeight) {
				case LOW:
					if (random.nextInt(3) != 0) {
						buildingHeight = BuildingHeight.MID;
						buildingSwitchProbability = 200;
					} else {
						buildingHeight = BuildingHeight.HIGH;
						buildingSwitchProbability = 100;
					}
					break;
				case MID:
					if (random.nextInt(3) != 0) {
						buildingHeight = BuildingHeight.LOW;
						buildingSwitchProbability = 1000;
					} else {
						buildingHeight = BuildingHeight.HIGH;
						buildingSwitchProbability = 100;
					}
					break;
				case HIGH:
					if (random.nextInt(3) != 0) {
						buildingHeight = BuildingHeight.LOW;
						buildingSwitchProbability = 1000;
					} else {
						buildingHeight = BuildingHeight.MID;
						buildingSwitchProbability = 200;
					}
					break;
				default:
					log.error("Invalid building-height");
					return;
			}

			log.debug("Changed building height to: " + buildingHeight.toString());

			buildingsInState = 0;
		}
	}

	/**
	 * Logic for when level is in start state
	 * 
	 * @param delta
	 *            time since last update
	 */
	private void start(float delta) {
		water.setScrollSpeed(-40);
		water.update(delta);

		if (stateTime > 1) {
			changeState(LevelState.ENEMY_PORTAL);
			mainEnemy.setState(ShipState.PORTAL);
		}
	}

	/**
	 * Logic for when level is in enemy portal state
	 * 
	 * @param delta
	 *            time since last update
	 */
	private void enemyPortal(float delta) {
		water.update(delta);
		mainEnemy.update(delta);

		if (stateTime > 1.5f) {
			changeState(LevelState.PLAYER_PORTAL);
			playerShip.setState(ShipState.PORTAL);
		}
	}

	/**
	 * Logic for when level is in player portal state
	 * 
	 * @param delta
	 *            time since last update
	 */
	private void playerPortal(float delta) {
		water.update(delta);
		playerShip.update(delta);
		mainEnemy.setVelocityX(40000);
		mainEnemy.update(delta);

		if (playerShip.getState() == ShipState.FLYING) {
			changeState(LevelState.CHASE);
		}
	}

	/**
	 * Logic for when level is in chase state
	 * 
	 * @param delta
	 *            time since last update
	 */
	private void chase(float delta) {
		waveTime += delta;

		if (waveTime > waveDuration) {
			newWave();
		}

		spawnEnemies(delta);

		comboTimer += delta;
		if (comboTimer > 1f) {
			if (comboCount > 0) {
				comboCount -= 1;
			}
			comboTimer = 0;
		}

		distanceTravelled -= getScrollSpeed() * delta;

		updateBackground(delta);

		updatePlayer(delta);
		updateMainEnemy(delta);

		updateBuildings(delta);
		updateEnemies(delta);
		updateBullets(delta);

		checkCollisions();

		if (checkGameOver()) {
			changeState(LevelState.GAME_OVER);
		} else {
			gameState.updateScore();
		}

		if (mainEnemy.getX() < AgentXSuperChase.WIDTH) {
			state = LevelState.BATTLE;
		}

	}

	/**
	 * Logic for when level is in battle state
	 * 
	 * @param delta
	 *            time since last update
	 */
	private void battle(float delta) {

		distanceTravelled -= getScrollSpeed() * delta;

		wave = Wave.NONE;

		updateBackground(delta);

		updatePlayer(delta);
		updateMainEnemy(delta);

		updateBuildings(delta);
		updateEnemies(delta);
		updateBullets(delta);

		checkCollisions();

		checkGameOver();

		if (checkGameOver()) {
			changeState(LevelState.GAME_OVER);
		} else {
			gameState.updateScore();
		}

		if (mainEnemy.getX() > AgentXSuperChase.WIDTH) {
			changeState(LevelState.GAME_OVER);
			state = LevelState.CHASE;
		}
	}

	/**
	 * Logic for when level is in game over state
	 * 
	 * @param delta
	 *            time since last update
	 */
	private void gameOver(float delta) {

		insertCoinTimer = (int) (11 - stateTime);

		if (playerShip.getState() == ShipState.DEAD && (stateTime > 10 || Gdx.input.isKeyPressed(Keys.ESCAPE))) {
			if (bigOnePresent) {
				bigOne.silence();
			}
			log.debug("Exiting game");
			((Game) Gdx.app.getApplicationListener()).setScreen(new GameOverScreen(gameState));
		}

		if (playerShip.getState() == ShipState.FLYING && (stateTime > 2 || Gdx.input.isKeyPressed(Keys.ESCAPE))) {
			if (bigOnePresent) {
				bigOne.silence();
			}
			log.debug("Exiting game");
			((Game) Gdx.app.getApplicationListener()).setScreen(new GameOverScreen(gameState));
		}

		updateBackground(delta);

		updatePlayer(delta);
		updateMainEnemy(delta);

		updateBuildings(delta);
		updateEnemies(delta);
		updateBullets(delta);

		checkCollisions();

		if (coins > 0 && playerShip.getState() == ShipState.DEAD && Gdx.input.isKeyPressed(Keys.ENTER)) {
			if (bigOnePresent) {
				bigOne.silence();
			}
			coins--;
			log.debug("Resuming game, coins left: " + coins);
			resumeGame();
		}
	}

	/**
	 * updates player ship
	 * 
	 * @param delta
	 *            time since last update
	 */
	private void updatePlayer(float delta) {
		regenTimer += delta;
		playerShip.update(delta);

		if (regenTimer > .5) {

			if (playerShip.getHealth() < 100 && playerShip.getState() != ShipState.DEAD) {
				if (bigOnePresent) {
					bigOne.silence();
				}
				playerShip.setHealth(playerShip.getHealth() + 1);
			}
			regenTimer = 0;
		}
	}

	/**
	 * updates main enemy ship
	 * 
	 * @param delta
	 *            time since last update
	 */
	private void updateMainEnemy(float delta) {
		mainEnemy.setVelocityX(-(playerShip.getX() - 860));
		mainEnemyAi.update(delta);
		mainEnemy.update(delta);
		if (getState() == LevelState.CHASE) {
			mainEnemy.setHealth(100);
		}
	}

	/**
	 * updates trash enemies
	 * 
	 * @param delta
	 *            time since last update
	 */
	private void updateEnemies(float delta) {
		for (TrashEnemy enemy : enemies) {
			enemy.update(delta);
			if (!enemy.isActive()) {
				enemies.removeValue(enemy, true);
				if (enemy instanceof BigOne) {
					bigOnePresent = false;
				}
			}
		}
	}

	/**
	 * updates background
	 * 
	 * @param delta
	 *            time since last update
	 */
	private void updateBackground(float delta) {
		background.update(delta, getScrollSpeed());
		water.setScrollSpeed(getScrollSpeed() * 1.2f);
		water.update(delta);
	}

	/**
	 * updates buildings
	 * 
	 * @param delta
	 *            time since last update
	 */
	private void updateBuildings(float delta) {
		for (Building building : buildings) {
			building.setScrollSpeed(getScrollSpeed());
			building.update(delta);
		}
		if (buildings.get(0).isScrolledLeft()) {
			Building building = buildings.removeIndex(0);
			buildingsWidth -= building.getWidth();
			buildingPool.recycle(building);

		}
		while (buildingsWidth <= AgentXSuperChase.WIDTH + 300) {
			lastTail = buildings.peek().getTailX();
			addBuilding();
		}
	}

	/**
	 * updates bullets
	 * 
	 * @param delta
	 *            time since last update
	 */
	private void updateBullets(float delta) {
		for (Bullet bullet : bullets) {
			if (bullet.getType() == BulletType.MINE) {
				bullet.setVelocity(-getScrollSpeed() / 2);
			}
			bullet.update(delta);
			if (!bullet.isActive()) {
				bulletPool.recycle(bullet);
				bullets.removeValue(bullet, true);
			}
		}
	}

	/**
	 * checks for collisions between game entities
	 */
	private void checkCollisions() {
		checkBuildingCollisions();
		checkEnemyCollisions();
		checkBulletCollisions();
	}

	/**
	 * checks for collision with buildings
	 */
	private void checkBuildingCollisions() {

		for (Building building : buildings) {
			if (!godMode && playerShip.colides(building.getBoundingBox()) && playerShip.getState() != ShipState.DEAD) {
				playerShip.hit(100);
			}
			for (TrashEnemy enemy : enemies) {
				if (enemy.colides(building.getBoundingBox()) && enemy.getState() != TrashState.DEAD) {
					enemy.setState(TrashState.DEAD);
				}
			}
		}
	}

	/**
	 * checks for collision with bullets
	 */
	private void checkBulletCollisions() {

		for (Bullet bullet : bullets) {
			if (!godMode && playerShip.colides(bullet.getBoundingBox()) && playerShip.getState() != ShipState.DEAD) {
				playerShip.hit(20);
				bullets.removeValue(bullet, true);

			}
			if (bullet.getType() == BulletType.PLAYER_BULLET && mainEnemy.colides(bullet.getBoundingBox())
					&& mainEnemy.getState() != ShipState.DEAD) {
				bullets.removeValue(bullet, true);
				mainEnemy.hit(10);

			}
			for (TrashEnemy enemy : enemies) {
				if (bullet.getType() == BulletType.PLAYER_BULLET && enemy.colides(bullet.getBoundingBox())
						&& enemy.getState() != TrashState.DEAD) {
					bullets.removeValue(bullet, true);
					enemy.setState(TrashState.DEAD);
					endPoint += enemy.getScore();

					if (!bigOnePresent) {
						comboCount += 1;
						comboTimer = 0;
					}
				}
			}
		}

		if (!godMode && bigOne != null && bigOne.isFiring() && playerShip.colides(bigOne.getLaser())) {
			playerShip.setState(ShipState.DEAD);
		}
	}

	/**
	 * checks for collisions with enemies
	 */
	private void checkEnemyCollisions() {

		if (!godMode && mainEnemy.getState() != ShipState.DEAD && playerShip.colides(mainEnemy.getBoundingBox())
				&& playerShip.getState() != ShipState.DEAD) {
			playerShip.hit(100);
		}

		for (TrashEnemy enemy : enemies) {
			if (!godMode && enemy.getState() != TrashState.DEAD && playerShip.colides(enemy.getBoundingBox())
					&& playerShip.getState() != ShipState.DEAD) {
				playerShip.hit(100);
				enemy.setState(TrashState.DEAD);
			}

			if (bigOne != null && bigOne.isFiring() && enemy instanceof BigOne == false
					&& enemy.getState() != TrashState.DEAD && enemy.colides(bigOne.getLaser()) || bigOne != null
					&& enemy instanceof BigOne == false && enemy.getState() != TrashState.DEAD
					&& enemy.colides(bigOne.getBoundingBox())) {
				enemy.setState(TrashState.DEAD);
			}
		}
	}

	/**
	 * checks for game over and sets score acordingly
	 * 
	 * @return true if game over
	 */
	private boolean checkGameOver() {
		if (mainEnemy.getState() == ShipState.DEAD) {
			return true;
		} else if (playerShip.getState() == ShipState.DEAD) {
			gameState.setScore(0);
			return true;
		} else if (gameState.getScore() < 1) {
			gameState.setScore(0);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Randomly switch to new wave type based on some simple rules
	 */
	private void newWave() {
		int nextWave = random.nextInt(5);

		if (enemies.size <= 3) {
			switch (nextWave) {
				case 0:
					changeWave(Wave.NONE);
					waveDuration = 1;
					break;
				case 1:
					changeWave(Wave.SENTRY_DRONES);
					waveDuration = 10;
					break;
				case 2:
					changeWave(Wave.ROTATORS);
					waveDuration = 10;
					break;
				case 3:
					changeWave(Wave.METEORITES);
					waveDuration = 13;
					break;
				case 4:
					changeWave(Wave.BLOCKADE);
					blockadeTimer = waveTime;
					waveDuration = 6;
					break;
				default:
					changeWave(Wave.NONE);
					waveDuration = 1;
					break;
			}
		} else {
			changeWave(Wave.NONE);
			waveDuration = 0;
		}

	}

	/**
	 * Spawns enenies based on wave type active
	 * 
	 * @param delta
	 *            time since last update
	 */
	private void spawnEnemies(float delta) {

		switch (wave) {
			case NONE:
				break;
			case SENTRY_DRONES:
				if (random.nextInt(45) < 1) {
					enemies.add(new SentryDrone(AgentXSuperChase.WIDTH + 300, random.nextInt(1000 - 400) + 400, 300,
							this));
				}
				break;
			case ROTATORS:
				if (random.nextInt(35) < 1) {
					enemies.add(new Rotator(AgentXSuperChase.WIDTH + 300, random.nextInt(1000 - 700) + 700, 480, this));
				}
				break;
			case METEORITES:
				if (waveTime > 3 && random.nextInt(25) < 1) {
					enemies.add(new Meteorit(random.nextInt(AgentXSuperChase.WIDTH - 600) + 1000,
							AgentXSuperChase.HEIGHT, this));
				}
				break;
			case BLOCKADE:
				blockadeTimer += delta;
				if (blockadeTimer > 3f) {
					for (int i = 1000; i > 100; i -= 150) {
						enemies.add(new SentryDrone(AgentXSuperChase.WIDTH + 420, i, 200, this));
						enemies.add(new Rotator(AgentXSuperChase.WIDTH + 300, i - 75, 200, this));
					}
					blockadeTimer = 0;
				}

				break;
			default:
				log.error("Invalid wave-type");
				break;
		}

		if (!bigOnePresent && comboCount > 14) {
			bigOne = new BigOne(-280, playerShip.getY() - 110, this);
			enemies.add(bigOne);
			bigOnePresent = true;

			endPoint += bigOne.getScore();
			comboCount = 0;
		}
	}

	/**
	 * Change wave type
	 * 
	 * @param wave
	 */
	private void changeWave(Wave wave) {
		this.wave = wave;
		waveTime = 0;

		log.debug("New wave: " + wave);
	}

	/**
	 * Change LevelState
	 * 
	 * @param state
	 */
	private void changeState(LevelState state) {
		this.state = state;
		stateTime = 0;

		log.debug("New state: " + state);
	}

	/**
	 * Updates the fps
	 * 
	 * @param delta
	 *            time since last update
	 */
	private void updateFPS(float delta) {
		longDelta += delta;
		if (longDelta > .5f) {
			longDelta = 0;
			fps = (int) (1 / delta);
		}
	}

	/**
	 * Resumes game after death
	 */
	private void resumeGame() {
		playerShip.setPositionX(400);
		playerShip.setPositionY(800);
		playerShip.setHealth(80);
		mainEnemy.setPositionX(mainEnemy.getX() * 1.2f);
		gameState.setScore(endPoint - distanceTravelled - gameState.getDistanceToEnemy());
		playerShip.setState(ShipState.FLYING);
		enemies.clear();
		bullets.clear();
		bigOne = null;
		bigOnePresent = false;
		comboCount = 0;
		state = LevelState.CHASE;

	}

	/**
	 * Silence the big one if it is present and quits to menu
	 */
	public void exitToMenu() {
		if (bigOnePresent) {
			bigOne.silence();
		}
		((Game) Gdx.app.getApplicationListener()).setScreen(new MenuScreen());

	}

	/**
	 * Toggles god-mode
	 */
	public void toggleGodMode() {
		godMode = !godMode;
	}

	/**
	 * 
	 * @return LevelState
	 */
	public LevelState getState() {
		return state;
	}

	/**
	 * 
	 * @return wave type
	 */
	public Wave getWave() {
		return wave;
	}

	/**
	 * 
	 * @return time in current wave
	 */
	public float getWaveTime() {
		return waveTime;
	}

	/**
	 * 
	 * @return frames per second
	 */
	public int getFps() {
		return fps;
	}

	/**
	 * 
	 * @return comboo count
	 */
	public int getComboCount() {
		return comboCount;
	}

	/**
	 * 
	 * @return BuildingHeight
	 */
	public BuildingHeight getBuildingHeight() {
		return buildingHeight;
	}

	/**
	 * 
	 * @return numbers of buildings on screen
	 */
	public int getBuildingsOnScreen() {
		return buildings.size;
	}

	/**
	 * 
	 * @return size of building pool
	 */
	public int getBuildingPoolSize() {
		return buildingPool.getPoolSize();
	}

	/**
	 * 
	 * @return bulletPool
	 */
	public BulletPool getBulletPool() {
		return bulletPool;
	}

	/**
	 * 
	 * @return number bullets on screen
	 */
	public int getBulletsOnScreen() {
		return bullets.size;
	}

	/**
	 * 
	 * @return size of bullet pool
	 */
	public int getBulletPoolSize() {
		return bulletPool.getPoolSize();
	}

	/**
	 * 
	 * @return scroll speed
	 */
	public float getScrollSpeed() {
		return BASE_SPEED - playerShip.getX() * 1.5f;
	}

	/**
	 * 
	 * @return distance travelled
	 */
	public int getDistanceTravelled() {
		return distanceTravelled;
	}

	public ParalaxBackground getBackground() {
		return background;
	}

	/**
	 * 
	 * @return water layer
	 */
	public ScrollingLayer getWater() {
		return water;
	}

	/**
	 * 
	 * @return buildings
	 */
	public Array<Building> getBuildings() {
		return buildings;
	}

	/**
	 * 
	 * @return bullets
	 */
	public Array<Bullet> getBullets() {
		return bullets;
	}

	/**
	 * 
	 * @return player ship
	 */
	public PlayerShip getPlayerShip() {
		return playerShip;
	}

	/**
	 * 
	 * @return main enemy ship
	 */
	public MainEnemyShip getMainEnemy() {
		return mainEnemy;
	}

	/**
	 * 
	 * @return main enemy AI
	 */
	public MainEnemyAi getMainEnemyAi() {
		return mainEnemyAi;
	}

	/**
	 * 
	 * @return trash enemies
	 */
	public Array<TrashEnemy> getEnemies() {
		return enemies;
	}

	/**
	 * 
	 * @return HUD
	 */
	public Hud getHud() {
		return hud;
	}

	/**
	 * 
	 * @return endpoint
	 */
	public int getEndPoint() {
		return endPoint;
	}

	/**
	 * 
	 * @return coins
	 */
	public int getCoins() {
		return coins;
	}

	/**
	 * 
	 * @return insert coin timer
	 */
	public int getInsertCoinTimer() {
		return insertCoinTimer;
	}

}
