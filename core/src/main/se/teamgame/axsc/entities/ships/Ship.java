package se.teamgame.axsc.entities.ships;

import se.teamgame.axsc.levels.Level;
import se.teamgame.axsc.managers.AssetManager;
import se.teamgame.axsc.pools.BulletPool;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Abstract ship class that main ships inherit from.
 * 
 * @author marcus
 *
 */
public abstract class Ship {

	public enum ShipState {
		START, PORTAL, FLYING, DEAD;
	}

	protected ShipState state;
	protected float stateTime;

	protected AssetManager manager;
	protected Level level;
	protected BulletPool bulletPool;

	protected Vector2 position;
	protected Vector2 velocity;

	protected Sprite bodySprite;
	protected Animation portal;
	protected Animation explosion;
	protected Rectangle[] boundingBox;

	protected Sound[] shootSounds;
	protected Sound portalSound;
	protected Sound explosionSound;
	protected boolean portalPlaying;
	protected boolean explosionPlaying;

	protected int health;

	/**
	 * Constructor
	 * 
	 * @param x
	 *            position
	 * @param y
	 *            position
	 * @param level
	 *            active level
	 */
	public Ship(float x, float y, Level level) {
		position = new Vector2(x, y);
		velocity = new Vector2();
		this.level = level;

		bulletPool = level.getBulletPool();
		manager = AssetManager.getInstance();

		health = 100;

		setupSprites();
		setupPortalAnimation();
		setupExplosionAnimation();
		setupBoundingBox();
		setupSounds();
	}

	/**
	 * Sets up sprites
	 */
	protected abstract void setupSprites();

	/**
	 * Sets up portal animation
	 */
	protected abstract void setupPortalAnimation();

	/**
	 * Sets up explosion animation
	 */
	protected abstract void setupExplosionAnimation();

	/**
	 * Sets up bounding box
	 */
	protected abstract void setupBoundingBox();

	/**
	 * Sets up sounds
	 */
	protected abstract void setupSounds();

	/**
	 * Renders the ship
	 * 
	 * @param batch
	 *            used to draw on
	 */
	public abstract void render(SpriteBatch batch);

	/**
	 * Renders debug information
	 * 
	 * @param renderer
	 *            ShapeRenderer used to render
	 */
	public abstract void debugRender(ShapeRenderer renderer);

	/**
	 * Updates the ship
	 * 
	 * @param delta
	 *            time since last time the method was executed
	 */
	public abstract void update(float delta);

	/**
	 * Checks for collision with a rectangle shape
	 * 
	 * @param rect
	 *            rectangle
	 * @return true if collision
	 */
	public boolean colides(Rectangle rect) {
		for (Rectangle box : boundingBox) {
			if (box.overlaps(rect)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks for collision with a circle shape
	 * 
	 * @param circle
	 * @return true if collision
	 */
	public boolean colides(Circle circle) {
		for (Rectangle box : boundingBox) {
			if (Intersector.overlaps(circle, box)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks for collision with an array of rectangle shapes
	 * 
	 * @param rects
	 *            rectangle array
	 * @return true if collision
	 */
	public boolean colides(Rectangle[] rects) {
		for (Rectangle rect : rects) {
			if (colides(rect)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Subtracts an amount of damage from ships health
	 * 
	 * @param damage
	 */
	public void hit(int damage) {
		health -= damage;
		if (health <= 0) {
			health = 0;
			setState(ShipState.DEAD);
		}
	}

	/**
	 * 
	 * @param health
	 */
	public void setHealth(int health) {
		this.health = health;
	}

	/**
	 * 
	 * @param xVel
	 */
	public void setVelocityX(float xVel) {
		velocity.x = xVel;
	}

	/**
	 * 
	 * @param yVel
	 */
	public void setVelocityY(float yVel) {
		velocity.y = yVel;
	}

	/**
	 * 
	 * @param xPos
	 */
	public void setPositionX(float xPos) {
		position.x = xPos;
	}

	public void setPositionY(float yPos) {
		position.y = yPos;
	}

	/**
	 * 
	 * @param state
	 */
	public void setState(ShipState state) {
		this.state = state;
		stateTime = 0;
	}

	/**
	 * 
	 * @return x position
	 */
	public float getX() {
		return position.x;
	}

	/**
	 * 
	 * @return y position
	 */
	public float getY() {
		return position.y;
	}

	/**
	 * 
	 * @return width
	 */
	public float getWidth() {
		return bodySprite.getWidth();
	}

	/**
	 * 
	 * @return height
	 */
	public float getHeight() {
		return bodySprite.getHeight();
	}

	/**
	 * 
	 * @return bounding box
	 */
	public Rectangle[] getBoundingBox() {
		return boundingBox;
	}

	/**
	 * 
	 * @return ship-state
	 */
	public ShipState getState() {
		return state;
	}

	/**
	 * 
	 * @return health
	 */
	public int getHealth() {
		return health;
	}
}
