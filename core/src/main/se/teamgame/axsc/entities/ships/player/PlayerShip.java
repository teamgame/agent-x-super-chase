package se.teamgame.axsc.entities.ships.player;

import java.util.Random;

import org.apache.log4j.Logger;

import se.teamgame.axsc.AgentXSuperChase;
import se.teamgame.axsc.entities.ships.Ship;
import se.teamgame.axsc.levels.Level;
import se.teamgame.axsc.pools.BulletPool;
import se.teamgame.axsc.pools.BulletPool.BulletType;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * PlayerShip class that is representing the player
 * 
 * @author marcus
 *
 */
public class PlayerShip extends Ship {

	static Logger log = Logger.getLogger(PlayerShip.class);

	public final float SPEED = 500;

	private final float FIRE_RATE = 77;
	private long lastShot = 0;

	private BulletPool bulletPool;

	private Rectangle box1;
	private Rectangle box2;
	private Rectangle box3;
	private Rectangle box4;

	private Sprite thrustSprite;

	private Sound[] shootSounds;
	private Sound ricochet;
	private Sound portalSound;
	private Sound explosionSound;
	private boolean portalPlaying;
	private boolean explosionPlaying;

	Random random;

	private boolean firing;

	/**
	 * Constructor
	 * 
	 * @param x
	 *            pos
	 * @param y
	 *            pos
	 * @param level
	 *            active level
	 */
	public PlayerShip(float x, float y, Level level) {
		super(x, y, level);
		log.debug("Creating player ship");

		this.bulletPool = level.getBulletPool();
		random = new Random();

		ricochet = manager.ricochetSound;

		state = ShipState.START;
		stateTime = 0;

		firing = false;

	}

	@Override
	protected void setupSprites() {
		log.debug("seting up sprites");
		bodySprite = new Sprite(manager.playerShip);
		thrustSprite = new Sprite(manager.thrustLeft);
	}

	@Override
	protected void setupPortalAnimation() {
		log.debug("seting up portal animation");
		portal = manager.portal;

	}

	@Override
	protected void setupExplosionAnimation() {
		log.debug("seting up explosion animation");
		explosion = manager.explosion;

	}

	@Override
	protected void setupBoundingBox() {
		log.debug("seting up bounding box");
		box1 = new Rectangle(getX() + 3, getY() + 5, 47, 31);
		box2 = new Rectangle(getX() + 50, getY() + 10, 50, 31);
		box3 = new Rectangle(getX() + 100, getY() + 17, 50, 17);
		box4 = new Rectangle(getX() + 150, getY() + 19, 47, 7);

		boundingBox = new Rectangle[] { box1, box2, box3, box4 };

	}

	@Override
	protected void setupSounds() {
		log.debug("seting up sounds");
		shootSounds = new Sound[] { manager.shoot1, manager.shoot2, manager.shoot3 };
		portalSound = manager.portalSound;
		explosionSound = manager.explosionSound1;
		portalPlaying = false;
		explosionPlaying = false;

	}

	@Override
	public void render(SpriteBatch batch) {
		if (state == ShipState.PORTAL) {
			batch.draw(portal.getKeyFrame(stateTime), position.x - 160, position.y - 160);
		} else if (state == ShipState.FLYING) {
			bodySprite.draw(batch);
			thrustSprite.draw(batch);
		} else if (state == ShipState.DEAD && !explosion.isAnimationFinished(stateTime)) {
			batch.draw(explosion.getKeyFrame(stateTime), position.x, position.y - 70);
		}
	}

	@Override
	public void debugRender(ShapeRenderer renderer) {

		if (state == ShipState.DEAD) {
			renderer.setColor(1, 0, 0, .4f);
		} else {
			renderer.setColor(0, 1, 0, .4f);
		}

		renderer.rect(box1.x, box1.y, box1.width, box1.height);
		renderer.rect(box2.x, box2.y, box2.width, box2.height);
		renderer.rect(box3.x, box3.y, box3.width, box3.height);
		renderer.rect(box4.x, box4.y, box4.width, box4.height);

	}

	@Override
	public void update(float delta) {
		stateTime += delta;

		if (state == ShipState.PORTAL) {
			if (!portalPlaying) {
				portalSound.play();
				portalPlaying = true;
			}
			if (portal.isAnimationFinished(stateTime)) {
				setState(ShipState.FLYING);
			}
		} else if (state == ShipState.FLYING) {
			explosionPlaying = false;

			position.add(velocity.cpy().scl(delta));

			float directionModifier = velocity.x < 0 ? -.2f : 0;
			directionModifier = velocity.x > 0 ? .2f : directionModifier;
			thrustSprite.setAlpha((float) (.4 + (Math.sin(stateTime * 150) * .1))
					+ ((getX() / AgentXSuperChase.WIDTH) * .2f) + directionModifier);

			if (firing) {
				fire();
			}
		} else if (state == ShipState.DEAD) {
			if (!explosionPlaying) {
				explosionSound.play(0.5f);
				explosionPlaying = true;
			}
			position.add(new Vector2(0, -100).scl(delta));
		}

		box1.setPosition(position.x + 3, position.y + 7);
		box2.setPosition(position.x + 50, position.y + 10);
		box3.setPosition(position.x + 100, position.y + 17);
		box4.setPosition(position.x + 150, position.y + 19);

		checkBounds();

		bodySprite.setPosition(position.x, position.y);
		thrustSprite.setPosition(position.x - thrustSprite.getWidth() - 1, position.y + 16);

	}

	@Override
	public void hit(int damage) {
		ricochet.play(.3f);
		super.hit(damage);
	}

	/**
	 * Keeps the player on the screen
	 */
	private void checkBounds() {
		if (position.x > AgentXSuperChase.WIDTH - getWidth()) {
			position.x = AgentXSuperChase.WIDTH - getWidth();
		}
		if (position.x < 0) {
			position.x = 0;
		}
		if (position.y > AgentXSuperChase.HEIGHT - getHeight()) {
			position.y = AgentXSuperChase.HEIGHT - getHeight();
		}
		if (position.y < 0) {
			position.y = 0;
		}
	}

	/**
	 * Adds a bullet to the level at the rate specified by FIRE_RATE
	 */
	public void fire() {
		long time = System.currentTimeMillis();
		if (state != ShipState.DEAD && time - lastShot > FIRE_RATE && firing) {
			lastShot = time;
			level.getBullets().add(
					bulletPool.getBullet(position.x + getWidth() + 2, position.y + 17, Math.PI / 2, SPEED * 4,
							BulletType.PLAYER_BULLET));
			shootSounds[random.nextInt(3)].play(.4f);
		}
	}

	/**
	 * Sets the firing flag
	 * 
	 * @param firing
	 */
	public void setFiring(boolean firing) {
		this.firing = firing;
	}

	/**
	 * 
	 * @return true if firing
	 */
	public boolean isFiring() {
		return firing;
	}

}
