package se.teamgame.axsc.entities.ships.mainenemy;

import java.util.Random;

import org.apache.log4j.Logger;

import se.teamgame.axsc.entities.ships.Ship;
import se.teamgame.axsc.levels.Level;
import se.teamgame.axsc.pools.BulletPool.BulletType;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * MainEnemyShip class that is representing the main enemy
 * 
 * @author marcus
 *
 */
public class MainEnemyShip extends Ship {

	static Logger log = Logger.getLogger(MainEnemyShip.class);

	private Rectangle box1;
	private Rectangle box2;
	private Rectangle box3;
	private Rectangle box4;

	private Sprite thrustSprite;

	private Sound[] shootSounds;
	private Sound portalSound;
	private Sound explosionSound;
	private boolean portalPlaying;
	private boolean explosionPlaying;

	Random random;

	/**
	 * Constructor
	 * 
	 * @param x
	 *            pos
	 * @param y
	 *            pos
	 * @param level
	 *            active level
	 */
	public MainEnemyShip(float x, float y, Level level) {
		super(x, y, level);
		log.debug("Creating main enemy");

		random = new Random();

		state = ShipState.START;
		stateTime = 0;

	}

	@Override
	protected void setupSprites() {
		log.debug("seting up sprites");
		bodySprite = new Sprite(manager.mainEnemy);
		thrustSprite = new Sprite(manager.thrustLeft);

	}

	@Override
	protected void setupPortalAnimation() {
		log.debug("seting up portal animation");
		portal = manager.portal;

	}

	@Override
	protected void setupExplosionAnimation() {
		log.debug("seting up explosion animation");
		explosion = manager.explosion;

	}

	@Override
	protected void setupBoundingBox() {
		log.debug("seting up bounding box");
		box1 = new Rectangle(getX() + 3, getY() + 9, 18, 33);
		box2 = new Rectangle(getX() + 18, getY() + 3, 72, 39);
		box3 = new Rectangle(getX() + 90, getY() + 11, 55, 27);
		box4 = new Rectangle(getX() + 145, getY() + 11, 52, 17);

		boundingBox = new Rectangle[] { box1, box2, box3, box4 };

	}

	@Override
	protected void setupSounds() {
		log.debug("seting up sounds");
		shootSounds = new Sound[] { manager.shoot1, manager.shoot2, manager.shoot3 };
		portalSound = manager.portalSound;
		explosionSound = manager.explosionSound1;
		portalPlaying = false;
		explosionPlaying = false;
	}

	@Override
	public void render(SpriteBatch batch) {
		if (state == ShipState.PORTAL) {
			batch.draw(portal.getKeyFrame(stateTime), position.x - 160, position.y - 160);
		} else if (state == ShipState.FLYING) {
			bodySprite.draw(batch);
			thrustSprite.draw(batch);
		} else if (state == ShipState.DEAD && !explosion.isAnimationFinished(stateTime)) {
			batch.draw(explosion.getKeyFrame(stateTime), position.x, position.y - 70);
		}
	}

	@Override
	public void debugRender(ShapeRenderer renderer) {

		if (state == ShipState.DEAD) {
			renderer.setColor(1, 0, 0, .4f);
		} else {
			renderer.setColor(1, 1, 0, .4f);
		}

		renderer.rect(box1.x, box1.y, box1.width, box1.height);
		renderer.rect(box2.x, box2.y, box2.width, box2.height);
		renderer.rect(box3.x, box3.y, box3.width, box3.height);
		renderer.rect(box4.x, box4.y, box4.width, box4.height);
	}

	@Override
	public void update(float delta) {
		stateTime += delta;

		if (state == ShipState.PORTAL) {
			if (!portalPlaying) {
				portalSound.play();
				portalPlaying = true;
			}
			if (portal.isAnimationFinished(stateTime)) {
				setVelocityX(1500);
				setState(ShipState.FLYING);
			}
		} else if (state == ShipState.FLYING) {
			thrustSprite.setAlpha((float) (.7 + (Math.sin(stateTime * 150) * .1)));
			position.add(velocity.cpy().scl(delta));
		} else if (state == ShipState.DEAD) {
			if (!explosionPlaying) {
				explosionSound.play(0.5f);
				explosionPlaying = true;
			}
			position.add(new Vector2(0, -100).scl(delta));
		}

		box1.setPosition(position.x + 3, position.y + 9);
		box2.setPosition(position.x + 18, position.y + 3);
		box3.setPosition(position.x + 90, position.y + 11);
		box4.setPosition(position.x + 145, position.y + 11);
		bodySprite.setPosition(position.x, position.y);
		thrustSprite.setPosition(position.x - thrustSprite.getWidth(), position.y + 13);

		if (getX() < 0) {
			position.x = 0;
		}
	}

	/**
	 * Adds a mine to the level
	 * 
	 * @param velocity
	 */
	public void dropMine(Float velocity) {

		level.getBullets().add(bulletPool.getBullet(getX(), getY(), Math.PI * 1.5, velocity, BulletType.MINE));

	}

	/**
	 * Adds a bullet to the level
	 * 
	 * @param x
	 *            pos
	 * @param y
	 *            pos
	 * @param direction
	 */
	public void fire(float x, float y, double direction) {

		level.getBullets().add(bulletPool.getBullet(x, y, direction, 600, BulletType.ENEMY_BULLET));
		shootSounds[random.nextInt(3)].play(.2f);
	}

}
