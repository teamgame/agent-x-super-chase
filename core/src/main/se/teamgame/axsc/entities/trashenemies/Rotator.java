package se.teamgame.axsc.entities.trashenemies;

import java.util.Random;

import org.apache.log4j.Logger;

import se.teamgame.axsc.AgentXSuperChase;
import se.teamgame.axsc.levels.Level;
import se.teamgame.axsc.pools.BulletPool.BulletType;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Rotator class that is representing an enemy of ratotor type in game
 * 
 * @author marcus
 *
 */
public class Rotator extends TrashEnemy {

	static Logger log = Logger.getLogger(Rotator.class);

	private float burstTimer;
	private float bulletTimer;
	private int bulletCount;

	private Random random;

	private Sprite turretSprite;
	private Sprite thrustSprite;
	private Rectangle box;

	private Sound[] shootSounds;
	private Sound[] explosionSounds;
	private boolean explosionPlayed;

	/**
	 * Constructor
	 * 
	 * @param x
	 *            pos
	 * @param y
	 *            pos
	 * @param speed
	 * @param level
	 *            active level
	 */
	public Rotator(float x, float y, float speed, Level level) {
		super(x, y, level);

		log.debug("Creating rotator");

		random = new Random();

		shootSounds = new Sound[] { manager.rotatorShoot1, manager.rotatorShoot2, manager.rotatorShoot3 };
		explosionSounds = new Sound[] { manager.explosionSound1, manager.explosionSound2, manager.explosionSound3 };
		explosionPlayed = false;

		burstTimer = 0;
		bulletTimer = 0;
		bulletCount = 0;

		setVelocityX(-speed);
	}

	@Override
	protected void setupSprites() {
		log.debug("seting up sprites");
		bodySprite = new Sprite(manager.rotatorBody);
		turretSprite = new Sprite(manager.rotatorTurret);
		thrustSprite = new Sprite(manager.rotatorThrust);

	}

	@Override
	protected void setupExplosionAnimation() {
		log.debug("seting up explosion animation");
		explosion = manager.explosion;

	}

	@Override
	protected void setupBoundingBox() {
		log.debug("seting up bounding box");
		box = new Rectangle(getX() + 5, getY() + 5, getWidth() - 10, getHeight() - 10);
		boundingBox = new Rectangle[] { box };

	}

	@Override
	protected void setupScore() {
		score = 3000;

	}

	@Override
	public void render(SpriteBatch batch) {
		if (state == TrashState.ALIVE) {
			bodySprite.draw(batch);
			turretSprite.draw(batch);
			thrustSprite.draw(batch);
		} else if (state == TrashState.DEAD && !explosion.isAnimationFinished(stateTime)) {
			batch.draw(explosion.getKeyFrame(stateTime), position.x - 70, position.y - 60);
		}

	}

	@Override
	public void debugRender(ShapeRenderer renderer) {
		renderer.setColor(1, 1, 0, .4f);
		renderer.rect(box.x, box.y, box.width, box.height);

	}

	@Override
	public void update(float delta) {
		stateTime += delta;
		burstTimer += delta;
		bulletTimer += delta;

		if (state == TrashState.ALIVE) {
			thrustSprite.setAlpha((float) (.7 + (Math.sin(stateTime * 150) * .1)));
			turretSprite.rotate(delta * 400);
			fire();

		} else {
			if (getX() < AgentXSuperChase.WIDTH && !explosionPlayed) {
				explosionSounds[random.nextInt(3)].play(.2f);
				explosionPlayed = true;
			}
			position.add(new Vector2(0, -100).scl(delta));
		}

		position.add(velocity.cpy().scl(delta));
		bodySprite.setPosition(position.x, position.y);
		turretSprite.setPosition(position.x + ((bodySprite.getWidth() - turretSprite.getWidth()) / 2) - 5, position.y
				+ ((bodySprite.getHeight() - turretSprite.getHeight()) / 2));
		thrustSprite.setPosition(position.x - thrustSprite.getWidth(), position.y
				+ ((bodySprite.getHeight() - thrustSprite.getHeight()) / 2));
		box.setPosition(position.x + 5, position.y + 5);

		if (getX() < -getWidth() || getY() < 0) {
			active = false;
		}
	}

	/**
	 * adds bullets to the level in bursts every half second
	 */
	public void fire() {

		if (bulletCount < 7 && bulletTimer > .07f) {
			bulletCount++;
			level.getBullets().add(
					bulletPool.getBullet(getX() + (getWidth() / 2), getY() + (getHeight() / 2),
							(turretSprite.getRotation() / 180 * Math.PI) - Math.PI / 2, 1000, BulletType.ENEMY_BULLET));
			shootSounds[random.nextInt(3)].play(.05f);
			bulletTimer = 0;
		}
		if (burstTimer > 1.1f) {
			burstTimer = 0;
			bulletCount = 0;
		}

	}
}
