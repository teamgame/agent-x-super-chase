package se.teamgame.axsc.entities.trashenemies;

import org.apache.log4j.Logger;

import se.teamgame.axsc.AgentXSuperChase;
import se.teamgame.axsc.levels.Level;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;

/**
 * BigOne class that is representing a big laser in the game
 * 
 * @author marcus
 *
 */
public class BigOne extends TrashEnemy {

	static Logger log = Logger.getLogger(BigOne.class);

	private Sprite thrust1;
	private Sprite thrust2;
	private Sprite thrust3;
	private Rectangle box1;
	private Rectangle box2;
	private Rectangle box3;
	private Rectangle box4;
	private Rectangle laserBox;
	private TextureRegion laser;
	private Sound alertSound;
	private Sound laserSound;
	private boolean firing;
	private boolean alertPlaying;
	private boolean laserPlaying;

	/**
	 * Constructor
	 * 
	 * @param x
	 *            pos
	 * @param y
	 *            pos
	 * @param level
	 *            active level
	 */
	public BigOne(float x, float y, Level level) {
		super(x, y, level);
		log.debug("Creating BigOne");

		setVelocityX(50);
		laser = manager.bigOneLaser;
		alertSound = manager.alertSound;
		laserSound = manager.laserSound;
		laserBox = new Rectangle(getX() + 100, getY() + ((getHeight() - 6) / 2), AgentXSuperChase.WIDTH, 6);
		firing = false;
		alertPlaying = false;
		laserPlaying = false;
	}

	@Override
	protected void setupSprites() {
		log.debug("seting up sprites");
		bodySprite = new Sprite(manager.bigOne);
		thrust1 = new Sprite(manager.thrustLeft);
		thrust2 = new Sprite(manager.thrustLeft);
		thrust3 = new Sprite(manager.thrustLeft);

	}

	@Override
	protected void setupExplosionAnimation() {
		log.debug("seting up explosion animation");
		explosion = manager.explosion;

	}

	@Override
	protected void setupBoundingBox() {
		log.debug("seting up bounding box");
		box1 = new Rectangle(getX() + 5, getY() + 35, 7, 119);
		box2 = new Rectangle(getX() + 12, getY() + 10, 132, 169);
		box3 = new Rectangle(getX() + 144, getY() + 47, 118, 95);
		box4 = new Rectangle(getX() + 262, getY() + 75, 12, 39);
		boundingBox = new Rectangle[] { box1, box2, box3, box4 };

	}

	@Override
	protected void setupScore() {
		score = 20000;

	}

	@Override
	public void render(SpriteBatch batch) {
		if (firing) {
			batch.draw(laser, getX() + 100, getY() + ((getHeight() - laser.getRegionHeight()) / 2),
					AgentXSuperChase.WIDTH, laser.getRegionHeight());
		}
		thrust1.draw(batch);
		thrust2.draw(batch);
		thrust3.draw(batch);
		bodySprite.draw(batch);

	}

	@Override
	public void debugRender(ShapeRenderer renderer) {
		renderer.setColor(1, 1, 0, .4f);
		renderer.rect(box1.x, box1.y, box1.width, box1.height);
		renderer.rect(box2.x, box2.y, box2.width, box2.height);
		renderer.rect(box3.x, box3.y, box3.width, box3.height);
		renderer.rect(box4.x, box4.y, box4.width, box4.height);

		if (firing) {
			renderer.setColor(0, 1, 1, .4f);
			renderer.rect(laserBox.x, laserBox.y, laserBox.width, laserBox.height);
		}

	}

	@Override
	public void update(float delta) {
		stateTime += delta;

		thrust1.setAlpha((float) (.7 + (Math.sin(stateTime * 150) * .1)));
		thrust2.setAlpha((float) (.7 + (Math.cos(stateTime * 150) * .1)));
		thrust3.setAlpha((float) (.7 + (Math.sin(stateTime * 150) * .1)));
		if (stateTime < 7) {
			if (!alertPlaying) {
				long id = alertSound.loop();
				alertSound.setPriority(id, 1000);
				alertPlaying = true;

			}
			setVelocityX(50);
		} else if (stateTime < 11) {
			setVelocityX(0);
			setVelocityY(15);
			firing = true;
			if (alertPlaying) {
				alertSound.stop();
				alertPlaying = false;
			}
			if (!laserPlaying) {
				long id = laserSound.loop(.6f);
				laserSound.setPriority(id, 1000);
				laserPlaying = true;
			}
		} else if (stateTime < 15) {
			setVelocityX(0);
			setVelocityY(-15);
		} else {
			if (laserPlaying) {
				laserSound.stop();
				laserPlaying = false;
			}
			setVelocityX(-50);
			setVelocityY(0);
			firing = false;
		}

		position.add(velocity.cpy().scl(delta));
		bodySprite.setPosition(position.x, position.y);

		thrust1.setPosition(position.x - thrust1.getWidth(), position.y + 45);
		thrust2.setPosition(position.x - thrust1.getWidth(), position.y + getHeight() - 45 - thrust2.getHeight());
		thrust3.setPosition(position.x - thrust1.getWidth(), position.y + ((getHeight() - thrust3.getHeight()) / 2));

		box1.setPosition(position.x + 5, position.y + 35);
		box2.setPosition(position.x + 12, position.y + 10);
		box3.setPosition(position.x + 144, position.y + 47);
		box4.setPosition(position.x + 262, position.y + 75);

		laserBox.setPosition(getX() + 100, getY() + ((getHeight() - 6) / 2));

		if (getX() < -getWidth() - 300) {
			active = false;
		}

	}

	/**
	 * silence all sounds of the laser
	 */
	public void silence() {
		alertSound.stop();
		laserSound.stop();

	}

	/**
	 * 
	 * @return true if it is firing
	 */
	public boolean isFiring() {
		return firing;
	}

	/**
	 * 
	 * @return laser bounding box
	 */
	public Rectangle getLaser() {
		return laserBox;
	}

}
