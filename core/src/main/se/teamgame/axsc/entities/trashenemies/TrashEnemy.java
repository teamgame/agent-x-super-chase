package se.teamgame.axsc.entities.trashenemies;

import se.teamgame.axsc.levels.Level;
import se.teamgame.axsc.managers.AssetManager;
import se.teamgame.axsc.pools.BulletPool;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Abstract TrashEnemy class that all trash enemies inherit from
 * 
 * @author marcus
 *
 */
public abstract class TrashEnemy {

	public enum TrashState {
		ALIVE, DEAD
	}

	protected TrashState state;
	protected float stateTime;

	protected boolean active;

	protected AssetManager manager;
	protected Level level;
	protected BulletPool bulletPool;

	protected Vector2 position;
	protected Vector2 velocity;

	protected int score;

	protected Sprite bodySprite;
	protected Animation explosion;
	protected Rectangle[] boundingBox;

	/**
	 * Constructor
	 * 
	 * @param x
	 *            pos
	 * @param y
	 *            pos
	 * @param level
	 *            active level
	 */
	public TrashEnemy(float x, float y, Level level) {
		position = new Vector2(x, y);
		velocity = new Vector2();

		manager = AssetManager.getInstance();
		this.level = level;
		bulletPool = level.getBulletPool();

		state = TrashState.ALIVE;
		stateTime = 0;

		active = true;

		setupSprites();
		setupExplosionAnimation();
		setupBoundingBox();
		setupScore();

	}

	/**
	 * Sets up sprites
	 */
	protected abstract void setupSprites();

	/**
	 * Sets up explosion animation
	 */
	protected abstract void setupExplosionAnimation();

	/**
	 * Sets up boundig box
	 */
	protected abstract void setupBoundingBox();

	/**
	 * Sets up score
	 */
	protected abstract void setupScore();

	/**
	 * Renders the trash enemy
	 * 
	 * @param batch
	 *            SpriteBatch to draw on
	 */
	public abstract void render(SpriteBatch batch);

	/**
	 * renders debug information
	 * 
	 * @param renderer
	 *            ShapeRenderer used to render
	 */
	public abstract void debugRender(ShapeRenderer renderer);

	/**
	 * Updates the trash enemy
	 * 
	 * @param delta
	 *            time since last time the method executed
	 */
	public abstract void update(float delta);

	/**
	 * Checks for collision with a rectangle shape
	 * 
	 * @param rect
	 *            rectangle
	 * @return true if collision
	 */
	public boolean colides(Rectangle rect) {
		for (Rectangle box : boundingBox) {
			if (box.overlaps(rect)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks for collision with a circle shape
	 * 
	 * @param circle
	 * @return true if collision
	 */
	public boolean colides(Circle circle) {
		for (Rectangle box : boundingBox) {
			if (Intersector.overlaps(circle, box)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks for collision with an array of rectangle shapes
	 * 
	 * @param rects
	 *            rectangle array
	 * @return true if collision
	 */
	public boolean colides(Rectangle[] rects) {
		for (Rectangle rect : rects) {
			if (colides(rect)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @param xVel
	 */
	public void setVelocityX(float xVel) {
		velocity.x = xVel;
	}

	/**
	 * 
	 * @param yVel
	 */
	public void setVelocityY(float yVel) {
		velocity.y = yVel;
	}

	/**
	 * 
	 * @param yPos
	 */
	public void setPositionY(float yPos) {
		position.y = yPos;
	}

	/**
	 * 
	 * @param state
	 *            TrashState
	 */
	public void setState(TrashState state) {
		this.state = state;
		stateTime = 0;
	}

	/**
	 * 
	 * @return x position
	 */
	public float getX() {
		return position.x;
	}

	/**
	 * 
	 * @return y position
	 */
	public float getY() {
		return position.y;
	}

	/**
	 * 
	 * @return width
	 */
	public float getWidth() {
		return bodySprite.getWidth();
	}

	/**
	 * 
	 * @return height
	 */
	public float getHeight() {
		return bodySprite.getHeight();
	}

	/**
	 * 
	 * @return bounding box
	 */
	public Rectangle[] getBoundingBox() {
		return boundingBox;
	}

	/**
	 * 
	 * @return true if trash enemy is active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * 
	 * @return TrashState
	 */
	public TrashState getState() {
		return state;
	}

	/**
	 * 
	 * @return score
	 */
	public int getScore() {
		return score;
	}

}
