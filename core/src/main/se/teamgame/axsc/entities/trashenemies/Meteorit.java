package se.teamgame.axsc.entities.trashenemies;

import java.util.Random;

import org.apache.log4j.Logger;

import se.teamgame.axsc.AgentXSuperChase;
import se.teamgame.axsc.levels.Level;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;

/**
 * meteorit class representin a meteorit in game
 * 
 * @author marcus
 *
 */
public class Meteorit extends TrashEnemy {

	static Logger log = Logger.getLogger(Meteorit.class);

	private Rectangle box;

	private Random random;

	private Sound[] explosionSounds;
	private boolean explosionPlayed;

	/**
	 * Constructor
	 * 
	 * @param x
	 *            pos
	 * @param y
	 *            pos
	 * @param level
	 *            active level
	 */
	public Meteorit(float x, float y, Level level) {
		super(x, y, level);
		log.debug("Creating meteorit");

		explosionSounds = new Sound[] { manager.explosionSound1, manager.explosionSound2, manager.explosionSound3 };

		random = new Random();

		setVelocityX(-1200);
		setVelocityY(-800);
	}

	@Override
	protected void setupSprites() {
		log.debug("seting up sprites");
		bodySprite = new Sprite(manager.meteorit);

	}

	@Override
	protected void setupExplosionAnimation() {
		log.debug("seting up explosion animation");
		explosion = manager.explosion;

	}

	@Override
	protected void setupBoundingBox() {
		log.debug("seting up bounding box");
		box = new Rectangle(getX() + 5, getY() + 5, getWidth() - 10, getHeight() - 10);
		boundingBox = new Rectangle[] { box };
	}

	@Override
	protected void setupScore() {
		score = 500;
	}

	@Override
	public void render(SpriteBatch batch) {
		if (state == TrashState.ALIVE) {
			bodySprite.draw(batch);

		} else if (state == TrashState.DEAD && !explosion.isAnimationFinished(stateTime)) {
			batch.draw(explosion.getKeyFrame(stateTime), position.x - 70, position.y - 90);
		}

	}

	@Override
	public void debugRender(ShapeRenderer renderer) {
		renderer.setColor(1, 1, 0, .4f);
		renderer.rect(box.x, box.y, box.width, box.height);

	}

	@Override
	public void update(float delta) {
		stateTime += delta;
		if (state == TrashState.ALIVE) {
			bodySprite.rotate(-delta * 800);
		} else {
			if (getX() < AgentXSuperChase.WIDTH && !explosionPlayed) {
				explosionSounds[random.nextInt(3)].play(.2f);
				explosionPlayed = true;
			}
			setVelocityY(-100);
			setVelocityX(level.getScrollSpeed());
		}
		position.add(velocity.cpy().scl(delta));
		bodySprite.setPosition(position.x, position.y);
		box.setPosition(position.x + 5, position.y + 5);

		if (getX() < -getWidth() || getY() < 0) {
			active = false;
		}
	}

}
