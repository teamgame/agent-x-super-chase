package se.teamgame.axsc.entities.trashenemies;

import java.util.Random;

import org.apache.log4j.Logger;

import se.teamgame.axsc.AgentXSuperChase;
import se.teamgame.axsc.levels.Level;
import se.teamgame.axsc.pools.BulletPool.BulletType;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * SentryDrone class that is representing a sentry-drone in game
 * 
 * @author marcus
 *
 */
public class SentryDrone extends TrashEnemy {

	static Logger log = Logger.getLogger(SentryDrone.class);

	private Rectangle box;

	private float bulletTimer = 0;
	private Random random;

	private Sprite thrustSprite;

	private Sound[] shootSounds;
	private Sound[] explosionSounds;
	private boolean explosionPlayed;

	private float spawnY;

	/**
	 * Constructor
	 * 
	 * @param x
	 *            pos
	 * @param y
	 *            pos
	 * @param speed
	 * @param level
	 *            active level
	 */
	public SentryDrone(float x, float y, float speed, Level level) {
		super(x, y, level);

		log.debug("Creating sentry-drone");
		random = new Random();

		shootSounds = new Sound[] { manager.sentryShoot1, manager.sentryShoot2, manager.sentryShoot3 };
		explosionSounds = new Sound[] { manager.explosionSound1, manager.explosionSound2, manager.explosionSound3 };
		explosionPlayed = false;

		spawnY = y;

		setVelocityX(-speed);

	}

	@Override
	protected void setupSprites() {
		log.debug("seting up sprites");
		bodySprite = new Sprite(manager.sentryDrone);
		thrustSprite = new Sprite(manager.sentryDroneThrust);

	}

	@Override
	protected void setupExplosionAnimation() {
		log.debug("seting up explosion animation");
		explosion = manager.explosion;

	}

	@Override
	protected void setupBoundingBox() {
		log.debug("seting up bounding box");
		box = new Rectangle(getX() + 5, getY() + 5, getWidth() - 10, getHeight() - 10);
		boundingBox = new Rectangle[] { box };

	}

	@Override
	protected void setupScore() {
		score = 1000;

	}

	@Override
	public void render(SpriteBatch batch) {

		if (state == TrashState.ALIVE) {
			bodySprite.draw(batch);
			thrustSprite.draw(batch);
		} else if (state == TrashState.DEAD && !explosion.isAnimationFinished(stateTime)) {
			batch.draw(explosion.getKeyFrame(stateTime), position.x, position.y - 70);
		}

	}

	@Override
	public void debugRender(ShapeRenderer renderer) {
		renderer.setColor(1, 1, 0, .4f);
		renderer.rect(box.x, box.y, box.width, box.height);

	}

	@Override
	public void update(float delta) {

		stateTime += delta;
		bulletTimer += delta;

		if (state == TrashState.ALIVE) {
			thrustSprite.setAlpha((float) (.7 + (Math.sin(stateTime * 150) * .1)));

			position.add(velocity.cpy().scl(delta));
			setPositionY((float) ((Math.sin(stateTime) * 50) + spawnY));

			fire();

		} else {
			if (getX() < AgentXSuperChase.WIDTH && !explosionPlayed) {
				explosionSounds[random.nextInt(3)].play(.2f);
				explosionPlayed = true;
			}
			position.add(new Vector2(0, -100).scl(delta));
		}

		box.setPosition(getX() + 5, getY() + 5);
		bodySprite.setPosition(position.x, position.y);
		thrustSprite.setPosition(getX() + ((getWidth() - thrustSprite.getWidth()) / 2),
				getY() - thrustSprite.getHeight() + 5);

		if (getX() < -getWidth() || getY() < 0) {
			active = false;
		}
	}

	/**
	 * Adds a bullet to the level every second
	 */
	public void fire() {
		if (bulletTimer > 1f) {
			level.getBullets().add(
					bulletPool.getBullet(getX(), getY() + getHeight() - 10, Math.PI * 1.5, 1000,
							BulletType.ENEMY_BULLET));
			shootSounds[random.nextInt(3)].play(.05f);
			bulletTimer = 0;
		}
	}
}
