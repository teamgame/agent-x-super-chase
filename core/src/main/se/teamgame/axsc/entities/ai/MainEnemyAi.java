package se.teamgame.axsc.entities.ai;

import org.apache.log4j.Logger;

import se.teamgame.axsc.AgentXSuperChase;
import se.teamgame.axsc.entities.buildings.Building;
import se.teamgame.axsc.entities.ships.Ship.ShipState;
import se.teamgame.axsc.entities.ships.mainenemy.MainEnemyShip;
import se.teamgame.axsc.entities.ships.player.PlayerShip;
import se.teamgame.axsc.levels.Level;
import se.teamgame.axsc.pools.BulletPool;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;

/**
 * AI class for main enemy
 * 
 * @author marcus
 *
 */
public class MainEnemyAi {

	static Logger log = Logger.getLogger(MainEnemyAi.class);

	private boolean onScreen;

	private MainEnemyShip mainEnemy;
	private PlayerShip playerShip;
	private Level level;

	private boolean playerFire = false;
	private float playerFireTimer = 0;

	private boolean hitRoof;

	private Rectangle frontSensor;
	private Rectangle bottomSensor;

	private float mineTimer = 0;
	private float bulletTimer = 0;
	private int bulletCount = 0;

	/**
	 * Constructor
	 * 
	 * @param level
	 *            active level
	 * @param bulletpool
	 *            an instance of BulletPool
	 */
	public MainEnemyAi(Level level, BulletPool bulletpool) {

		log.debug("Creating main-enemy AI");

		this.level = level;
		this.mainEnemy = level.getMainEnemy();
		this.playerShip = level.getPlayerShip();
		this.hitRoof = false;

		this.frontSensor = new Rectangle(mainEnemy.getX(), mainEnemy.getY() - 80, 600, 2);
		this.bottomSensor = new Rectangle(mainEnemy.getX(), mainEnemy.getY() - 200, 200, 200);

	}

	/**
	 * Updates the Ai
	 * 
	 * @param delta
	 *            time since last time the method was executed
	 */
	public void update(float delta) {

		mineTimer += delta;
		bulletTimer += delta;
		playerFireTimer += delta;

		frontSensor.setPosition(mainEnemy.getX(), mainEnemy.getY() - 80);
		bottomSensor.setPosition(mainEnemy.getX(), mainEnemy.getY() - 200);

		checkIfOnScreen();

		if (onScreen) {

			checkPlayerFire();

			if (playerFire && mainEnemy.getY() < playerShip.getY() + 100 && mainEnemy.getY() > playerShip.getY() - 100) {

				dodgeBullets();

			} else {

				followLevel();

				if (level.getDistanceTravelled() > 2500) {

					if (mineTimer > .7f && mainEnemy.getState() != ShipState.DEAD
							&& playerShip.getState() != ShipState.DEAD) {
						dropMine();
						mineTimer = 0;
					}

					if (bulletCount < 3) {
						if (bulletTimer > .2f && mainEnemy.getState() != ShipState.DEAD
								&& playerShip.getState() != ShipState.DEAD) {
							fire();
						}
					} else if (bulletTimer > .7f) {
						bulletCount = 0;
					}
				}
			}

		} else {
			mainEnemy.setVelocityY(0);
			mainEnemy.setPositionY(800);
		}

	}

	/**
	 * Renders debug information
	 * 
	 * @param renderer
	 *            a ShapeRenderer
	 */
	public void debugRender(ShapeRenderer renderer) {

		if (onScreen) {
			renderer.setColor(1, 0, 1, .4f);
			renderer.rect(frontSensor.x, frontSensor.y, frontSensor.width, frontSensor.height);
			renderer.rect(bottomSensor.x, bottomSensor.y, bottomSensor.width, bottomSensor.height);
		}
	}

	/**
	 * Sets playerFire boolean to true if player is firing and it keeps it true
	 * 0.3 seconds after the player stops firing
	 */
	private void checkPlayerFire() {
		if (playerShip.isFiring()) {
			playerFire = true;
			playerFireTimer = 0;

		} else if (!playerShip.isFiring() && playerFireTimer > .3) {
			playerFire = false;
		}
	}

	/**
	 * Used for dodging player bullets, the enemy will try move upwards until it
	 * hits the roof then he will start move downward until his y position is
	 * 750
	 */
	private void dodgeBullets() {
		if (mainEnemy.getY() < AgentXSuperChase.HEIGHT - mainEnemy.getHeight() && !hitRoof) {
			mainEnemy.setVelocityY(500);
			if (mainEnemy.getY() > AgentXSuperChase.HEIGHT - mainEnemy.getHeight() - 5) {
				hitRoof = true;
			}

		} else {

			mainEnemy.setVelocityY(-500);

			if (mainEnemy.getY() < 750) {
				hitRoof = false;
			}
		}
	}

	/**
	 * Used to detect buildings and follow their height.
	 */
	private void followLevel() {
		boolean frontSensorHit = false;
		boolean bottomSensorHit = false;

		for (Building building : level.getBuildings()) {
			if (frontSensor.overlaps(building.getBoundingBox())) {
				frontSensorHit = true;
			}
			if (bottomSensor.overlaps(building.getBoundingBox())) {
				bottomSensorHit = true;
			}
		}

		if (frontSensorHit || mainEnemy.getY() < 500) {
			mainEnemy.setVelocityY(500);
		} else if (bottomSensorHit) {
			mainEnemy.setVelocityY(0);
		} else {
			mainEnemy.setVelocityY(-200);
		}
	}

	/**
	 * Makes the enemy drop a mine
	 */
	private void dropMine() {

		mainEnemy.dropMine(-(level.BASE_SPEED - playerShip.getX() * 1.5f) / 2);

	}

	/**
	 * Makes the enemy fire at the player, calculates direction and where to
	 * spawn the bullet
	 */
	private void fire() {

		float deltaX = mainEnemy.getX() - playerShip.getX();
		float deltaY = mainEnemy.getY() - playerShip.getY();

		double direction = Math.atan2(deltaY, deltaX) - (Math.PI / 2);

		float xSpawn = (float) (mainEnemy.getX() + (mainEnemy.getWidth() / 2) + (Math.sin(direction) * 110));

		float ySpawn = (float) (mainEnemy.getY() + (mainEnemy.getHeight() / 2) + (-Math.cos(direction) * 55));

		mainEnemy.fire(xSpawn, ySpawn, direction);

		bulletTimer = 0;
		bulletCount++;

	}

	/**
	 * Checks if the enemy is on the screen
	 */
	private void checkIfOnScreen() {
		if (mainEnemy.getX() < 0 - mainEnemy.getWidth() || mainEnemy.getX() > AgentXSuperChase.WIDTH) {
			onScreen = false;
		} else {
			onScreen = true;
		}
	}

}
