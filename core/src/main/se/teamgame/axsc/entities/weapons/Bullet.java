package se.teamgame.axsc.entities.weapons;

import se.teamgame.axsc.AgentXSuperChase;
import se.teamgame.axsc.pools.BulletPool.BulletType;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;

/**
 * Bullet class that represents a bullet in game
 * 
 * @author marcus
 *
 */
public class Bullet {

	private Vector2 position;
	private Vector2 velocity;
	private double direction;
	private Sprite bodySprite;
	private BulletType type;

	private Circle boundingBox;

	private boolean active = true;

	/**
	 * Constructor
	 * 
	 * @param x
	 *            pos
	 * @param y
	 *            pos
	 * @param direction
	 *            in radians
	 * @param velocity
	 * @param texture
	 *            textureRegion
	 * @param type
	 *            BulletType
	 */
	public Bullet(float x, float y, double direction, float velocity, TextureRegion texture, BulletType type) {

		this.velocity = new Vector2();
		this.velocity.x = (float) (Math.sin(direction) * velocity);
		this.velocity.y = (float) (-Math.cos(direction) * velocity);
		this.direction = direction;
		this.type = type;

		this.position = new Vector2(x, y);

		bodySprite = new Sprite(texture);

		boundingBox =
				new Circle(x + bodySprite.getWidth() / 2, y + bodySprite.getHeight() / 2, bodySprite.getHeight() / 2);
	}

	/**
	 * Renders the bullet
	 * 
	 * @param batch
	 *            SpriteBatch used to draw the bullet
	 */
	public void render(SpriteBatch batch) {
		bodySprite.draw(batch);
	}

	/**
	 * Renders debug information
	 * 
	 * @param renderer
	 *            ShapeRenderer used to render
	 */
	public void debugRender(ShapeRenderer renderer) {
		renderer.setColor(0, 1, 1, .4f);
		renderer.circle(getBoundingBox().x, getBoundingBox().y, getBoundingBox().radius);
	}

	/**
	 * Updates the bullet
	 * 
	 * @param delta
	 *            time since last time the method was executed
	 */
	public void update(float delta) {
		position.add(velocity.cpy().scl(delta));
		boundingBox.setPosition(position.x + bodySprite.getWidth() / 2, position.y + bodySprite.getHeight() / 2);

		checkBounds();
		bodySprite.setPosition(position.x, position.y);
	}

	/**
	 * Sets the bullet to inactive if it is outside of the screen
	 */
	private void checkBounds() {
		if (position.x > AgentXSuperChase.WIDTH || position.x < 0 || position.y > AgentXSuperChase.HEIGHT
				|| position.y < 0) {
			active = false;
		}
	}

	/**
	 * Resets the bullet
	 * 
	 * @param x
	 *            pos
	 * @param y
	 *            pos
	 * @param direction
	 *            in radians
	 * @param velocity
	 * @param texture
	 *            TextureRegion
	 * @param type
	 *            BulletType
	 */
	public void reset(float x, float y, double direction, float velocity, TextureRegion texture, BulletType type) {

		this.velocity.x = (float) (Math.sin(direction) * velocity);
		this.velocity.y = (float) (-Math.cos(direction) * velocity);
		this.direction = direction;
		this.type = type;

		this.position.x = x;
		this.position.y = y;

		boundingBox.x = x;
		boundingBox.y = y;

		bodySprite = new Sprite(texture);

		boundingBox =
				new Circle(x + bodySprite.getWidth() / 2, y + bodySprite.getHeight() / 2, bodySprite.getHeight() / 2);
		active = true;
	}

	/**
	 * 
	 * @return BulletType
	 */
	public BulletType getType() {
		return type;
	}

	/**
	 * 
	 * @return width
	 */
	public float getWidth() {
		return bodySprite.getWidth();
	}

	/**
	 * 
	 * @return height
	 */
	public float getHeight() {
		return bodySprite.getHeight();
	}

	/**
	 * 
	 * @return bounding box (circle)
	 */
	public Circle getBoundingBox() {
		return boundingBox;
	}

	/**
	 * 
	 * @return true if active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * 
	 * @param active
	 *            active flag
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * 
	 * @param velocity
	 */
	public void setVelocity(float velocity) {
		this.velocity.x = (float) (Math.sin(direction) * velocity);
		this.velocity.y = (float) (-Math.cos(direction) * velocity);
	}

	/**
	 * 
	 * @param direction
	 *            in radians
	 */
	public void setDirection(double direction) {
		this.direction = direction;
	}
}
