package se.teamgame.axsc.entities.background;

import org.apache.log4j.Logger;

import se.teamgame.axsc.managers.AssetManager;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * ParalaxBackground class, allows paralax-effect on background
 * 
 * @author marcus
 *
 */
public class ParalaxBackground {

	static Logger log = Logger.getLogger(ParalaxBackground.class);

	private AssetManager manager;

	private Texture back;
	private ScrollingLayer layer1;
	private ScrollingLayer layer2;
	private ScrollingLayer layer3;

	/**
	 * Cosntructor
	 * 
	 * @param scrollSpeed
	 *            for the background
	 */
	public ParalaxBackground(float scrollSpeed) {

		log.debug("Creating paralax-background");

		manager = AssetManager.getInstance();
		back = manager.skyBackground;
		layer1 = new ScrollingLayer(manager.mntLayerBack, scrollSpeed / 15);
		layer2 = new ScrollingLayer(manager.mntLayerFront, scrollSpeed / 10);
		layer3 = new ScrollingLayer(manager.backgroundSkyline, scrollSpeed / 2);
	}

	/**
	 * Updates the background
	 * 
	 * @param delta
	 *            time since last time the method was executed
	 * @param scrollSpeed
	 *            for the background
	 */
	public void update(float delta, float scrollSpeed) {

		layer1.setScrollSpeed(scrollSpeed / 15);
		layer2.setScrollSpeed(scrollSpeed / 10);
		layer3.setScrollSpeed(scrollSpeed / 2);

		layer1.update(delta);
		layer2.update(delta);
		layer3.update(delta);
	}

	/**
	 * Renders the background
	 * 
	 * @param batch
	 *            the SpriteBatch used to draw on
	 */
	public void render(SpriteBatch batch) {
		batch.draw(back, 0, 0, 1920, 1080);
		layer1.render(batch);
		layer2.render(batch);
		layer3.render(batch);

	}

}
