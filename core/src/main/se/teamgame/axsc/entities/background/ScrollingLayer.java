package se.teamgame.axsc.entities.background;

import org.apache.log4j.Logger;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * ScrollingLayer class, handles continious scrolling of an image
 * 
 * @author marcus
 *
 */
public class ScrollingLayer {

	static Logger log = Logger.getLogger(ScrollingLayer.class);

	private Vector2 position1, position2;
	private Vector2 velocity;

	private Texture texture;

	/**
	 * Constructor
	 * 
	 * @param texture
	 *            used as background
	 * @param scrollSpeed
	 *            for the layer
	 */
	public ScrollingLayer(Texture texture, float scrollSpeed) {

		log.debug("Creating Scrolling-layer");

		this.texture = texture;
		position1 = new Vector2(0, 0);
		position2 = new Vector2(texture.getWidth(), 0);
		velocity = new Vector2(scrollSpeed, 0);
	}

	/**
	 * Renders the layer
	 * 
	 * @param batch
	 *            the SpriteBatch used to draw on
	 */
	public void render(SpriteBatch batch) {

		batch.draw(texture, position1.x, position1.y);
		batch.draw(texture, position2.x, position2.y);
	}

	/**
	 * Updates the layer
	 * 
	 * @param delta
	 *            time since last time the method was executed
	 */
	public void update(float delta) {
		position1.add(velocity.cpy().scl(delta));
		position2.add(velocity.cpy().scl(delta));

		if (position1.x <= -texture.getWidth()) {
			position1.x = position2.x + texture.getWidth();
		}
		if (position2.x <= -texture.getWidth()) {
			position2.x = position1.x + texture.getWidth();
		}

	}

	/**
	 * 
	 * @return layers scroll-speed
	 */
	public float getScrollSpeed() {
		return velocity.x;
	}

	/**
	 * 
	 * @param scrollSpeed
	 */
	public void setScrollSpeed(float scrollSpeed) {
		velocity.x = scrollSpeed;
	}

}
