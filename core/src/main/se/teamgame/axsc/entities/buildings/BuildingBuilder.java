package se.teamgame.axsc.entities.buildings;

import java.util.Random;

import org.apache.log4j.Logger;

import se.teamgame.axsc.managers.AssetManager;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

/**
 * BuildingBuilder class, builds a building based onprovided specifications
 * 
 * @author marcus
 *
 */
public class BuildingBuilder {

	static Logger log = Logger.getLogger(BuildingBuilder.class);

	public enum BuildingType {
		A, B, C
	}

	public enum BuildingHeight {
		LOW, MID, HIGH
	}

	AssetManager manager;
	private Random random;

	private int blocksX;
	private int blocksY;

	private TextureRegion topCenter;
	private TextureRegion topLeft;
	private TextureRegion topRight;
	private TextureRegion leftLit;
	private TextureRegion rightLit;
	private TextureRegion leftDark;
	private TextureRegion rightDark;
	private TextureRegion centerLit;
	private TextureRegion centerDark;

	/**
	 * Constructor
	 */
	public BuildingBuilder() {

		log.debug("Creating building-builder");

		random = new Random();
		manager = AssetManager.getInstance();

	}

	/**
	 * Prepares the builder with the correct assets and dimensions
	 * 
	 * @param type
	 *            of building
	 * @param buildingHeight
	 *            height of building
	 */
	public void prepareBuilder(BuildingType type, BuildingHeight buildingHeight) {

		loadAssets(type);
		calculateDimensions(buildingHeight);
	}

	/**
	 * Loads the correct assets based on building type
	 * 
	 * @param type
	 */
	private void loadAssets(BuildingType type) {
		topCenter = manager.topCenter;

		switch (type) {
			case A:
				topLeft = manager.topLeft1;
				topRight = manager.topRight1;
				leftLit = manager.leftLit1;
				rightLit = manager.rightLit1;
				leftDark = manager.leftDark1;
				rightDark = manager.rightDark1;
				centerLit = manager.centerLit1;
				centerDark = manager.centerDark1;
				break;
			case B:
				topLeft = manager.topLeft2;
				topRight = manager.topRight2;
				leftLit = manager.leftLit2;
				rightLit = manager.rightLit2;
				leftDark = manager.leftDark2;
				rightDark = manager.rightDark2;
				centerLit = manager.centerLit2;
				centerDark = manager.centerDark2;
				break;
			case C:
				topLeft = manager.topLeft3;
				topRight = manager.topRight3;
				leftLit = manager.leftLit3;
				rightLit = manager.rightLit3;
				leftDark = manager.leftDark3;
				rightDark = manager.rightDark3;
				centerLit = manager.centerLit3;
				centerDark = manager.centerDark3;
				break;
			default:
				log.error("Invalid building-type");
				break;
		}
	}

	/**
	 * generates dimensions of the building based on height
	 * 
	 * @param buildingHeight
	 */
	private void calculateDimensions(BuildingHeight buildingHeight) {

		switch (buildingHeight) {
			case LOW:
				blocksX = random.nextInt(12 - 6) + 6;
				blocksY = random.nextInt(20 - 15) + 15;
				break;
			case MID:
				blocksX = random.nextInt(12 - 8) + 8;
				blocksY = random.nextInt(27 - 17) + 17;
				break;
			case HIGH:
				blocksX = random.nextInt(12 - 10) + 10;
				blocksY = random.nextInt(35 - 25) + 25;
				break;
			default:
				log.error("Invalid building-height");
				break;
		}
	}

	/**
	 * Creates an array with the building blocks used for the building based on
	 * its dimensions
	 * 
	 * @param blocksX
	 *            blocks wide
	 * @param blocksY
	 *            blocks tall
	 * @return an array with the building blocks used for the building
	 */
	private Array<TextureRegion> createBuilding(int blocksX, int blocksY) {
		Random rand = new Random();

		Array<TextureRegion> buildingBlocks = new Array<TextureRegion>();

		for (int i = 0; i < blocksX - 2; i++) {
			int roofDecoration = rand.nextInt(15);
			switch (roofDecoration) {
				case 0:
					buildingBlocks.add(manager.roofOrnament1);
					break;
				case 1:
					buildingBlocks.add(manager.roofOrnament2);
					break;
				case 2:
					buildingBlocks.add(manager.roofOrnament3);
					break;
				case 3:
					buildingBlocks.add(manager.roofOrnament4);
					break;
				case 4:
					buildingBlocks.add(manager.roofOrnament5);
					break;
				case 5:
					buildingBlocks.add(manager.roofOrnament6);
					break;
				case 6:
					buildingBlocks.add(manager.roofOrnament7);
					break;
				default:
					buildingBlocks.add(manager.roofOrnament0);
					break;
			}

		}

		for (int y = 0; y < blocksY; y++) {
			for (int x = 0; x < blocksX; x++) {
				if (y == 0) {
					if (x == 0) {
						buildingBlocks.add(topLeft);
					} else if (x == blocksX - 1) {
						buildingBlocks.add(topRight);
					} else {
						buildingBlocks.add(topCenter);
					}
				} else {
					if (x == 0) {
						if (rand.nextBoolean()) {
							buildingBlocks.add(leftLit);
						} else {
							buildingBlocks.add(leftDark);
						}
					} else if (x == blocksX - 1) {
						if (rand.nextBoolean()) {
							buildingBlocks.add(rightLit);
						} else {
							buildingBlocks.add(rightDark);
						}
					} else {
						if (rand.nextBoolean()) {
							buildingBlocks.add(centerLit);
						} else {
							buildingBlocks.add(centerDark);
						}
					}
				}

			}

		}

		return buildingBlocks;
	}

	/**
	 * 
	 * @return a TextureRegion array representing the building
	 */
	public Array<TextureRegion> getBuilding() {
		return createBuilding(blocksX, blocksY);
	}

	/**
	 * 
	 * @return number of blocks wide
	 */
	public int getBlocksX() {
		return blocksX;
	}

	/**
	 * 
	 * @return number of blocks tall
	 */
	public int getBlocksY() {
		return blocksY;
	}
}
