package se.teamgame.axsc.entities.buildings;

import org.apache.log4j.Logger;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

/**
 * Building entity, handles ingame representation of buildings
 * 
 * @author marcus
 *
 */
public class Building {

	static Logger log = Logger.getLogger(Building.class);

	public final static int BLOCK_WIDTH = 16;
	public final static int BLOCK_HEIGHT = 16;

	private Vector2 position;
	private Vector2 velocity;
	private int blocksX;
	private int blocksY;
	private boolean isScrolledLeft;

	private Rectangle boundingBox;

	Array<TextureRegion> buildingBlocks;

	/**
	 * Constructor
	 * 
	 * @param buildingBlocks
	 *            array of TextureRegions used for the building
	 * @param x
	 *            x position
	 * @param y
	 *            y position
	 * @param blocksX
	 *            number of blocks wide
	 * @param blocksY
	 *            number of blocks tall
	 * @param scrollSpeed
	 *            scroll-speed for the building
	 */
	public Building(Array<TextureRegion> buildingBlocks, float x, float y, int blocksX, int blocksY, float scrollSpeed) {

		position = new Vector2(x, y);
		velocity = new Vector2(scrollSpeed, 0);
		this.blocksX = blocksX;
		this.blocksY = blocksY;

		this.buildingBlocks = buildingBlocks;

		boundingBox = new Rectangle(x, y, getWidth(), getHeight());

	}

	/**
	 * Renders the building
	 * 
	 * @param batch
	 *            the SpriteBatch used to draw on
	 */
	public void render(SpriteBatch batch) {

		Vector2 currentPos = new Vector2(BLOCK_WIDTH, getHeight());

		boolean firstRow = true;

		for (TextureRegion block : buildingBlocks) {
			batch.draw(block, currentPos.x + position.x, currentPos.y + position.y);
			if (currentPos.y < position.y / 2) {
				batch.draw(block, currentPos.x + position.x, (position.y - (currentPos.y * 2) - BLOCK_HEIGHT),
						block.getRegionWidth(), block.getRegionHeight() * 2);
			}

			if (firstRow) {

				currentPos.x += BLOCK_WIDTH;
				if (currentPos.x > blocksX * BLOCK_WIDTH - BLOCK_WIDTH - 1) {
					currentPos.x = 0;
					currentPos.y -= BLOCK_HEIGHT;
					firstRow = false;
				}
			} else {

				currentPos.x += BLOCK_WIDTH;
				if (currentPos.x > blocksX * BLOCK_WIDTH - 1) {
					currentPos.x = 0;
					currentPos.y -= BLOCK_HEIGHT;
				}

			}
		}

	}

	/**
	 * Renders debug information
	 * 
	 * @param renderer
	 *            a ShapeRenderer
	 */
	public void debugRender(ShapeRenderer renderer) {
		renderer.setColor(1, 1, 0, .4f);
		renderer.rect(getBoundingBox().x, getBoundingBox().y, getBoundingBox().width, getBoundingBox().height);

	}

	/**
	 * Updates the building
	 * 
	 * @param delta
	 *            time since last time the method was executed
	 */
	public void update(float delta) {
		position.add(velocity.cpy().scl(delta));
		boundingBox.setPosition(position);

		if (getTailX() < 0) {
			isScrolledLeft = true;
		}

	}

	/**
	 * Resets the building
	 * 
	 * @param buildingBlocks
	 *            array of TextureRegions used for the building
	 * @param x
	 *            x position
	 * @param y
	 *            y position
	 * @param blocksX
	 *            number of blocks wide
	 * @param blocksY
	 *            number of blocks tall
	 * @param scrollSpeed
	 *            scroll-speed for the building
	 */
	public void reset(Array<TextureRegion> buildingBlocks, float x, float y, int blocksX, int blocksY, float scrollSpeed) {
		position = new Vector2(x, y);
		velocity = new Vector2(scrollSpeed, 0);
		this.blocksX = blocksX;
		this.blocksY = blocksY;

		this.buildingBlocks = buildingBlocks;

		boundingBox = new Rectangle(x, y, getWidth(), getHeight());

		isScrolledLeft = false;
	}

	/**
	 * 
	 * @return true if building is out of the left side of the screen
	 */
	public boolean isScrolledLeft() {
		return isScrolledLeft;
	}

	/**
	 * 
	 * @return the x position of the right side of the building
	 */
	public float getTailX() {
		return position.x + blocksX * BLOCK_WIDTH;
	}

	/**
	 * 
	 * @return the x position of the builduing
	 */
	public float getX() {
		return position.x;
	}

	/**
	 * 
	 * @return the y position of the building
	 */
	public float getY() {
		return position.y;
	}

	/**
	 * 
	 * @return width of the building
	 */
	public int getWidth() {
		return blocksX * BLOCK_WIDTH;
	}

	/**
	 * 
	 * @return height of the building
	 */
	public int getHeight() {
		return blocksY * BLOCK_HEIGHT;
	}

	/**
	 * 
	 * @return the bounding box of the building
	 */
	public Rectangle getBoundingBox() {
		return boundingBox;
	}

	/**
	 * 
	 * @return scroll-speed of the building
	 */
	public float getScrollSpeed() {
		return velocity.x;
	}

	/**
	 * 
	 * @param scrollSpeed
	 */
	public void setScrollSpeed(float scrollSpeed) {
		velocity.x = scrollSpeed;
	}

}
