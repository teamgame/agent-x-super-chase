package se.teamgame.axsc.screens.menu.accessors;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.teamgame.axsc.testrunner.LibGDXTestRunner;

import com.badlogic.gdx.scenes.scene2d.Actor;

@RunWith(LibGDXTestRunner.class)
public class ActorAccessorTest {

	ActorAccessor accessor;
	private Actor target;
	private float[] alpha;
	private int returnedValue;

	@Before
	public void setup() {
		accessor = new ActorAccessor();

		target = new Actor();
		alpha = new float[] { .5f };
	}

	@Test
	public void testSetValues() {

		accessor.setValues(target, 0, alpha);
		assertEquals("Check if Alpha is 0.5", .5f, target.getY(), .01f);

		alpha = new float[] { 0, 1, .5f };
		accessor.setValues(target, 1, alpha);
		assertEquals("Check if red is 0", 0, target.getColor().r, .01f);
		assertEquals("Check if green is 1", 1, target.getColor().g, .01f);
		assertEquals("Check if blue is 0.5", .5f, target.getColor().b, .01f);

		alpha = new float[] { .3f };
		accessor.setValues(target, 2, alpha);
		assertEquals("Check if alpha is 0.3", .3f, target.getColor().a, .01f);
	}

	@Test
	public void testGetValues() {

		accessor.setValues(target, 0, alpha);

		returnedValue = accessor.getValues(target, 0, alpha);
		assertEquals("Check if y value returned 1", 1, returnedValue, 0);

		alpha = new float[] { 0, 1, .5f };
		returnedValue = accessor.getValues(target, 1, alpha);
		assertEquals("Check if rgb value returned 3", 3, returnedValue, 0);

		alpha = new float[] { .5f };
		returnedValue = accessor.getValues(target, 2, alpha);
		assertEquals("Check if alpha value returned 1", 1, returnedValue, 0);

		returnedValue = accessor.getValues(target, 5, alpha);
		assertEquals("Check if failed value returned -1", -1, returnedValue, 0);
	}

}
