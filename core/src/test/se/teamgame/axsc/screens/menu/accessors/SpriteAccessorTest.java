package se.teamgame.axsc.screens.menu.accessors;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.teamgame.axsc.testrunner.LibGDXTestRunner;

import com.badlogic.gdx.graphics.g2d.Sprite;

@RunWith(LibGDXTestRunner.class)
public class SpriteAccessorTest {

	SpriteAccessor accessor;
	private Sprite sprite;
	private float[] alpha;
	private int returnedValue;

	@Before
	public void setup() {
		accessor = new SpriteAccessor();

		sprite = new Sprite();
		alpha = new float[] { .5f };
	}

	@Test
	public void testSetValues() {

		accessor.setValues(sprite, 0, alpha);
		assertEquals("Check if Alpha is 0.5", .5f, sprite.getColor().a, .01f);
	}

	@Test
	public void testGetValues() {

		accessor.setValues(sprite, 0, alpha);

		returnedValue = accessor.getValues(sprite, 0, alpha);
		assertEquals("Check if alpha value returned 1", 1, returnedValue, 0);

		returnedValue = accessor.getValues(sprite, 5, alpha);
		assertEquals("Check if failed value returned -1", -1, returnedValue, 0);
	}
}
