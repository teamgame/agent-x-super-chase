package se.teamgame.axsc.testrunner;

import java.lang.Thread.UncaughtExceptionHandler;

import org.apache.log4j.Logger;
import org.junit.internal.runners.model.EachTestNotifier;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.InitializationError;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class LibGDXTestRunner extends BlockJUnit4ClassRunner {
	public LibGDXTestRunner(Class<?> klass) throws InitializationError {
		super(klass);

	}

	static Logger log = Logger.getLogger(LibGDXTestRunner.class);

	volatile boolean finished = false;

	@Override
	public void run(final RunNotifier notifier) {
		log.debug("Starting " + getDescription());
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 100;
		config.height = 80;
		config.title = "Test";

		config.forceExit = false;

		final LibGDXTestRunner runner = this;
		LwjglApplication app = null;

		try {
			Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler() {

				@Override
				public void uncaughtException(Thread t, Throwable e) {
					runner.finished = true;
					EachTestNotifier testNotifier = new EachTestNotifier(notifier, getDescription());
					testNotifier.addFailure(e);
				}
			});

			Gdx.gl = null;
			app = new LwjglApplication(new ApplicationListener() {

				@Override
				public void resume() {
				}

				@Override
				public void resize(int width, int height) {
				}

				@Override
				public void render() {
					if (!finished) {

						try {
							runner.actualRun(notifier);
						} catch (Throwable t) {
							log.error("Error", t);
						} finally {
							runner.finished = true;
						}
					}
				}

				@Override
				public void pause() {
				}

				@Override
				public void dispose() {
				}

				@Override
				public void create() {
				}
			}, config);

			while (!finished) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					log.error("Interrupted", e);
				}
			}
		} catch (Throwable t) {
			log.error("Something went wrong", t);

		} finally {
			if (app != null) {
				app.stop();
			}
		}

		log.debug(getDescription() + " done");
	}

	public void actualRun(RunNotifier notifier) {
		super.run(notifier);
	}

}
