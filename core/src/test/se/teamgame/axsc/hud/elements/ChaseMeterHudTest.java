package se.teamgame.axsc.hud.elements;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.teamgame.axsc.managers.AssetManager;
import se.teamgame.axsc.testrunner.LibGDXTestRunner;

@RunWith(LibGDXTestRunner.class)
public class ChaseMeterHudTest {

	ChaseMeterHud chaseMeter_HUD;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		AssetManager.getInstance().loadGraphics();
		AssetManager.getInstance().loadFonts();
		AssetManager.getInstance().loadSounds();
	}

	@Before
	public void setup() throws Exception {
		chaseMeter_HUD = new ChaseMeterHud();
	}

	@Test
	public void testGetBackgoundSprite() throws Exception {

		Assert.assertNotNull("Could not get background sprite", chaseMeter_HUD.getBackgroundSprite());
	}

	@Test
	public void testGetPlayerSprite() throws Exception {

		Assert.assertNotNull("Could not get foreground sprite", chaseMeter_HUD.getPlayerSprite());
	}

	@Test
	public void testGetEnemySprite() throws Exception {

		Assert.assertNotNull("Could not get label", chaseMeter_HUD.getEnemySprite());
	}
}
