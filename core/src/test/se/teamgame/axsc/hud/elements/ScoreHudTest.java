package se.teamgame.axsc.hud.elements;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.teamgame.axsc.managers.AssetManager;
import se.teamgame.axsc.testrunner.LibGDXTestRunner;

@RunWith(LibGDXTestRunner.class)
public class ScoreHudTest {

	ScoreHud score_HUD;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		AssetManager.getInstance().loadGraphics();
		AssetManager.getInstance().loadFonts();
		AssetManager.getInstance().loadSounds();
	}

	@Before
	public void setup() throws Exception {
		score_HUD = new ScoreHud();
	}

	@Test
	public void testGetScore() throws Exception {

		Assert.assertNotNull("Could not get label", score_HUD.getScore());
	}
}
