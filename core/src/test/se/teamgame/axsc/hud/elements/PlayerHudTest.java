package se.teamgame.axsc.hud.elements;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.teamgame.axsc.managers.AssetManager;
import se.teamgame.axsc.testrunner.LibGDXTestRunner;

@RunWith(LibGDXTestRunner.class)
public class PlayerHudTest {

	PlayerHud player_HUD;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		AssetManager.getInstance().loadGraphics();
		AssetManager.getInstance().loadFonts();
		AssetManager.getInstance().loadSounds();
	}

	@Before
	public void setup() throws Exception {
		player_HUD = new PlayerHud();
	}

	@Test
	public void testGetBackgoundSprite() throws Exception {

		Assert.assertNotNull("Could not get background sprite", player_HUD.getBackgroundSprite());
	}

	@Test
	public void testGetForegroundSprite() throws Exception {

		Assert.assertNotNull("Could not get foreground sprite", player_HUD.getForgroundSprite());
	}

	@Test
	public void testGetLabel() throws Exception {

		Assert.assertNotNull("Could not get label", player_HUD.getLabel());
	}

}
