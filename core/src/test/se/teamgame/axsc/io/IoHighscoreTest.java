package se.teamgame.axsc.io;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.teamgame.axsc.gamestate.GameState;
import se.teamgame.axsc.levels.Level;
import se.teamgame.axsc.managers.AssetManager;
import se.teamgame.axsc.testrunner.LibGDXTestRunner;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

@RunWith(LibGDXTestRunner.class)
public class IoHighscoreTest {

	AssetManager manager;
	IoHighscore ioh;
	private FileHandle file;
	private GameState state;
	private LabelStyle style;
	private Label labelA;
	private Label labelB;
	private Label labelC;
	private String[] higscoreList;
	private final String filename = "score.tst";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		AssetManager.getInstance().loadGraphics();
		AssetManager.getInstance().loadFonts();
		AssetManager.getInstance().loadSounds();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		AssetManager.getInstance().dispose();
	}

	@Before
	public void setup() throws Exception {

		ioh = new IoHighscore();
		manager = AssetManager.getInstance();
		file = Gdx.files.local(filename);

		state = new GameState(new Level());
		state.setScore(400000);
		style = new LabelStyle(manager.font_white, null);
		labelA = new Label("A", style);
		labelB = new Label("B", style);
		labelC = new Label("C", style);

		ioh.saveHighscore(state, labelA, labelB, labelC, filename);
	}

	@After
	public void tearDown() {
		Gdx.files.local("score.tst").delete();
	}

	@Test
	public void testSaveHighscore() {

		higscoreList = file.readString().split(",");

		assertEquals("Check score save", "400000", higscoreList[0]);
		assertEquals("Check name save", "ABC", higscoreList[1]);
	}

	@Test
	public void testLoadHighscore() {

		higscoreList = ioh.loadHighscore(filename);

		assertEquals("Check score load", "400000", higscoreList[0]);
		assertEquals("Check name load", "ABC", higscoreList[1]);
	}

}
