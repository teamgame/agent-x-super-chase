package se.teamgame.axsc.pools;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import se.teamgame.axsc.entities.buildings.Building;
import se.teamgame.axsc.entities.buildings.BuildingBuilder.BuildingHeight;
import se.teamgame.axsc.entities.buildings.BuildingBuilder.BuildingType;

public class BuildingPoolTest {

	BuildingPool pool;

	@Before
	public void setUp() throws Exception {
		pool = new BuildingPool();
	}

	@Test
	public void testGetBuilding() {
		assertNotNull("Get building", pool.getBuilding(10, 10, BuildingHeight.LOW, BuildingType.A, 10));
	}

	@Test
	public void testRecycle() {
		Building building = pool.getBuilding(10, 10, BuildingHeight.LOW, BuildingType.A, 10);
		pool.recycle(building);
		assertEquals("Recycle", 1, pool.getPoolSize());
	}

	@Test
	public void testGetPoolSize() {
		assertEquals("Get pool size", 0, pool.getPoolSize());
	}

}
