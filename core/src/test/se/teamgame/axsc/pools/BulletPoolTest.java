package se.teamgame.axsc.pools;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.teamgame.axsc.entities.weapons.Bullet;
import se.teamgame.axsc.managers.AssetManager;
import se.teamgame.axsc.pools.BulletPool.BulletType;
import se.teamgame.axsc.testrunner.LibGDXTestRunner;

@RunWith(LibGDXTestRunner.class)
public class BulletPoolTest {

	BulletPool pool;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		AssetManager.getInstance().loadGraphics();
		AssetManager.getInstance().loadFonts();
		AssetManager.getInstance().loadSounds();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		AssetManager.getInstance().dispose();
	}

	@Before
	public void setUp() throws Exception {
		pool = new BulletPool();
	}

	@Test
	public void testGetBuilding() {
		assertNotNull("Get bullet", pool.getBullet(10, 10, 10, 10, BulletType.PLAYER_BULLET));
	}

	@Test
	public void testRecycle() {
		Bullet bullet = pool.getBullet(10, 10, 10, 10, BulletType.PLAYER_BULLET);
		pool.recycle(bullet);
		assertEquals("Recycle", 1, pool.getPoolSize());
	}

	@Test
	public void testGetPoolSize() {
		assertEquals("Get pool size", 0, pool.getPoolSize());
	}

}
