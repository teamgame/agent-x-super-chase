package se.teamgame.axsc.managers;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.teamgame.axsc.testrunner.LibGDXTestRunner;

@RunWith(LibGDXTestRunner.class)
public class AssetManagerTest {

	AssetManager manager;

	@Before
	public void setUp() throws Exception {
		manager = AssetManager.getInstance();

		manager.loadGraphics();
		manager.loadFonts();
		manager.loadSounds();
	}

	@After
	public void tearDown() throws Exception {
		manager.dispose();
	}

	@Test
	public void testLoadSplashGraphics() throws Exception {

		Assert.assertNotNull("Letter A not loaded", manager.a_letter_big);
		Assert.assertNotNull("Letter E not loaded", manager.e_letter_big);
		Assert.assertNotNull("Letter G not loaded", manager.g_letter_big);
		Assert.assertNotNull("Letter M not loaded", manager.m_letter_big);
		Assert.assertNotNull("Letter T not loaded", manager.t_letter_big);
		Assert.assertNotNull("Letter e not loaded", manager.e_letter_small);
		Assert.assertNotNull("Letter n not loaded", manager.n_letter_small);
		Assert.assertNotNull("Letter p not loaded", manager.p_letter_small);
		Assert.assertNotNull("Letter r not loaded", manager.r_letter_small);
		Assert.assertNotNull("Letter s not loaded", manager.s_letter_small);
		Assert.assertNotNull("Letter t not loaded", manager.t_letter_small);
	}

	@Test
	public void testLoadMenuAssets() throws Exception {

		Assert.assertNotNull("Menu logo not loaded", manager.menu_logo);
		Assert.assertNotNull("Menu skin not loaded", manager.menu_skin);
	}

	public void testLoadHudAssets() throws Exception {

		Assert.assertNotNull("Health background bar not loaded", manager.bar_bkg);
		Assert.assertNotNull("Health player bar not loaded", manager.player_bar);
		Assert.assertNotNull("Health enemy bar not loaded", manager.enemy_bar);
		Assert.assertNotNull("Metior warning not loaded", manager.meteorWarning);
		Assert.assertNotNull("Insert coin not loaded", manager.insertCoin);
	}

	@Test
	public void testLoadFonts() throws Exception {

		Assert.assertNotNull("Red font not loaded", manager.font_red);
		Assert.assertNotNull("White font not loaded", manager.font_white);
		Assert.assertNotNull("Arkitekt20 font not loaded", manager.arkitech20);
		Assert.assertNotNull("Arkitekt30 font not loaded", manager.arkitech30);
		Assert.assertNotNull("Arkitekt60 font not loaded", manager.arkitech60);
		Assert.assertNotNull("Arkitekt100 font not loaded", manager.arkitech100);
		Assert.assertNotNull("Arkitekt menu font not loaded", manager.arkitech_menu);
	}

	@Test
	public void testLoadSounds() throws Exception {

		Assert.assertNotNull("Alert sound not loaded", manager.alertSound);
		Assert.assertNotNull("Laser sound not loaded", manager.laserSound);
		Assert.assertNotNull("Explosion sound  one not loaded", manager.explosionSound1);
		Assert.assertNotNull("Explosion sound  two not loaded", manager.explosionSound2);
		Assert.assertNotNull("Explosion sound  three not loaded", manager.explosionSound3);
		Assert.assertNotNull("Ricochet sound not loaded", manager.ricochetSound);
		Assert.assertNotNull("Rotator sound one not loaded", manager.rotatorShoot1);
		Assert.assertNotNull("Rotator sound two not loaded", manager.rotatorShoot2);
		Assert.assertNotNull("Rotator sound three not loaded", manager.rotatorShoot3);
		Assert.assertNotNull("Sentry sound one not loaded", manager.sentryShoot1);
		Assert.assertNotNull("Sentry sound two not loaded", manager.sentryShoot2);
		Assert.assertNotNull("Sentry sound three not loaded", manager.sentryShoot3);
		Assert.assertNotNull("Shoot sound one not loaded", manager.shoot1);
		Assert.assertNotNull("Shoot sound two not loaded", manager.shoot2);
		Assert.assertNotNull("Shoot sound three not loaded", manager.shoot3);
		Assert.assertNotNull("Portal sound not loaded", manager.portal);

		Assert.assertNotNull("Background music not loaded", manager.backgroundMusic);
	}

	@Test
	public void testLoadBuildingBlocks() throws Exception {

		Assert.assertNotNull("topCenter not loaded", manager.topCenter);

		Assert.assertNotNull("TopLeft1 not loaded", manager.topLeft1);
		Assert.assertNotNull("topRight1 not loaded", manager.topRight1);
		Assert.assertNotNull("leftLit1 not loaded", manager.leftLit1);
		Assert.assertNotNull("rightLit1 not loaded", manager.rightLit1);
		Assert.assertNotNull("leftDark1 not loaded", manager.leftDark1);
		Assert.assertNotNull("rightDark1 not loaded", manager.rightDark1);
		Assert.assertNotNull("centerLit1 not loaded", manager.centerLit1);
		Assert.assertNotNull("centerDark1 not loaded", manager.centerDark1);

		Assert.assertNotNull("topLeft2 not loaded", manager.topLeft2);
		Assert.assertNotNull("topRight2 not loaded", manager.topRight2);
		Assert.assertNotNull("leftLit2 not loaded", manager.leftLit2);
		Assert.assertNotNull("rightLit2 not loaded", manager.rightLit2);
		Assert.assertNotNull("leftDark2 not loaded", manager.leftDark2);
		Assert.assertNotNull("rightDark2 not loaded", manager.rightDark2);
		Assert.assertNotNull("centerLit2 not loaded", manager.centerLit2);
		Assert.assertNotNull("centerDark2 not loaded", manager.centerDark2);

		Assert.assertNotNull("topLeft3 not loaded", manager.topLeft3);
		Assert.assertNotNull("topRight3 not loaded", manager.topRight3);
		Assert.assertNotNull("leftLit3 not loaded", manager.leftLit3);
		Assert.assertNotNull("rightLit3 not loaded", manager.rightLit3);
		Assert.assertNotNull("leftDark3 not loaded", manager.leftDark3);
		Assert.assertNotNull("rightDark3 not loaded", manager.rightDark3);
		Assert.assertNotNull("centerLit3 not loaded", manager.centerLit3);
		Assert.assertNotNull("centerDark3 not loaded", manager.centerDark3);

		Assert.assertNotNull("roofOrnament0 not loaded", manager.roofOrnament0);
		Assert.assertNotNull("roofOrnament1 not loaded", manager.roofOrnament1);
		Assert.assertNotNull("roofOrnament2 not loaded", manager.roofOrnament2);
		Assert.assertNotNull("roofOrnament3 not loaded", manager.roofOrnament3);
		Assert.assertNotNull("roofOrnament4 not loaded", manager.roofOrnament4);
		Assert.assertNotNull("roofOrnament5 not loaded", manager.roofOrnament5);
		Assert.assertNotNull("roofOrnament6 not loaded", manager.roofOrnament6);
		Assert.assertNotNull("roofOrnament7 not loaded", manager.roofOrnament7);
	}

	@Test
	public void testLoadBackgroundGraphics() throws Exception {

		Assert.assertNotNull("skyBackground not loaded", manager.skyBackground);
		Assert.assertNotNull("backgroundSkyline not loaded", manager.backgroundSkyline);
		Assert.assertNotNull("mntLayerBack not loaded", manager.mntLayerBack);
		Assert.assertNotNull("mntLayerFront not loaded", manager.mntLayerFront);
		Assert.assertNotNull("water not loaded", manager.water);
	}

	@Test
	public void testLoadShipsGraphics() throws Exception {

		Assert.assertNotNull("playerShip not loaded", manager.playerShip);
		Assert.assertNotNull("mainEnemy not loaded", manager.mainEnemy);
	}

	@Test
	public void testLoadWeaponsGraphics() throws Exception {

		Assert.assertNotNull("playerBullet not loaded", manager.playerBullet);
		Assert.assertNotNull("enemyBullet not loaded", manager.enemyBullet);
		Assert.assertNotNull("mine not loaded", manager.mine);
	}

	@Test
	public void testLoadTrashEnemyGraphics() throws Exception {

		Assert.assertNotNull("thrust left not loaded", manager.thrustLeft);
		Assert.assertNotNull("sentry drone not loaded", manager.sentryDrone);
		Assert.assertNotNull("sentry drone thrust not loaded", manager.sentryDroneThrust);
		Assert.assertNotNull("rotator body not loaded", manager.rotatorBody);
		Assert.assertNotNull("rotator turret not loaded", manager.rotatorTurret);
		Assert.assertNotNull("rotator thrust not loaded", manager.rotatorThrust);
		Assert.assertNotNull("meteorit not loaded", manager.meteorit);
		Assert.assertNotNull("big one not loaded", manager.bigOne);
		Assert.assertNotNull("big one laser not loaded", manager.bigOneLaser);
	}

	@Test
	public void testLoadAnimations() throws Exception {

		Assert.assertNotNull("explosion not loaded", manager.explosion);
		Assert.assertNotNull("portal not loaded", manager.portal);
	}
}
