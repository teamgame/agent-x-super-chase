package se.teamgame.axsc.gamestate;

import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.teamgame.axsc.levels.Level;
import se.teamgame.axsc.levels.Level.LevelState;
import se.teamgame.axsc.managers.AssetManager;
import se.teamgame.axsc.testrunner.LibGDXTestRunner;

@RunWith(LibGDXTestRunner.class)
public class GameStateTest {

	static Level level;
	GameState gameState;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		AssetManager.getInstance().loadGraphics();
		AssetManager.getInstance().loadFonts();
		AssetManager.getInstance().loadSounds();
		level = new Level();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		AssetManager.getInstance().dispose();
	}

	@Before
	public void setUp() throws Exception {
		gameState = new GameState(level);
	}

	@Test
	public void testGetScore() {
		assertEquals("Get score", 500000, gameState.getScore());
	}

	@Test
	public void testGetPlayerHealth() {
		assertEquals("Get player health", 100, gameState.getPlayerHealth());
	}

	@Test
	public void testGetEnemyHealth() {
		assertEquals("Get enemy health", 100, gameState.getEnemyHealth());
	}

	@Test
	public void testGetDistanceTraveled() {
		assertEquals("Get distance traveled", 0, gameState.getDistanceTravelled());
	}

	@Test
	public void testGetLevelState() {
		assertEquals("Get level state", LevelState.START, gameState.getLevelState());
	}

	@Test
	public void testGetDistanceToEnemy() {
		assertEquals("Get distance to enemy", 0, gameState.getDistanceToEnemy());
	}

	@Test
	public void testGetCombo() {
		assertEquals("Get combo", 0, gameState.getCombo());
	}

	@Test
	public void testGetFps() {
		assertEquals("Get fps", 0, gameState.getFps());
	}

	@Test
	public void testGetEndpoint() {
		assertEquals("Get endpoint", 500000, gameState.getEndpoint());
	}

	@Test
	public void testUpdateScore() {
		gameState.setScore(10);
		gameState.updateScore();
		assertEquals("Update score", 500000, gameState.getScore());
	}

	@Test
	public void testSetScore() {
		gameState.setScore(10);
		assertEquals("Set score", 10, gameState.getScore());
	}

}
