package se.teamgame.axsc.entities.background;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.teamgame.axsc.testrunner.LibGDXTestRunner;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;

@RunWith(LibGDXTestRunner.class)
public class ScrollingLayerTest {

	ScrollingLayer scrollingLayer;
	Texture texture;
	Pixmap pixmap;

	@Before
	public void setUp() throws Exception {
		pixmap = new Pixmap(1, 1, Format.RGB888);
		texture = new Texture(pixmap);
		scrollingLayer = new ScrollingLayer(texture, 50);
	}

	@After
	public void tearDown() throws Exception {
		pixmap.dispose();
		texture.dispose();
	}

	@Test
	public void testGetScrollSpeed() {
		assertEquals("get scroll speed", 50, scrollingLayer.getScrollSpeed(), 0.01);
	}

	@Test
	public void testSetScrollSpeed() {
		scrollingLayer.setScrollSpeed(100);

		assertEquals("set scroll speed", 100, scrollingLayer.getScrollSpeed(), 0.01);
	}

}
