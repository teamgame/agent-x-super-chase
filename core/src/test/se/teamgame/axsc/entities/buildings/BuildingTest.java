package se.teamgame.axsc.entities.buildings;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Before;
import org.junit.Test;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

public class BuildingTest {

	int blocksX = 10;
	int blocksY = 10;
	Building building;

	@Before
	public void setUp() throws Exception {
		building = new Building(new Array<TextureRegion>(), 10, 10, blocksX, blocksY, 10);
	}

	@Test
	public void testReset() {
		building.reset(new Array<TextureRegion>(), 20, 20, blocksX, blocksY, 20);
		assertFalse("Scrolled left", building.isScrolledLeft());
		assertEquals("Get X", 20, building.getX(), 0.01);
		assertEquals("Get Y", 20, building.getY(), 0.01);
		assertEquals("get scroll speed", 20, building.getScrollSpeed(), 0.01);
	}

	@Test
	public void testIsScrolledLeft() {
		assertFalse("Scrolled left", building.isScrolledLeft());
	}

	@Test
	public void testGetTailX() {
		assertEquals("Tail X", building.getX() + blocksX * Building.BLOCK_WIDTH, building.getTailX(), 0.01);
	}

	@Test
	public void testGetX() {
		assertEquals("Get X", 10, building.getX(), 0.01);
	}

	@Test
	public void testGetY() {
		assertEquals("Get Y", 10, building.getY(), 0.01);
	}

	@Test
	public void testGetWidth() {
		assertEquals("Width", blocksX * Building.BLOCK_WIDTH, building.getWidth(), 0.01);
	}

	@Test
	public void testGetHeight() {
		assertEquals("Height", blocksY * Building.BLOCK_HEIGHT, building.getHeight(), 0.01);
	}

	@Test
	public void testGetScrollSpeed() {
		assertEquals("get scroll speed", 10, building.getScrollSpeed(), 0.01);
	}

	@Test
	public void testSetScrollSpeed() {
		building.setScrollSpeed(100);

		assertEquals("set scroll speed", 100, building.getScrollSpeed(), 0.01);
	}

}
