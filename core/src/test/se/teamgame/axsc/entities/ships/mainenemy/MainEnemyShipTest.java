package se.teamgame.axsc.entities.ships.mainenemy;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.teamgame.axsc.entities.ships.Ship.ShipState;
import se.teamgame.axsc.levels.Level;
import se.teamgame.axsc.managers.AssetManager;
import se.teamgame.axsc.testrunner.LibGDXTestRunner;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;

@RunWith(LibGDXTestRunner.class)
public class MainEnemyShipTest {

	MainEnemyShip ship;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		AssetManager.getInstance().loadGraphics();
		AssetManager.getInstance().loadFonts();
		AssetManager.getInstance().loadSounds();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		AssetManager.getInstance().dispose();
	}

	@Before
	public void setUp() throws Exception {
		ship = new MainEnemyShip(10, 10, new Level());
	}

	@Test
	public void testColidesRectangle() {
		assertTrue("Rectangle collision", ship.colides(new Rectangle(10, 10, 10, 10)));
		assertFalse("No rectangle collision", ship.colides(new Rectangle(500, 500, 10, 10)));
	}

	@Test
	public void testColidesCircle() {
		assertTrue("Circle collision", ship.colides(new Circle(10, 10, 10)));
		assertFalse("No circle collision", ship.colides(new Circle(500, 500, 10)));
	}

	@Test
	public void testColidesRectangleArray() {
		assertTrue("Rectangle array collision", ship.colides(new Rectangle[] { new Rectangle(10, 10, 10, 10) }));
		assertFalse("No rectangle array collision", ship.colides(new Rectangle[] { new Rectangle(500, 500, 10, 10) }));
	}

	@Test
	public void testHit() {
		ship.hit(20);
		assertEquals("Hit for 20", 80, ship.getHealth());
	}

	@Test
	public void testGetX() {
		assertEquals("Get x", 10, ship.getX(), 0.01);
	}

	@Test
	public void testGetY() {
		assertEquals("Get y", 10, ship.getY(), 0.01);
	}

	@Test
	public void testGetWidth() {
		assertEquals("Get width", 200, ship.getWidth(), 0.01);
	}

	@Test
	public void testGetHeight() {
		assertEquals("Get height", 45, ship.getHeight(), 0.01);
	}

	@Test
	public void testGetBoundingBox() {
		assertNotNull("Bounding box", ship.getBoundingBox());
	}

	@Test
	public void testGetState() {
		assertEquals("Get state", ShipState.START, ship.getState());
	}

	@Test
	public void testGetHealth() {
		assertEquals("Get health", 100, ship.getHealth());
	}

	@Test
	public void testSetHealth() {
		ship.setHealth(50);
		assertEquals("Set health 50", 50, ship.getHealth());
	}

	@Test
	public void testSetVelocityX() {
		ship.setVelocityX(50);
		ship.setState(ShipState.FLYING);
		ship.update(1);
		assertEquals("Set velocity X 50", 60, ship.getX(), 0.01);
	}

	@Test
	public void testSetVelocityY() {
		ship.setVelocityY(50);
		ship.setState(ShipState.FLYING);
		ship.update(1);
		assertEquals("Set velocity Y 50", 60, ship.getY(), 0.01);
	}

	@Test
	public void testSetPositionX() {
		ship.setPositionX(50);
		assertEquals("Set position X 50", 50, ship.getX(), 0.01);
	}

	@Test
	public void testSetPositionY() {
		ship.setPositionY(50);
		assertEquals("Set position Y 50", 50, ship.getY(), 0.01);
	}

	@Test
	public void testSetState() {
		ship.setState(ShipState.DEAD);
		assertEquals("Set state to dead", ShipState.DEAD, ship.getState());
	}
}
