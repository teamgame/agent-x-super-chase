package se.teamgame.axsc.entities.weapons;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.teamgame.axsc.pools.BulletPool.BulletType;
import se.teamgame.axsc.testrunner.LibGDXTestRunner;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

@RunWith(LibGDXTestRunner.class)
public class BulletTest {

	Pixmap pixmap;
	Texture texture;
	TextureRegion textureRegion;

	Bullet bullet;

	@Before
	public void setUp() throws Exception {
		pixmap = new Pixmap(1, 1, Pixmap.Format.RGB888);
		texture = new Texture(pixmap);
		textureRegion = new TextureRegion(texture);
		bullet = new Bullet(10, 10, Math.PI / 2, 10, textureRegion, BulletType.PLAYER_BULLET);
	}

	@After
	public void tearDown() throws Exception {
		pixmap.dispose();
		texture.dispose();
	}

	@Test
	public void testReset() {
		bullet.reset(20, 20, Math.PI / 2, 10, textureRegion, BulletType.ENEMY_BULLET);
		assertEquals("Get Type", BulletType.ENEMY_BULLET, bullet.getType());
	}

	@Test
	public void testGetType() {
		assertEquals("Get Type", BulletType.PLAYER_BULLET, bullet.getType());
	}

	@Test
	public void testGetWidth() {
		assertEquals("Get width", 1, bullet.getWidth(), 0.01);
	}

	@Test
	public void testGetHeight() {
		assertEquals("Get height", 1, bullet.getHeight(), 0.01);
	}

	@Test
	public void testGetBoundingBox() {
		assertNotNull("Get boundingbox", bullet.getBoundingBox());
	}

	@Test
	public void testIsActive() {
		assertTrue("Is active", bullet.isActive());
	}

	@Test
	public void testSetActive() {
		bullet.setActive(false);
		assertFalse("Not active", bullet.isActive());
	}

}
