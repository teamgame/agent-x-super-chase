package se.teamgame.axsc.entities.trashenemies;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.teamgame.axsc.entities.trashenemies.TrashEnemy.TrashState;
import se.teamgame.axsc.levels.Level;
import se.teamgame.axsc.managers.AssetManager;
import se.teamgame.axsc.testrunner.LibGDXTestRunner;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;

@RunWith(LibGDXTestRunner.class)
public class BigOneTest {

	BigOne bigOne;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		AssetManager.getInstance().loadGraphics();
		AssetManager.getInstance().loadFonts();
		AssetManager.getInstance().loadSounds();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		AssetManager.getInstance().dispose();
	}

	@Before
	public void setUp() throws Exception {
		bigOne = new BigOne(10, 10, new Level());
	}

	@Test
	public void testColidesRectangle() {
		assertTrue("Rectangle collision", bigOne.colides(new Rectangle(15, 15, 10, 10)));
		assertFalse("No rectangle collision", bigOne.colides(new Rectangle(500, 500, 10, 10)));
	}

	@Test
	public void testColidesCircle() {
		assertTrue("Circle collision", bigOne.colides(new Circle(15, 15, 10)));
		assertFalse("No circle collision", bigOne.colides(new Circle(500, 500, 10)));
	}

	@Test
	public void testColidesRectangleArray() {
		assertTrue("Rectangle array collision", bigOne.colides(new Rectangle[] { new Rectangle(15, 15, 10, 10) }));
		assertFalse("No rectangle array collision", bigOne.colides(new Rectangle[] { new Rectangle(500, 500, 10, 10) }));
	}

	@Test
	public void testGetX() {
		assertEquals("Get x", 10, bigOne.getX(), 0.01);
	}

	@Test
	public void testGetY() {
		assertEquals("Get y", 10, bigOne.getY(), 0.01);
	}

	@Test
	public void testGetWidth() {
		assertEquals("Get width", 280, bigOne.getWidth(), 0.01);
	}

	@Test
	public void testGetHeight() {
		assertEquals("Get height", 189, bigOne.getHeight(), 0.01);
	}

	@Test
	public void testGetBoundingBox() {
		assertNotNull("Bounding box", bigOne.getBoundingBox());
	}

	@Test
	public void testGetState() {
		assertEquals("Get state", TrashState.ALIVE, bigOne.getState());
	}

	@Test
	public void testIsActive() {
		assertTrue("Is active", bigOne.isActive());
	}

	@Test
	public void testGetScore() {
		assertEquals("Get score", 20000, bigOne.getScore());
	}

	@Test
	public void testSetVelocityX() {
		bigOne.setVelocityX(50);
		bigOne.update(1);
		assertEquals("Set velocity X 50", 60, bigOne.getX(), 0.01);
	}

	@Test
	public void testSetVelocityY() {
		bigOne.setVelocityY(50);
		bigOne.update(1);
		assertEquals("Set velocity Y 50", 60, bigOne.getY(), 0.01);
	}

	@Test
	public void testSetPositionY() {
		bigOne.setPositionY(50);
		assertEquals("Set position Y 50", 50, bigOne.getY(), 0.01);
	}

	@Test
	public void testSetState() {
		bigOne.setState(TrashState.DEAD);
		assertEquals("Set state to dead", TrashState.DEAD, bigOne.getState());
	}

	@Test
	public void testIsFiring() {
		assertFalse("Is firing", bigOne.isFiring());
	}

	@Test
	public void testGetLaser() {
		assertNotNull("Get laser", bigOne.getLaser());
	}

}
