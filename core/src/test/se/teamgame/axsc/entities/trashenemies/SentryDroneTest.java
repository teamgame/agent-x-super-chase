package se.teamgame.axsc.entities.trashenemies;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.teamgame.axsc.entities.trashenemies.TrashEnemy.TrashState;
import se.teamgame.axsc.levels.Level;
import se.teamgame.axsc.managers.AssetManager;
import se.teamgame.axsc.testrunner.LibGDXTestRunner;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;

@RunWith(LibGDXTestRunner.class)
public class SentryDroneTest {

	SentryDrone sentryDrone;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		AssetManager.getInstance().loadGraphics();
		AssetManager.getInstance().loadFonts();
		AssetManager.getInstance().loadSounds();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		AssetManager.getInstance().dispose();
	}

	@Before
	public void setUp() throws Exception {
		sentryDrone = new SentryDrone(10, 10, 10, new Level());
	}

	@Test
	public void testColidesRectangle() {
		assertTrue("Rectangle collision", sentryDrone.colides(new Rectangle(15, 15, 10, 10)));
		assertFalse("No rectangle collision", sentryDrone.colides(new Rectangle(500, 500, 10, 10)));
	}

	@Test
	public void testColidesCircle() {
		assertTrue("Circle collision", sentryDrone.colides(new Circle(15, 15, 10)));
		assertFalse("No circle collision", sentryDrone.colides(new Circle(500, 500, 10)));
	}

	@Test
	public void testColidesRectangleArray() {
		assertTrue("Rectangle array collision", sentryDrone.colides(new Rectangle[] { new Rectangle(15, 15, 10, 10) }));
		assertFalse("No rectangle array collision",
				sentryDrone.colides(new Rectangle[] { new Rectangle(500, 500, 10, 10) }));
	}

	@Test
	public void testGetX() {
		assertEquals("Get x", 10, sentryDrone.getX(), 0.01);
	}

	@Test
	public void testGetY() {
		assertEquals("Get y", 10, sentryDrone.getY(), 0.01);
	}

	@Test
	public void testGetWidth() {
		assertEquals("Get width", 120, sentryDrone.getWidth(), 0.01);
	}

	@Test
	public void testGetHeight() {
		assertEquals("Get height", 43, sentryDrone.getHeight(), 0.01);
	}

	@Test
	public void testGetBoundingBox() {
		assertNotNull("Bounding box", sentryDrone.getBoundingBox());
	}

	@Test
	public void testGetState() {
		assertEquals("Get state", TrashState.ALIVE, sentryDrone.getState());
	}

	@Test
	public void testIsActive() {
		assertTrue("Is active", sentryDrone.isActive());
	}

	@Test
	public void testGetScore() {
		assertEquals("Get score", 1000, sentryDrone.getScore());
	}

	@Test
	public void testSetVelocityX() {
		sentryDrone.setVelocityX(50);
		sentryDrone.update(1);
		assertEquals("Set velocity X 50", 60, sentryDrone.getX(), 0.01);
	}

	@Test
	public void testSetPositionY() {
		sentryDrone.setPositionY(50);
		assertEquals("Set position Y 50", 50, sentryDrone.getY(), 0.01);
	}

	@Test
	public void testSetState() {
		sentryDrone.setState(TrashState.DEAD);
		assertEquals("Set state to dead", TrashState.DEAD, sentryDrone.getState());
	}

}
