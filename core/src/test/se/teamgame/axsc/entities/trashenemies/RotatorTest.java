package se.teamgame.axsc.entities.trashenemies;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.teamgame.axsc.entities.trashenemies.TrashEnemy.TrashState;
import se.teamgame.axsc.levels.Level;
import se.teamgame.axsc.managers.AssetManager;
import se.teamgame.axsc.testrunner.LibGDXTestRunner;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;

@RunWith(LibGDXTestRunner.class)
public class RotatorTest {

	Rotator rotator;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		AssetManager.getInstance().loadGraphics();
		AssetManager.getInstance().loadFonts();
		AssetManager.getInstance().loadSounds();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		AssetManager.getInstance().dispose();
	}

	@Before
	public void setUp() throws Exception {
		rotator = new Rotator(10, 10, 10, new Level());
	}

	@Test
	public void testColidesRectangle() {
		assertTrue("Rectangle collision", rotator.colides(new Rectangle(15, 15, 10, 10)));
		assertFalse("No rectangle collision", rotator.colides(new Rectangle(500, 500, 10, 10)));
	}

	@Test
	public void testColidesCircle() {
		assertTrue("Circle collision", rotator.colides(new Circle(15, 15, 10)));
		assertFalse("No circle collision", rotator.colides(new Circle(500, 500, 10)));
	}

	@Test
	public void testColidesRectangleArray() {
		assertTrue("Rectangle array collision", rotator.colides(new Rectangle[] { new Rectangle(15, 15, 10, 10) }));
		assertFalse("No rectangle array collision",
				rotator.colides(new Rectangle[] { new Rectangle(500, 500, 10, 10) }));
	}

	@Test
	public void testGetX() {
		assertEquals("Get x", 10, rotator.getX(), 0.01);
	}

	@Test
	public void testGetY() {
		assertEquals("Get y", 10, rotator.getY(), 0.01);
	}

	@Test
	public void testGetWidth() {
		assertEquals("Get width", 57, rotator.getWidth(), 0.01);
	}

	@Test
	public void testGetHeight() {
		assertEquals("Get height", 35, rotator.getHeight(), 0.01);
	}

	@Test
	public void testGetBoundingBox() {
		assertNotNull("Bounding box", rotator.getBoundingBox());
	}

	@Test
	public void testGetState() {
		assertEquals("Get state", TrashState.ALIVE, rotator.getState());
	}

	@Test
	public void testIsActive() {
		assertTrue("Is active", rotator.isActive());
	}

	@Test
	public void testGetScore() {
		assertEquals("Get score", 3000, rotator.getScore());
	}

	@Test
	public void testSetVelocityX() {
		rotator.setVelocityX(50);
		rotator.update(1);
		assertEquals("Set velocity X 50", 60, rotator.getX(), 0.01);
	}

	@Test
	public void testSetVelocityY() {
		rotator.setVelocityY(50);
		rotator.update(1);
		assertEquals("Set velocity Y 50", 60, rotator.getY(), 0.01);
	}

	@Test
	public void testSetPositionY() {
		rotator.setPositionY(50);
		assertEquals("Set position Y 50", 50, rotator.getY(), 0.01);
	}

	@Test
	public void testSetState() {
		rotator.setState(TrashState.DEAD);
		assertEquals("Set state to dead", TrashState.DEAD, rotator.getState());
	}

}
