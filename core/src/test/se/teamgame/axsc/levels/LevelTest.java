package se.teamgame.axsc.levels;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.teamgame.axsc.entities.buildings.BuildingBuilder.BuildingHeight;
import se.teamgame.axsc.levels.Level.LevelState;
import se.teamgame.axsc.levels.Level.Wave;
import se.teamgame.axsc.managers.AssetManager;
import se.teamgame.axsc.testrunner.LibGDXTestRunner;

@RunWith(LibGDXTestRunner.class)
public class LevelTest {

	Level level;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		AssetManager.getInstance().loadGraphics();
		AssetManager.getInstance().loadFonts();
		AssetManager.getInstance().loadSounds();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		AssetManager.getInstance().dispose();
	}

	@Before
	public void setUp() throws Exception {
		level = new Level();
	}

	@Test
	public void testGetState() {
		assertEquals("State", LevelState.START, level.getState());
	}

	@Test
	public void testGetWave() {
		assertEquals("Wave", Wave.NONE, level.getWave());
	}

	@Test
	public void testGetWaveTime() {
		assertEquals("WaveTime", 0, level.getWaveTime(), 0.01);
	}

	@Test
	public void testGetFps() {
		assertEquals("FPS", 0, level.getFps());
	}

	@Test
	public void testGetComboCount() {
		assertEquals("ComboCount", 0, level.getComboCount());
	}

	@Test
	public void testBuildingHeight() {
		assertEquals("Wave", BuildingHeight.LOW, level.getBuildingHeight());
	}

	@Test
	public void testBuildingsOnScreen() {
		assertTrue("Buildings less than 0", level.getBuildingsOnScreen() > 0);
	}

	@Test
	public void testGetBuildingPoolSize() {
		assertEquals("Building pool size", 0, level.getBuildingPoolSize());
	}

	@Test
	public void testGetBulletPool() {
		assertNotNull("Bulletpool", level.getBulletPool());
	}

	@Test
	public void testGetBulletsOnScreen() {
		assertEquals("Bullets on screen", 0, level.getBulletsOnScreen());
	}

	@Test
	public void testGetBulletPoolSize() {
		assertEquals("Bullet pool size", 0, level.getBuildingPoolSize());
	}

	@Test
	public void testGetScrollSpeed() {
		assertEquals("Scroll speed", level.BASE_SPEED - level.getPlayerShip().getX() * 1.5f, level.getScrollSpeed(),
				0.01);
	}

	@Test
	public void testGetDistanceTraveled() {
		assertEquals("Distance traveled", 0, level.getDistanceTravelled());
	}

	@Test
	public void testGetBackground() {
		assertNotNull("Background", level.getBackground());
	}

	@Test
	public void testGetWater() {
		assertNotNull("Water", level.getWater());
	}

	@Test
	public void testGetBuildings() {
		assertNotNull("Buildings", level.getBuildings());
		assertEquals(level.getBuildingsOnScreen(), level.getBuildings().size);
	}

	@Test
	public void testGetBullets() {
		assertNotNull("Bullets", level.getBullets());
		assertEquals(level.getBulletsOnScreen(), level.getBullets().size);
	}

	@Test
	public void testGetPlayerShip() {
		assertNotNull("PlayerShip", level.getPlayerShip());
	}

	@Test
	public void testGetMainEnemy() {
		assertNotNull("MainEnemy", level.getMainEnemy());
	}

	@Test
	public void testGetMainEnemyAi() {
		assertNotNull("MainEnemyAi", level.getMainEnemyAi());
	}

	@Test
	public void testGetEnemies() {
		assertNotNull("Enemies", level.getEnemies());
	}

	@Test
	public void testGetHud() {
		assertNotNull("Hud", level.getHud());
	}

	@Test
	public void testGetEndPoint() {
		assertEquals("End point", 500000, level.getEndPoint());
	}

	@Test
	public void testGetCoins() {
		assertEquals("Coins", 3, level.getCoins());
	}

	@Test
	public void testGetInsertCoinTimer() {
		assertEquals("Insert coin timer", 0, level.getInsertCoinTimer());
	}

}
