package se.teamgame.axsc.desktop;

import static se.teamgame.axsc.AgentXSuperChase.TITLE;
import static se.teamgame.axsc.AgentXSuperChase.VERSION;
import se.teamgame.axsc.AgentXSuperChase;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

/**
 * Desktop launcher, entry point for game
 * 
 * @author marcus
 *
 */
public class DesktopLauncher {

	public static void main(String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = String.format("%s - v%s", TITLE, VERSION);
		config.vSyncEnabled = true;
		new LwjglApplication(new AgentXSuperChase(), config);
	}
}
